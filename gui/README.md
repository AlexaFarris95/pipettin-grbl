# GUI

Graphical user interface for the pipetting-bot project.

Licenced under the terms of the following licence: [GNU Affero General Public License v3.0 only](https://spdx.org/licenses/AGPL-3.0-only.html)

---

Table of contents:

[[_TOC_]]

---

A quick view:

![Screenshot_20220721_121830.png](../doc/gui/Screenshot_20220721_121830.png)

> Workspace builder and protocol designer GUI.

## App structure

En backend de NodeJS.

Orientación general:

* Todo empieza en "bin/www", que levanta un servidor http de Node y sirve lo que está en "public/".
* Para cada ruta o path del URL hay un handler en [app.js](./app.js).
* La ruta principal "/" va a [routes/index.js](./routes/index.js), que de acuerdo al URL renderea algun template de la carpeta [views](./views).
* Tamibén está lo que devuelve JSON en [routes/mainApi.js](./routes/mainApi.js).
* Las funciones del HTML estan todas en [public/js/app.js](./public/js/app.js).
* Por ejemplo, para guardar el JSON de una platform en mongo, la función "`saveNewPlatform()`" hace un "`POST`" request a la API de platforms, 

Observaciones para meter mano:

* Las rutas de un router (en "`routes/`") son relativas a como se declararon en el "`app.js`" (usando el comando "`app.use()`").
* En los request handlers, "`res.json`" es equivalente a un "`return()`" en otros lenguajes; devuelve un objeto json. Podria ser "`res.send`" para mandar texto plano, etc.
* Las formas de los contents son dibujadas por  "`drawContent()`" en  [public/js/app.js](./public/js/app.js). El orden de los elementos del SVG (en "z") corresponde con el orden del agregado de elementos en "`drawContent()`".
* Los colorcitos de los tags se arman por hash en [views/workspacePanel.ejs](./views/workspacePanel.ejs).
* No se puede compartir el código del frontend (en "public") y del backend (en "views" y "routes") al mismo tiempo.
* El joystick está en [public/js/joystick.js](./public/js/joystick.js).
* Hay un [public/css/style.css](./public/css/style.css). El "#" es un "id" de un HTML tag, y el "." corresponde a una "clase" de un HTML tag.
* Abort protocol or Reset the commander: A running protocol can be killed _smoothly_ with a single click to the "skull" button. A second click will trigger a hard reset on the Arduino (details below).

Information flow for the "kill" button:

1. The button is defined in [views/home.ejs](./views/home.ejs).
2. Pressing it calls the `killProtocolProcess` defined in [public/js/app.js](./public/js/app.js).
3. That function _posts_ to `/api/run-command/kill`.
4. In [app.js](./app.js), it is defined that `/api/run-command` will be handled by `routes/commandsApi`. That corresponds to [routes/commandsApi.js](./routes/commandsApi.js).
6. Code in that file then calls the `commander.kill` method, defined in [lib/commander.js](./lib/commander.js)
7. The `kill` method _finally_ does the job, sending a "`kill_commander`" socketio message to the Python program in [commander.py](../protocol2gcode/commander.py) (the event is actually handled in [gcodeCommander.py](../protocol2gcode/gcodeCommander.py), by the `kill_commander` method).
8. The Python program receives this signal, and chooses what to do, tipically aborting or killing things. Learn more here: [README.md](../protocol2gcode/README.md).

## Screenshots

> Eye candy for the masses (?)

### Loaded workspace

![screenshot.png](../doc/gui/screenshot.png)

### Workspace setup and manual control

Note the "manual control" panel, floating on the workspace.

![Screenshot_20220721_121830.png](../doc/gui/Screenshot_20220721_121830.png)

### Platform definitions

List of platforms in the local Mongo database.

![Screenshot_20220721_121839.png](../doc/gui/Screenshot_20220721_121839.png)

A platform's definition can be edited, copied, and pasted.

![Screenshot_20220721_121849.png](../doc/gui/Screenshot_20220721_121849.png)

### Adding contents

Create a content object:

![content.png](../doc/gui/content.png)

Assign properties: volume, tags, name, etc.

> Note: a tube _must_ have non-zero volume to be used as a solution component source.

![content_props.png](../doc/gui/content_props.png)

### Protocol definition

Right sidebar:

![Screenshot_20220721_121910.png](../doc/gui/Screenshot_20220721_121910.png)



## Install requirements

Overall software requirements:

- node.js
- mongodb > 2.6
- serial communication libraries

> Note: skip all steps if using our custom OS image.

> Note: most, if not all, of the commands below are meant to be run in a Raspberry Pi, through `ssh`.

### Node

#### Dependencies

Install Node, and some serial communication libraries.

```bash
sudo apt install nodejs npm pkg-config libusb-1.0-0-dev libudev-dev  # This might take a while
```

#### Install app

1. Install:

```bash
cd gui
npm install  # This might take a while
```

#### Run app manually

```bash
node bin/www
```

3. Visit: [http://localhost:3333](http://localhost:3333) (replace "localhost" with your Pi's IP address if appropriate).
4. Stop the app by hitting Ctrl+C.

> Note: no workspaces, platforms, or protocols are defined unless imported (see notes below).

#### Systemd unit

A systemd unit file exists to automate startup and restart of the GUI.

Unit file located at: [`systemd.units/nodegui.service`](../systemd.units/nodegui.service)

For development, this systemd unit file uses `nodemon` instead of `node` to launch the app.

A compatible (legacy) nodemon version was installed with:

```bash
# Details at https://github.com/remy/nodemon/issues/1948#issuecomment-953665876
sudo npm install -g nodemon@2.0.12
```

The unit file should use `node` instead, for deployment.

Find details about systemd units at: [`systemd.units/README.md`](../systemd.units/README.md)

### MongoDB

Instructions for building and installing mongodb `v3.2.12` on Raspbian 32-bit.

Mongo needs to be > 2.6 to work with the Node driver. See: https://mongobooster.useresponse.com/topic/wire-version

I followed this guide: https://koenaerts.ca/compile-and-install-mongodb-on-raspberry-pi/

The project's "official" image already has the mongo binaries. See [GUIDES.md](../GUIDES.md) for instructions.

#### Prepare

Install build dependencies:

```bash
sudo apt update
sudo apt upgrade
sudo apt install wget scons build-essential
sudo apt install libboost-filesystem-dev libboost-program-options-dev libboost-system-dev libboost-thread-dev
sudo apt install python-pymongo  # Required by the protocol2gcode module.
```

Download and extract MongoDB:

```bash
mkdir -p mongo/install
cd mongo/install
wget https://fastdl.mongodb.org/src/mongodb-src-r3.2.12.tar.gz
# https://github.com/mongodb/mongo/archive/r3.2.12.tar.gz
tar xvf mongodb-src-r3.2.12.tar.gz
cd mongodb-src-r3.2.12
```

Create a very large swap:

```bash
sudo dd if=/dev/zero of=/mytempswapfile bs=1024 count=524288
sudo chmod 0600 /mytempswapfile
sudo mkswap /mytempswapfile
sudo swapon /mytempswapfile
```

And finally run these scripts:

```bash
cd src/third_party/mozjs-38/
./get_sources.sh
./gen-config.sh arm linux
cd -
```

#### Build MongoDB

First edit some files:

- Add `#include sys/sysmacros.h` to the file `src/mongo/db/storage/mmap_v1/mmap_v1_engine.cpp`

> There are some warnings treated as errors by cc1plus, so disable that behaviour and compile anyway. I skipped compilation of mongos.

Then build:

```bash
scons mongo mongod --wiredtiger=off --mmapv1=on --disable-warnings-as-errors  # guessing i do not need mongos
```

Reduce binary sizes:

```bash
strip -s mongo
strip -s mongod
```

#### Compilation notes

At first i tried to compile by ignoring two warnings by adding lines to the SConstruct file after `if myenv.ToolchainIs('clang', 'gcc')`:

```
        # AddToCCFLAGSIfSupported(myenv, '-Wno-parentheses')
        # AddToCCFLAGSIfSupported(myenv, '-Wnonnull-compare')
```

But the `nonnull-compare` warning was still raised as an error. I do not know what I am doing... so please help improve this if you know how :)

I did not look at all warnings during compilation so there may be other type of ignored warnings. Like "-Wstringop-truncation" (?)


#### Install and cleanup

Install:

```bash
cd build/opt/mongo
sudo cp mongo mongod /usr/local/bin/
```

Cleanup:

```bash
reboot
sudo swapoff /mytempswapfile
sudo rm /mytempswapfile
```

#### Configure mongod

Edit `/etc/mongod.conf` such that:

```
# mongod.conf

# for documentation of all options, see:
#   http://docs.mongodb.org/manual/reference/configuration-options/

# Where and how to store data.
storage:
  dbPath: /var/lib/mongodb
  journal:
    enabled: true
  engine: mmapv1
#  mmapv1:
#  wiredTiger:

# where to write logging data.
systemLog:
  destination: file
  logAppend: true
  path: /var/log/mongodb/mongod.log

# network interfaces
net:
  port: 27017
  bindIp: 127.0.0.1
```

The most important bit is the `engine: mmapv1` setting.

#### Create files and dirs

Some files and directories might be missing in your filesystem, and prevent Mongo from starting up.

To create them, run:

```bash
sudo mkdir -p /data/db/
sudo mkdir -p /var/lib/mongodb/
sudo mkdir -p /var/log/mongodb/
sudo touch /var/log/mongodb/mongod.log
```

#### Enable run on boot

Using `/etc/rc.local`, adding the following line before `exit 0`:

```bash
mongod --config /etc/mongod.conf
```

<!--
**NOT USED**: Enable unit service:

`sudo systemctl enable mongodb`

**NOT USED**: Using init:

* `/etc/init.d/mongodb`
* Which uses the config file: `/etc/mongodb.conf`

See also the notes written here: [mongo_notes.md](../doc/mongo_notes.md)
-->

#### Manually start mongod

To start mongod manually, run:

```bash
sudo mongod --storageEngine=mmapv1
```

If everything is ok, not messages should output, and it should hold the prompt.

Then reboot, and check that the mongo process is alive, using `ps -e | grep mongo`.

Inspect the log files for any issues by running: `cat /var/log/mongodb/mongod.log` (or more, less, page, etc.)

#### Repair abnormal shutdown

Sometimes the database is shutdown abnormally, and requires a _repair_.

To repair and start the database manually, run:

    sudo mongod --storageEngine=mmapv1 --repair
    sudo mongod --storageEngine=mmapv1
    
#### Autostart mongod

Add the following line to `/etc/rc.local` on the Pi's filesystem, just before the `exit 0` line:

```bash
mongod --config /etc/mongod.conf
```

<!-- NO LONGER IN USE
After you install MongoDB (see notes below) enable the systemd service:
`sudo systemctl enable mongodb  # After installation`
-->

### Install mongotools

You can use this CLI tool to import and export workspace, protocol, and platform definitions to JSON files (see below).

To install, run: `sudo apt install mongo-tools`

Official repo: https://github.com/mongodb/mongo-tools

The defaults from raspberry pi repos worked for the `mongoimport` command.

See [`defaults/README.md`](../defaults/README.md) for usage examples.

## Start the GUI

There is a systemctl service unit for this now. See files in the `systemd.units` directory.

Get support or report errors by creating an issue [here](https://gitlab.com/pipettin-bot/pipettin-grbl/-/boards/4685246).

Open the GUI by visiting: http://localhost:3333

To start it manually, change to the `gui` directory and run `node`:

```bash
cd gui
node bin/www
```

## Import GUI objects

See [`defaults/README.md`](../defaults/README.md) to learn how to import the default objects: protocols, workspaces, and platforms.

This requires `mongo-tools`, and a running instance of MongoDB.

## TO-DO

- refactor frontend (app.js)
- global config in mongo (area size, etc)
- import  steps
- item overwrite options (color, max volume, default heights)
- do not allow to refresh the page
- allow background for petri and rotation option 
- allow "all" in targets/sources selection in protocol step
- Fill mongo collections on first start

## Bugs

- Step selection is not working sometimes
- Save protocol is not saving if you are in a text area (programate click)

# Testing

Get support or report errors by creating an issue [here](https://gitlab.com/pipettin-bot/pipettin-grbl/-/boards/4685246).

## Requirements

* Node
* MongoDB

### MongoDB

In Arch Linux: https://wiki.archlinux.org/title/MongoDB

```bash
yay -S mongodb-bin mongodb-tools-bin
# yay -S mongodb-tools-bin  # Optional

sudo systemctl start mongodb.service
sudo systemctl status mongodb.service  # Check
```

### Node

Install Node (I used an LTS version), and the `npm` package manager:

```bash
sudo pacman -S nodejs-lts-gallium npm
```

Install the app:

```bash
cd gui
npm install
```

## Start the app

Start:

```bash
node bin/www
```

Visit: [http://localhost:3333](http://localhost:3333)

# Troubleshooting

Get support or report errors by creating an issue [here](https://gitlab.com/pipettin-bot/pipettin-grbl/-/boards/4685246).
