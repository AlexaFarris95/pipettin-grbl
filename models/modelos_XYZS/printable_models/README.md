# 3D-printable models

- [ ] TODO: Prepare printable STLs with updated models. The directory is currently empty.

Find here STL files for the full assembly, and some subsets (useful to follow the [assembly guide](https://gitlab.com/pipettin-bot/pipettin-grbl-docs)).

Full assembly export:

## XY axis parts

Base pieces for the XY frame

![XY_slicer.png](./images/XY_slicer.png)

## XZ axis parts

Base pieces for the Z axis

![Z_slicer.png](./images/Z_slicer.png)

## XY stepper and GT2 belt stuff

Stepper bases, idler bases, belt locks, and else.

![stepper_bases_y_idler.png](./images/stepper_bases_y_idler.png)

## Cable carrier links

3 cable carrier links (yellow arrows) and their three clips (blue), the two link bases for the tool's cablecarrier (magenta arrows), one bearing base for the link bases (green arrow), and the two XZ-axis' link bases (cyan arrows).

Only three link modules are in the file, for testing before printing all of them. You will need 100 of them for all axes, and a bit more to spare (note that you don't need 100 clips, about 30 will get the job done).

![cable_carriers.png](./images/cable_carriers.png)

## Pipette tool parts

![pipette_parts.png](./images/pipette_parts.png)

## End stop mounts

end-stops clamps for the 12 mm and 8 mm rods.

![end_stop_clamps.png](./images/end_stop_clamps.png)
