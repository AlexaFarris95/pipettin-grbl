# Tool-changer with a mini-latch

A tool-changing system using a push-latch and small permanent magnets.

## Description

In this mechanism, a "push latch" mechanism (not drawn) holds the tool on the toolpost (the red part on the right).

The tool is fixed to a pair of 8 mm linear rods (in red, at the center), which allow it to dock with the toolpost (by sliding on lm8uu linear bearings, only the slots are drawn).

When the toolhead (gray part on the right) approaches, it docks to the tool by sliding the pair of 8 mm rods into it's own pair of lm8uu linear bearings mounted directly on the Z axis carriage (red doodles on the right).

After docking to the tool, the toolhead pushes it forward a small amount, to actuate the "push latch" mechanism, thereby releasing the tool from the toolpost.

At the same time, by touching the tool, the Z carriage binds to the tool's back plate with a set of neodymium magnets (not drawn), which hold the tool in place against the toolhead.

To park the tool, the process is reversed (i.e. the toolhead slides the rods into the toolpost's linear bearings, and when the latch mechanism is actuated a second time, it locks the tool to the toolpost. Thus, when backing out, the toolhead will be separated from the tool, because the magnets holding it in place are (or must) not be as strong as the Y axis motors).

By "push latch" mechanism we mean this: https://www.youtube.com/watch?v=2clPZhs7DfU

![minilatch.png](./images/minilatch.png)

![push_latch.png](./images/push_latch.png)

Hay un mecanismo ya modelado en thingiverse: https://www.thingiverse.com/thing:4544557

![thingi.png](./images/thingi.png)

More videos:

* https://www.youtube.com/watch?v=VA7UGVCpcFk
* https://www.youtube.com/watch?v=MhVw-MHGv4s
* https://www.youtube.com/watch?v=2clPZhs7DfU
* https://www.youtube.com/watch?v=CGSd9GPEX9c

## Versions

### Mini-latch toolchanger v2

FreeCAD assembly: [toolchanger_assembly_v2.FCStd](./toolchanger_assembly_v2.FCStd)

Lista de materiales para una tool (objeto a la derecha en la foto): [bom.csv](./doc/bom.csv)

Exported models:

* [toolchanger_assembly_v2_toolparking.stl](./exported_models/toolchanger_assembly_v2_toolparking.stl)
* [toolchanger_assembly_v2_newparts.stl](./exported_models/toolchanger_assembly_v2_newparts.stl)
* Only the models not appearing on other directories have been exported.

![toolchanger_idea_magnets_and_push_latch_v2.png](./images/toolchanger_idea_magnets_and_push_latch_v2.png)

### Mini-latch toolchanger v1

Simple y funcional.

Me falta solucionar el tema end-stops.

Tuve que subir le Vref de los stepper drivers de eje Y a `0.950 V` para que no perdiera pasos.

![toolchanger_idea_magnets_and_push_latch_v1.png](./images/toolchanger_idea_magnets_and_push_latch_v1.png)



## Mini-latch model

Find the [mini_latch.FCStd](./extra_models/mini_latch.FCStd) FreeCAD file in the [extra_models](./extra_models) directory, and have a look at its [README.md](./extra_models/README.md).

This model is not meant for printing, it is only imported in assemblies for checking.

![hafele_minilatch_approx.png](./extra_models/images/hafele_minilatch_approx.png)

# Alternate systems

## Pipe's tool-changer

Have a look at:

* [modelos_toolchanger](../../modelos_toolchanger)
    * [README.md](../../modelos_toolchanger/README.md)
    * [pipes_toolchanger](../../modelos_toolchanger/pipes_toolchanger)
        * [README.md](../../modelos_toolchanger/pipes_toolchanger/README.md)

![toolchanger_screenshot_getit.png](../../modelos_toolchanger/images/toolchanger_screenshot_getit.png)

## 3D-printed push-latch

Models here: [improted_projects](./improted_projects)

![pic.png](./improted_projects/images/pic.png)
