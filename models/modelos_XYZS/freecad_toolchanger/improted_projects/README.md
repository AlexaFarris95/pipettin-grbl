# Imported models

Models from external projects.

## Push latch mechanism

Model files zipped here: [4544557_Push_Latch_Mechanism.zip](./4544557_Push_Latch_Mechanism.zip)

Originally published at Thingiverse: https://www.thingiverse.com/thing:4544557/files 

Push Latch Mechanism by [Coffreedom_Official](https://www.thingiverse.com/Coffreedom_Official) is licensed under the [Creative Commons - Attribution](https://creativecommons.org/licenses/by/4.0/) license.

![pic.png](./images/pic.png)

Buena explicación: https://youtu.be/3_wPH904a_8?t=275

![examples.png](./images/examples.png)

Las piezas son un poco diferentes a las que están en las fotos de thingiverse, pero sirven igual (ver [makes](https://www.thingiverse.com/thing:4544557/makes)). Hice una segunda paanquita para ver si suma algo usar un ejecito de metal de 5 mm.

![printed.png](./images/printed.png)