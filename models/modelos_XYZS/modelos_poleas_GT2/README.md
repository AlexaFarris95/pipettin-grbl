# GT2 idler pulley

Model files:

* Customized OpenSCAD file: [Pulley_T-MXL-XL-HTD-GT2_N-tooth.scad](./parametric_pulley/Pulley_T-MXL-XL-HTD-GT2_N-tooth.scad)
* Rendered and exported in FreeCAD: [idler_GT2_20T.FCStd](./idler_GT2_20T.FCStd)

Original OpenSCAD file published by `droftarts` at [Thingiverse](http://www.thingiverse.com/thing:16627) (see [LICENSE.txt](./parametric_pulley/LICENSE.txt) and [SOURCES.txt](./parametric_pulley/SOURCES.txt)).

Exported models:

* Plain idler: [idler_GT2_20T_liso_10.5_OD.stl](./exported_models/idler_GT2_20T_liso_10.5_OD.stl)
* Toothed idler: [idler_GT2_20T.stl](./exported_models/idler_GT2_20T.stl) (may be defective, read on).

## With teeth

**WARNING**: The original model does not fit my GT2 belts exactly, causing them to slip once in a while. I am now using a plain idler, with no teeth instead. See: [idler_GT2_20T_lisa.FCStd](./idler_GT2_20T_lisa.FCStd) and [issue 55](https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/55).


![pulley.png](./images/pulley.png)

> GT2 idler.

## Plain idler

![pulley_lisa.png](./images/pulley_lisa.png)

> Tooth-less idler, see [issue 55](https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/55).