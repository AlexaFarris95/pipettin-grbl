# Models for the pipette tool

Models in this directory are for the printed parts of the Gilson p200 pipette adapter and actuator.

Exported models:

* [eje_S.stl](./exported_models/eje_S.stl): base parts of the adapter.
* [gilson_tip_probe_p20.stl](./exported_models/gilson_tip_probe_p20.stl): tip probe parts for the p20 micropipette.
* [gilson_tip_probe_p200.stl](./exported_models/gilson_tip_probe_p200.stl): tip probe parts for the p200 micropipette.

Contents:

[[_TOC_]]

To-do:

- [ ] Update model screenshots in this readme.

----

![p20_tool_assembled.jpg](./images/p20_tool_assembled.jpg)

> Current prototype.

## Pipette tool adapter

### Base models

Basic models for old and new pipette adapters are here: [`eje_S.FCStd`](./eje_S.FCStd)

![base_parts.png](./images/base_parts.png)

### Assemblies

* [eje_S_assembly3.FCStd](./eje_S_assembly3.FCStd): part assembly.

![03-eje_S-assembly3.png](./images/03-eje_S-assembly3.png)

* [eje_S_assembly3_nobase.FCStd](./eje_S_assembly3_nobase.FCStd): same, without the blue baseplate.

![eje_S_assembly3_nobase.png](./images/eje_S_assembly3_nobase.png)


## p200 and p20 tip probe

This system uses 3D-printed parts and an opto-endstop to place tips in the fixed-depth way (constant pressure woudl be the other way, but was harder to achieve).

There are tip probes for to pipettes:

* Gilson p200. 
* Gilson p20.

All models are here:

* [`gilson_tip_probe.FCStd`](gilson_tip_probe.FCStd)

![p20_tip_probe.png](./images/p20_tip_probe.png)

> p20 (left) and p200 (right) pipette tip probes.

![](images/02-tip_probe.png)

> First prototype probe, for the p200 pipette.

# Deprecated models

We no longer rely on these models.

## Servo gears

Estos engranajes servían para eyectar el tip de las micropipetas, en la versión anterior del robot. Ver archivos en [modelos_servo](./modelos_servo).

Actualmente usamos un "tip ejection post", que está documentado en el [README.md](../../modelos_toolchanger/README.md) de la carpeta [modelos_toolchanger](../../modelos_toolchanger).

### Gear

Modelo final en [freecad_base_models](./modelos_servo/freecad_base_models):
* [README.md](./modelos_servo/freecad_base_models/README.md)
* [servo_gear_fixation.FCStd](./modelos_servo/freecad_base_models/servo_gear_fixation.FCStd)

Gear base en OpenSCAD en [scad_gear_models](./modelos_servo/scad_gear_models):
* [README.md](./modelos_servo/scad_gear_models/README.md)
* [gears.scad](./modelos_servo/scad_gear_models/gears.scad)

![gear.png](./modelos_servo/freecad_base_models/images/gear.png "gear.png")


# To-Do

- [ ] Consider adding Pipette Jockey's Dragon Lab pipette adapter!
    - https://youtu.be/BhQub5Xh_8o?t=1668

