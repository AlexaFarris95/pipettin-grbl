# Models for XYS axes and the p200 Pipette tool

Machine axes overview:

* XY axes are belt driven for speed.
* Z axis is driven by a lead-screw for strength (essential for tip placement).
* S axis is driven by a lead-screw for precision (essential for pipetting).

![assembly3_tooclhanger.png](./images/assembly3_tooclhanger.png)

> I modeled everything from scratch for learning purposes :) but most of the XYZ stuff should be generic, in the sense that it should be replaceable by any 3 axis CNC "frame".

----

Table of contents:

[[_TOC_]]

-—

## Printable models

Have a look at the [printable_models](./printable_models) subdirectory, and its [README.md](./printable_models/README.md) file.

Most of them exported from part assemblies in FreeCAD.

## Part assemblies

### Assembly3

Usé Assembly3 en FreeCAD weekly: `FreeCAD_weekly-builds-29485-2022-07-11-conda-Linux-x86_64-py310.AppImage`.

Los assemblies están en varios archivos de FreeCAD que dicen "assembly3". Tienen assemblies hechas con ese plugin. Para usarlo tuve que bajar un weekly de FreeCAD 0.2 e instalar el plugin de la forma estandar. Links:

* https://wiki.freecadweb.org/Release_notes_0.20
* https://wiki.freecadweb.org/AppImage
* https://github.com/FreeCAD/FreeCAD-Bundle/releases/tag/weekly-builds
* [FreeCAD\_weekly-builds-29485-2022-07-11-conda-Linux-x86\_64-py310.AppImage](https://github.com/FreeCAD/FreeCAD-Bundle/releases/download/weekly-builds/FreeCAD_weekly-builds-29485-2022-07-11-conda-Linux-x86_64-py310.AppImage)

> Nota: tuve que usar el appimage porque Assembly3 en FreeCAD 0.2 no anda en mi Arch Linux con Python 3.10 (porque es demasiado nuevo). El AppImage anda joya.

Los archivos importantes para el assembly con este plugin son:

* [assembly3_toolchanger.FCStd](./assembly3_toolchanger.FCStd): assembly del robot multi-herramienta.
* [assembly3.FCStd](./assembly3.FCStd): assembly del robot de 1 herramienta.
    * En assembly3 se puede chequear de que archivo es cada parte (seleccionar, click derecho, link actions, go to linked object) por ejemplo pueden llegar a estos otros archivos.
    * [eje_XYZ_assembly3.FCStd](./freecad_base_models/eje_XYZ_assembly3.FCStd)
    * [idler_GT2_20T.FCStd](./modelos_poleas_GT2/idler_GT2_20T.FCStd)
    * [eje_S.FCStd](./freecad_pipetting_models/eje_S.FCStd)
    * [gt2_rack.FCStd](./modelos_racks_GT2/gt2_rack.FCStd)
    * y otros...

#### Multi-tool

Este assembly es el actual.

![assembly3_tooclhanger.png](./images/assembly3_tooclhanger.png)

Más abajo están algunas capturas de los assemblies del robot de una pipeta. Las dejé porque tienen info que tengo que pasar en limpio.

#### Single-tool

Una assembly más completa:

![assembly3_colors.png](./images/assembly3_colors.png)
![assembly3_colors2.png](./images/assembly3_colors2.png)


En el screenshot a continuación se ven las medidas de las barras de 8 mm y 12 mm (ambos 50 cm), y de la mesa perforada (46cm x 54 cm). Nota: las piezas de la herramienta y del toolchanger no se muestran.

![assembly3.png](./images/assembly3.png)

The assemblies is for illustration and checking purposes. The following parts are missing:

- [ ] GT2 belts, threaded rods, bearings, etc.
- [ ] All cable carrier links.
- [ ] All electronics.

The following drawing shows the places where some of these parts should go, please compare with pictures of the assembled machine.

![assembly3_colors_doodles2.png](./images/assembly3_colors_doodles2.png)

> XY belts (green), cable carriers for the tool (cyan), ZX axes (yellow) and Y axis (red), and electronics (white, likely place).

## FreeCAD models

### XYZ frame models: [`freecad_base_models`](./freecad_base_models)

Los modelos base de los ejes XYZ se parametrizan en FreeCAD, ver [`freecad_base_models`](./freecad_base_models) y su [README.md](./freecad_base_models/README.md).

* `eje_XYZ.FCStd`: all models for the XYZ axes (unassembled).
* `eje_X_belt_tensioner.FCStd`: structure for the XY belt tensioners (for holding bearings and a rod).

![eje_XYZ_assembly3.png](./freecad_base_models/images/eje_XYZ_assembly3.png)

Deprecated:

* `trabitas_gt2.FCStd`: lids for pressing GT2 belts onto the corresponding "locking teeth" part.

### Pipette models: [`freecad_pipetting_models`](./freecad_pipetting_models)

Models for the pipette holder, actuator, and tip probe.

The pipette adapter and actuator parts are in the [freecad_pipetting_models](./freecad_pipetting_models) directory, see its [README.md](./freecad_pipetting_models/README.md).

Renders:

![03-eje_S-assembly3.png](./freecad_pipetting_models/images/03-eje_S-assembly3.png)

> Pipette tool holder and actuator (orange), slot for the cable carrier base/bearing (green), tip probe (red), and tool basepate (blue). Note that the parts for the tip ejection servo are missing, because it is no longer used.

![](freecad_pipetting_models/images/02-tip_probe.png)

> Pipette tool "tip probe" (the optostop sensor is mounted in the red coloured part).

The full pipette axis toolhead is the part shown below. On top are drawn the pipette (left, in red), the two steel rods (blue) that hold everything in place, and the pipette's stepper (right, in red).

![03-freecad_assembly_Saxis.png](./images/03-freecad_assembly_Saxis.png)

> Not drawn: the lead screw actuating the pipette, the tip-ejection servo (now deprecated), and a nose on the smiley face (yellow).

### Cable carrier links y bases: [`freecad_cablecarriers`](./freecad_cablecarriers)

Ver [freecad_cablecarriers](./freecad_base_models/freecad_cablecarriers) y su [README.md](./freecad_base_models/freecad_cablecarriers/README.md).

![cable_bases_assemblies.png](./freecad_base_models/freecad_cablecarriers/images/cable_bases_assemblies.png)

> Cable carrier module (left) and "bases" (right) which are lated mounting on bearings.

![cable_bases.png](./freecad_base_models/freecad_cablecarriers/images/cable_bases.png "cable_bases.png")

> Cable carrier "bases" for the tool/S-axis (bottom), X-axis (mid-top and top-right pieces) and Y-axis (top left).

### GT2 belt tranmission parts

#### Pulley idler: [`modelos_poleas_GT2`](./modelos_poleas_GT2)

Polea para tensar correas GT2 de los ejes Y y X. En dos variantes, una lisa y una dentada.

Ver [README.md](./modelos_poleas_GT2/README.md) en [modelos_poleas_GT2](./modelos_poleas_GT2).

![pulley.png](./modelos_poleas_GT2/images/pulley.png)
![pulley_lisa.png](./modelos_poleas_GT2/images/pulley_lisa.png)

> GT2 20T pulley idler, for a GT2 6mm width belt.

Originalmente las poleas para tensar las correas se bajaron de thingiverse, y se modificaron [en TinkerCAD](https://www.tinkercad.com/things/aSPLpupQbbE-copy-of-pippetin-grbl-v096/edit), pero todo lo de TinkerCAD está ahora obsoleto.

#### Pulley idler: [`modelos_racks_GT2`](./modelos_racks_GT2)

Pieza para aferrar correas GT2 a los carritos de los ejes Y y X.

Ver [README.md](./modelos_racks_GT2/README.md) en [modelos_poleas_GT2](./modelos_racks_GT2).

![rack_and_lid.png](./modelos_racks_GT2/images/rack_and_lid.png)

# Older models

## Old TinkerCAD models

Antes hacía todo en TinkerCAD, Las [partes](https://www.tinkercad.com/things/aSPLpupQbbE-copy-of-pippetin-grbl-v096/edit) que faltaba portear ya están todas en FreeCAD :)

**Deprecation note**: estos archivos ya se pueden borrar.

Ver el [README.md](./old_models/tinkercad_exports/README.md) en [tinkercad_exports](./old_models/tinkercad_exports).

## Old thingiverse belt-locks

**Deprecation note**: estos archivos ya se pueden borrar.

Ver el [README.md](./old_models/thingiverse_belt_locks/README.md) en [thingiverse_belt_locks](./old_models/thingiverse_belt_locks).

# Changelog and version comments

See: [CHANGELOG.md](./CHANGELOG.md)

# To-do

Agregar al readme info sobre:

- [ ] Perfiles 2020 y el frame.
- [ ] Mesa de trabajo.
- [ ] Modelos NEMA 17
- [ ] ...?

# Opinión: A2plus vs Assembly3

Es un poco diferente a A2plus, quizás un poco más fácil de usar para empezar, es tan simple como "agregar" partes de otros archivos usando el botón correspondiente.

Assembly3 no tiene offsets para los "coincident planes" ni la opción de "invertir" la dirección de un constraint, que era bastante útil en A2plus. Otras cosas son un poco más razonables, pero no se si vale la pena cambiarse.

Lo que anda mucho mejor es la propagación de cambios en assemblies chicos, a los assemblies más grandes que los usan. No es perfecto pero agiliza mucho respecto a A2plus, que al actualizar una parte fallaba horriblemente (y había que armar las constraints desde cero).

Otra ventaja de Assembly3 es que se pueden editar los archivos importados desde el mismo assembly, y los cambios se guardan en el archivo original. Tambien se puede ir aver el archivo original con el menú de links, haciendo click derecho sobre la parte.

Últimamente tuve problemas con Assembly3, los links se desconfiguran cuando abro el archivo, y se arreglan solo cuando voy abriendo cada uno de los archivos manualmente. Muy molesto.

Además, las constraints a veces dicen ser inconsistentes, pero se resuelven después de abrir y cerrar FreeCAD. Un misterio.

No probé Assembly4.

## A2plus assembly (deprecated)

Reference part assembly in FreeCAD: [assembly.FCStd](./assembly.FCStd)

Model exports at [printable_models](./printable_models).

> I moved to assembly3.
