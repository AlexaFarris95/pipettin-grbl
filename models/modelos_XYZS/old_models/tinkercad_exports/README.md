# TinkerCAD exports

Initially all parts were modelled in TinkerCAD (assemblies included). I dont know why I thought that was a good idea _hehe_.

Many have all been replaced by FreeCAD models and assemblies (see other directories).

The following remain:

* [v0.99/Pippetin-grbl v0.99 - belt_stuff.stl](./v0.99/Pippetin-grbl v0.99 - belt_stuff.stl)
* [v0.99/Pippetin-grbl v0.99 - cable_management_stuff.stl](./v0.99/Pippetin-grbl v0.99 - cable_management_stuff.stl)

Content reference for the image below:

* GT2 20T pulleys for tensioning for an 8mm shaft (bottom-left): 
* GT2 belt "fasteners" (top, top-left, and mid-right).
* Cable carriers (bottom-left).
* Cable carrier "bases"; custom pieces needed at the ends of the carrier (bottom-right and bottom-mid).

![tinkercad_stuff.png](./images/tinkercad_stuff.png)

## Assembly (deprecated)

Antes los modelos base se ensamblaban en TinkerCAD y se exportaron a la carpeta `tinkercad_exports/`.

Most models are now created directly in FreeCAD, but the "verification by assembly" is still in Tinkercad:

* https://www.tinkercad.com/things/aSPLpupQbbE-copy-of-pippetin-grbl-v096/edit

This will change in the near future!

![Image not found: images/02-tinkercad_assembly.png](images/02-tinkercad_assembly.png "Image not found: images/02-tinkercad_assembly.png")

### Auxiliary workspaces

Cable carrier base parts: https://www.tinkercad.com/things/3vgmXZh5ky1-base-pasacables/edit

Stepper/servo mount reemplazado! https://www.tinkercad.com/things/cFK7Cm1zh40-sizzling-tumelo-uusam/edit

## Previous versions

Links para editar:

* < v0.7.5: https://www.tinkercad.com/things/fmZqSN6cHmf-pippetin-grbl-v053/edit
* < v0.9.6: https://www.tinkercad.com/things/4lBHoceIFgf-pippetin-grbl-v096/edit
* v0.99: https://www.tinkercad.com/things/aSPLpupQbbE-copy-of-pippetin-grbl-v096/edit
  * Auxiliary workspace: https://www.tinkercad.com/things/lQpzvFQYTJL-powerful-blad/edit

Cosas servo:

* Viejo: https://www.tinkercad.com/things/2ZC4g48iKkl-servo-pipette-dock/edit
* Nuevo: https://www.tinkercad.com/things/4lBHoceIFgf-pippetin-grbl-v096/edit

Cosas tip probe:

* https://www.tinkercad.com/things/jhh7ATe5kqK-gilson-pipette-grbl-probing-accessory-for-tips/edit

Cosas ventilador Arduino:

* https://www.tinkercad.com/things/dVSJR8i45g9-unor3basecncshieldbasewithfaninverted/edit
* Creo que estos modelos deberian ir en la parte de electronica.