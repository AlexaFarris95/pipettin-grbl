# GT2 belt models 

From this directory only some models remain in use (in the [TinkerCAD assembly](../tinkercad_exports/README.md), not yet ported to FreeCAD):

* [polea_gt2_20t.stl](./polea_gt2_20t.stl)
* [Y_carriage_belt_holder_B.stl](./Y_carriage_belt_holder_B.stl)
* [Y_carriage_belt_holder_C.stl](./Y_carriage_belt_holder_C.stl)

The rest are attempts to fasten the belts with more "advanced" models, that turned out to be unnecesary.

![preview.png](./images/preview.png)

Most were taken from Thingiverse, credit to the authors! (see `modelos thingiverse/`).
