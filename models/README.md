# 3D Models directory index

Find herein 3D all printable models to make our CNC frame and exchangeable tool adapter for the pipetting robot.

Design files are distributed under the terms of the [CERN-OHL-S](https://gitlab.com/pipettin-bot/pipettin-grbl/-/blob/master/LICENSE.md#hardware) licence.

----

Table of contents:

[[_TOC_]]

----

## XYZ and tool axes

This directory holds models for the structure of:

* A generic 3-axis XYZ/cartesian CNC frame.
* Exchangable pipetting "tools".

This is by far the most complex subdirectory with respect to models, but most are hopefully well organized in larger FreeCAD files. See the [modelos_XYZS](./modelos_XYZS) directory and its [README.md](modelos_XYZS/README.md) for details.

![assembly3_tooclhanger.png](./modelos_XYZS/images/assembly3_tooclhanger.png)

### Structural frame

Visit the [modelos_XYZS/freecad_base_models/alu_frame](./modelos_XYZS/freecad_base_models/alu_frame) directory and its [README.md](./modelos_XYZS/freecad_base_models/alu_frame/README.md).

> Note: these parts must be cut and machined, and are not meant for 3D-printing.

![alu_frame2_as3.png](./modelos_XYZS/freecad_base_models/alu_frame/images/alu_frame2_as3.png)

### Cable management

Cable carriers, originally in OpenSCAD format, re-modelled in FreeCAD.

See the [README.md](./modelos_XYZS/freecad_base_models/freecad_cablecarriers/README.md) and models at [`modelos_XYZS/freecad_base_models/freecad_cablecarriers`](./modelos_XYZS/freecad_base_models/freecad_cablecarriers).

> Note: these parts tolerate a small amount of "elephants foot" aberration in 3D-printers, but will print better with a well calibrated first layer, or with appropriate corrections.

![cable_bases_and_basic_connectors.png](./modelos_XYZS/freecad_base_models/freecad_cablecarriers/images/cable_bases_and_basic_connectors.png "cable_bases_and_basic_connectors.png")

> These are adapted from original OpenSCAD designs posted at Thingiverse by [Gasolin](https://www.thingiverse.com/thing:4375470).

### Pipette tool adapter

See the [README.md](./modelos_XYZS/freecad_pipetting_models/README.md) file at [modelos_XYZS/freecad_pipetting_models](./modelos_XYZS/freecad_pipetting_models).

![03-eje_S-assembly3.png](./modelos_XYZS/freecad_pipetting_models/images/03-eje_S-assembly3.png)

### GT2 transmission

Ver:

* [README.md](./modelos_XYZS/modelos_poleas_GT2/README.md) en [modelos_poleas_GT2](./modelos_XYZS/modelos_poleas_GT2).
* [README.md](./modelos_XYZS/modelos_racks_GT2/README.md) en [modelos_racks_GT2](./modelos_XYZS/modelos_racks_GT2).

![pulley.png](./modelos_XYZS/modelos_poleas_GT2/images/pulley.png "pulley.png")

> Pulley idler.

![new_racks_small.png](./modelos_XYZS/modelos_racks_GT2/images/new_racks_small.png)

> Belt locks.

## Tool-changer

> New!

There are two tool-changer implementations available in this repo:

* One based on the mini-latch (this has been tested and works well enough).
* Another one inspired on the mecanism of the Getit 3D printer (in active development, not tested yet).

See the [README.md](./modelos_toolchanger/README.md) file at [modelos_toolchanger](./modelos_toolchanger) for more details.

![toolchanger_idea_magnets_and_push_latch_v2.png](./modelos_toolchanger/images/toolchanger_idea_magnets_and_push_latch_v2.png)

> Mini-latch mechanism.

![toolchanger_screenshot_getit.png](./modelos_toolchanger/images/toolchanger_screenshot_getit.png)

> Inspired by the Getit printer mechanism.


## Tip-ejection post

Pipette tips are now ejected by a slow collision with the tip-ejection post:

Find models here [common_parts](./modelos_toolchanger/pipes_toolchanger/common_parts).

![post.png](./modelos_toolchanger/pipes_toolchanger/common_parts/images/post.png)

## Baseplate

Models for the baseplate, where protocol objects can be consistently placed in the same XYZ position, relative to the rest of the machine. See the [README.md](./modelos_baseplate/README.md) file at [modelos_baseplate](./modelos_baseplate).

![second_version_model.png](./modelos_baseplate/images/second_version_model.png)

## Electronics

Cases and holders for the "electronic" parts, including:

* End-stops mounts for 8mm and 12mm rods: [arandela_end_stop_8mm_y_12mm.FCStd](./modelos_electronica/modelos_end_stop/freecad_base_models/arandela_end_stop_8mm_y_12mm.FCStd)

See the [modelos_electronica/README.md](modelos_electronica/README.md) file at  [`modelos_electronica`](./modelos_electronica).

![arandela_end_stop_8mm_y_12mm.png](./modelos_electronica/modelos_end_stop/freecad_base_models/images/arandela_end_stop_8mm_y_12mm.png)

Other models, not currently used:

* Cooler fan base for the Arduino UNO with CNC shield.
* Others.

# Extra models

Models in this section are not currently in use, but might be helpful.

Old models around pipettes and syringes: [old_models/modelos_pipetas_jeringas](./old_models/modelos_pipetas_jeringas).

## 3D-printable slider nuts and couplings for 2020-profiles

Models for coupling 2020 and 3030 aluminium profiles, and extra pieces for leveling.

See the [modelos_marco_aluminio/README.md](modelos_marco_aluminio/README.md) file at [`modelos_marco_aluminio`](./modelos_marco_aluminio).

![03-pasantes_2020.png](./modelos_marco_aluminio/images/03-pasantes_2020.png)

![acople](modelos_marco_aluminio/images/02-acople_2020_3030.png)


# To-do

Check general issues at https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues

And documentation issues at https://gitlab.com/pipettin-bot/pipettin-grbl-docs/-/issues
