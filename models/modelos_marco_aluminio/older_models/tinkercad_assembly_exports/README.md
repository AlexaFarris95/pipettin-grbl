# Older models

Older TinkerCAD models.

## 2020-feet and 2020-3030 adapter

Estos modelos siguen en Tinkercad, no los pasé a FreeCAD.

Ya no son necesarios en el último assembly.

![not_ported.png](./images/not_ported.png)

Adjustable feet (white printed parts):

![pie](images/small/01-pie.png)

2020-3030 adapter (blue arrow):

![02-acople_2020_3030_arrow.png](./images/02-acople_2020_3030_arrow.png)
