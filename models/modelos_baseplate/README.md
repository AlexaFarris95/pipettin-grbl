# Baseplate models

The baseplate connects the machine's structural frame to the protocol objects (i.e. tube racks, tip racks, etc.).
It is no more than a "table" with regular holes, and _curbs_ as a way to align objects to it.

Parts:

* Baseplate: [baseplate_assembly3-mesa2.stl](./exported_models/baseplate_assembly3-mesa2.stl)
    * This part was machined out of a laminate wood piece, 9 mm in depth.
* Crubs:
    * [baseplate_assembly3-module1_corner.stl](./exported_models/baseplate_assembly3-module1_corner.stl)
    * [baseplate_assembly3-module1_straight.stl](./exported_models/baseplate_assembly3-module1_straight.stl)

<!-- ![baseplate.png](./images/baseplate.png) -->

![pic.jpg](./images/pic.jpg)

## Explanation

### Curbs

Using a regularly spaced, square grid of 6 mm holes, "curb" modules are placed to generate guides on the workspace.

Those guides can be used to align objects relative to the machine's coordinates, both easily and consistently.

![modulitos2.png](./images/modulitos2.png)

Modules: two modules are shown, they can be combined to make larger guides.

![modulitos.png](./images/modulitos.png)

### Material

This laminated wood is not super smooth, but it is cheap and it does the job.

![second_version_model.png](./images/second_version_model.png)

> The baseplate is a perforated piece of wood with some curbs fixed to it (white and yellow parts). Machined 9 mm thickness laminated wood, barnished for water resistance. 

### Frame-mount

A simple but effective 3-point kinematic coupling was used.

The baseplate will sit on the aluminium frame, through the heads of three ["round head" screws](https://www.google.com/search?q=screw+head+styles), placed on the bottom side of the baseplate (see pictures below).

The screws could eventually be used to level the baseplate.

Since the aluminium frame is v-slotted, this system is a ["Maxwell" kinematic coupling with 6 contact points](https://en.wikipedia.org/wiki/File:Kelvin_Kinematic_Coupling.png) (which means that it can only fit in one position and with one orientation, its movement is totally constrained over the horizontal plane).

![3_pt_kinematic_coupling.png](./images/3_pt_kinematic_coupling.png)

> Three M4, round-headed, philips screws were placed on the marked holes, "from below" (such that the head of the screws lies on the bottom side of the baseplate).

![3_pt_kinematic_coupling_frame.png](./images/3_pt_kinematic_coupling_frame.png)

> The three screws on the underside of ths baseplate rest on the v-slots of the marked aluminium profiles. Because two of the contact points are on the same v-slot (and thus are parallel), the third contact point can always be aligned with the third screw.

### Stability

This design, however, is unstable when downward pressure is exherted outside (magenta arrow) of the virtual triangle (yellow lines), drawn between the three contact points (red circles). That is, the table could "tilt". On the contrary, downward forces excherted inside the triangle (green downward arrow) do not generate instabilities.

To solve this, the M4 are screwed into the baseplate, untill their heads hit the surface. Because the screwheads sink a bit into the v-slots of the profiles, this results in a very small space between the baseplate and the top side of the aluminium profiles. It is **curcial** that this small (circa 0.2 mm) but not zero.

In this setup, any pressure outside of the triangle will deform the baseplate very sligtly, and this will put it in contact with the profiles, thereby adding support to it, and removing the instability issue.

![stability.png](./images/stability.png)

> Internal triangle.

![points.jpg](./images/points.jpg)

> Implementation.

### Alternative

3-point kinematic coupling with "split" third point: https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/48

This can produce a stable configuration, but it is harder to _make_; all dimensions must bepretty exact.

![3_pt_kinematic_coupling-split_3rd.png](./images/3_pt_kinematic_coupling-split_3rd.png)

## First ideas

* A wooden baseplate that is always placed in the same position relative to the machine (it would be best if it were removeable).
* The baseplate should have a system of "stops" or slots that consistently aligns objects on the same spot of the baseplate.

The system below is based on simple pairs of corner pieces: one stiff and one elastic. The tip and tube racks align well enough to them.

Ideally there would be more places to fix things, and they should be as object-agnostic as possible.

![first_version.jpg](./images/first_version.jpg)

Corners: https://www.tinkercad.com/things/bkN5FanJTbc-topes-pipetting-grbl-platforms/edit?sharecode=7mTgQmolDguW5YVhoKrvYynJvYVMCshlfleS4CLZQGE

![tinkercad_design.png](./images/tinkercad_design.png)