Peido a "Quemando", según sus especificaciones: http://www.quemando.com.ar/v3/index.php/corte-laser/

# Archivo de corte

Se removieron lineas duplicadas.

![nicolas_mendez_acrilico_transparente_3mm.svg](./pedidos/nicolas_mendez_acrilico_transparente_3mm.svg)
# Partes

Exportadas como "flattened SVG" de [enclosure.FCStd](../enclosure.FCStd)

![enclosure-base.svg](./enclosure-base.svg)

![enclosure-lid.svg](./enclosure-lid.svg)
![enclosure-side.svg](./enclosure-side.svg)
