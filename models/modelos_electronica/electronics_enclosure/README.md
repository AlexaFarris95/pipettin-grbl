
# Electronics enclosure

## Acrylic box

Parameters:

* 3 mm wall thickness.
* 220 mm (total width).
* 100 mm (total depth).
* 50 mm (total height).
* T-slots for binding sides.

Model: [enclosure.FCStd](./enclosure.FCStd)

![enclosure.png](./images/enclosure.png)

Result:

![first_build.jpeg](./images/first_build.jpeg)

### Mount

Use two of the red part to fix the encolsure to a 2020 profile, using slider nuts beneath.

![tope_perfil.png](./images/tope_perfil.png)

### Mounting holes dimensions

Raspberry Pi 4B: [Raspberry Pi 4 Model B.zip](./reference_models/Raspberry Pi 4 Model B.zip)

![raspberry_pi_dims.png](./images/raspberry_pi_dims.png)

> Raspberry Pi 4B

Arduino UNO: [arduino_hole_dimensions.pdf](../cnc_shield_holder/images/arduino_hole_dimensions.pdf)

![dimensions.png](../cnc_shield_holder/images/dimensions.png)

> Arduino UNO

## Box generators

Used for concept models only, final models made in FreeCAD.

* https://www.instructables.com/The-Ultimate-Guide-to-Laser-cut-Box-Generators/
* https://www.makercase.com/#/basicbox

# Exporting models to SVG

1. Create a TechDraw page and view for each shape.
2. Export each page as a "Technical Drawing" to SVG format.
3. Edit each SVG to remove all duplicate shapes and lines, using standard Inkscape tools such as:
    - Path / Combine
    - Path / Difference
    - Nodes tool / Delete segment between nodes
    - Nodes tool / Join selected nodes
    - …
4. Place parts against each other, in order to share as many edges as possible.
    - Delete the shared edges with the "Nodes tool / Delete segment between nodes" tool.

# Instrucciones de Quemando

Pedido a "Quemando", según sus especificaciones: http://www.quemando.com.ar/v3/index.php/corte-laser/

- Crear un documento con el siguiente formato: `Nombre_apellido_material_color_espesor`.
    - Ej: `Cosme_fulanito_acrilico_blanco_4mm.ai`
- Configurar el Documento, Color de documento: RGB (Illustrator y corel)
- Armar un recuadro con el área de corte máximo de 810 mm x 450mm y distribuir las piezas (Ver el plano de distribución y la tabla de Materiales y espesores).
- Codigo de color para operaciones:
    - **Corte**: Rojo `R 255 G 0 B 0` – Corta el material, lo separa de la placa.
    - **Marcado**: Azul `R 0 G 0 B 255` – Sirve para marcas de posicionamiento, numerar las piezas, detalles, realiza un suave grabado en valor de linea.
    - **Grabado**: Negro Relleno `R 0 G 0 B 0` – sirve para Logos en pleno, Títulos, texturizar un material, etc.
- Stroke/valor de linea/espesor de linea: 0,01 Pt Illustrator, Hairline Corel.
- En el archivo solamente dejar los Layer/capa:
    - corte
    - marcado
    - grabado
- Usar dimensiones en mm. Para trabajos en Autocad No usar el layout para definir el tamaño, solo trabajar en el model a tamaño de impresión.
- Las piezas deben estar dibujadas vectorialmente con los colores correspondientes al tipo de Función.
- Dejar al menos 3 mm libres al margen del borde de la placa.
- Utilizar líneas continuas y curvas cerradas. Al compartir las líneas de corte reducís el tiempo de grabado y el costo.
- Acotar la medida de la placa.
- Si usas Hatch tenes que explotarlos.
- Es importante especificar siempre el color y espesor de los materiales para que no haya errores. En el Alto impacto aclarar si se usa el lado brillante u opaco para arriba.
    - Ej: Cosme_fulanito_acrilico_blanco_4mm.ai
- En materiales tipo alto impacto / acrílico la separación recomendable entre lineas debe ser de al menos 2mm. En ciertos casos se debe dejar mas separación.
    - Ej: Formas donde predomina el largo sobre el alto, repeticiones, etc.
- Tener en cuenta, que las las piezas pequeñas, menores a 7x7mm, pueden perderse al momento del corte; ya que la maquina cuenta con un sistema de extracción de aire.

# Changelog

## 2022-12-08: Lado para 3D y bugfixes

Lado para 3D:

![lado3d.png](./images/lado3d.png)

Fixes:

- Move arduinos 12 mm downwards, because they blocked the RPi USB port.
- Move Arduinos closer together.
    - Add mointing holes for a step down converter, to alternatively power the RPi without USB.

![bugfixes.png](./images/bugfixes.png)

## 2022-12-09: CNC shield ref model

Adjust the fan position using a reference CNC shield model.

![cnc_shield_ref_model.png](./images/cnc_shield_ref_model.png)