# Box for a switch

This "box" holds the power switch against a 2020 profile.

It is open on the back, making it easy to put the cables in.
 
![box.png](./images/box.png)

# Changelog

## 2022-12-17

Added pads and holes to secure the "box" to the 2020 alu frame, using slider nuts.

![Screenshot_20221217_173504.png](./images/changelog/Screenshot_20221217_173504.png)