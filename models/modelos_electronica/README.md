# Modelos electrónica

Find some details on the electronics side here: [doc/electronica](../../doc/electronica)

Deprecated models here: [old_models](./old_models)

## Enclosure

Models for laser-cutting in acrylic.

See [electronics_enclosure](./electronics_enclosure) and its [README.md](./electronics_enclosure/README.md).

![enclosure.png](./electronics_enclosure/images/enclosure.png)

## Base para end-stops

3D-printable parts to "fasten" mechanichal limit switches to the 8mm and 12 mm steel rods.

* directory: [modelos_end_stop/freecad_base_models](./modelos_end_stop/freecad_base_models)
* current models: [arandela_end_stop_8mm_y_12mm.FCStd](./modelos_end_stop/freecad_base_models/arandela_end_stop_8mm_y_12mm.FCStd)

![arandela_end_stop_8mm_y_12mm.png](./modelos_end_stop/freecad_base_models/images/arandela_end_stop_8mm_y_12mm.png)

