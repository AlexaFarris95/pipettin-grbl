These are parts used in FreeCAD assembly files elsewhere, for both Pipe's and Nico's tool-changer (details at [README.md](../../README.md)).

## Tip-ejection post

For now only this model has been used only in the [assembly3_toolchanger.FCStd](../../../modelos_XYZS/assembly3_toolchanger.FCStd) assembly.

Exported model: [poste_expulsor.STEP](./poste_expulsor.STEP)

Source: [Assembly_Pipetadora.zip](../Assembly_Pipetadora.zip)

![post.png](./images/post.png)
