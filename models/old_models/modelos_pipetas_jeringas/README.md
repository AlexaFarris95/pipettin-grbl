# Tool models for liquid handling

**Deprecation note**: All models in this directory are deprecated, and have been replaced by models at the [`/models/modelos_XYZS/freecad_pipetting_models`](../modelos_XYZS/freecad_pipetting_models) directory.

Only miscelaneous models remain here:

## [topeJeringa1mL](./topeJeringa1mL)

![image.png](./topeJeringa1mL/image.png)

## [pipetman](./pipetman)

Intentar modelar una pipeta Gilson, sin éxito.

Ver: [README.md](./pipetman/README.md)

## [gilson_vastago_p200](./gilson_vastago_p200)

![image.png](./gilson_vastago_p200/image.png)

