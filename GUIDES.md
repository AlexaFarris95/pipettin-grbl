> This content is not yet in a "readable" state.

Table of contents:

[[_TOC_]]

# Development

> TO-DO

<!--
3) Guia de contributing/developer (todo lo que hay hoy en los readme basicamente)
 - Explicacion de la arquitectura
 - Explicacion de cada modulo (protocol2gcode, gui, etc) y como usarlos y correrlos por separado
 - Como instalar los requires (mongo, python, etc) desde 0 en una raspberry pelada u otro linux
-->

## Overview

A conceptual overview of the main components and their interactions:

- A graphical web interface made in Node.js is served by a Raspberry Pi 4.
    - The web app allows users to build workspaces and protocols, through a friendly UI.
    - The app's backend interacts with a python module through a websocket.
    - The python module translates the protocols to GCODE, and operates the machine.
- The Raspberry Pi runs Raspbian OS, and operates:
  - An Arduino running GRBL through USB, and a hard-reset circuit.
  - Two stepper drivers, which displace the pipette's shaft.
  - Two mechanical endstops to home the pipette's steppers.
  - Two opto-endstops for correct tip placement.
- The Arduino runs the [GRBL 1.1h firmware](https://github.com/gnea/grbl/), and operates:
  - A basic 3-axis CNC machine (which moves the pipette around).
  - Two end-stop probes for tool parking.

Background services are managed by `systemd` user units `commander.service` and `nodegui.service` (both defined at `/home/pi/.config/systemd/user`, enabled by default).


![graphic_overview](doc/media/overview.svg)





## Software architecture

1. The GUI generates "workspaces" by using platforms as "templates" and filling the `content` of them regarding user definitions in the UI.
2. Then the user defines a "protocol" for that workspace (a sequence of operations over objects in the workspace). The GUI translates the protocol into "High Level Commands" as representations of each operation or group of operations.
3. The "protocol2gcode" python3 module interprets these commands sequentially, and streams the necessary GCODE to the hardware firmware (modified GRBL 1.1h).

Templates for the definitions (of workspaces, protocols, and platforms) are in JSON format. Those must be imported to your local mongodb server (learn more at the [protocol2gcode](#protocol2gcode) section).

## Software modules

### GUI

Express.js webapp to create workspaces, contents and protocols.

### protocol-builder

JS library that interprets the user input, and generates protocol.json files.

### workspace-builder

JS library that interprets the user input, and generates workspace.json files.

### protocol2gcode

Python library that interprets protocol files and generate/stream GCODE to grbl.

After the GUI defines workspace, platforms and protocol, it saves all elements it in the mongodb database.
Then, it calls this python library to interpret it, and generate/stream the GCODE to grbl.

The library runs as a systemd service, and communicates with the GUI through SocketIO.
It can be used without the GUI, as long as the workspace objects are in the Mongo database.

See [protocol2gcode/README.md](protocol2gcode/README.md) and [defaults/README.md](defaults/README.md) for instructions on setting up testing without the GUI, using the default JSON templates.

## Arduino Firmware

GRBL was configured in "gantry" mode: two independent steppers and end stops for the Y axis.

## Hardware

Hardware _at a glance_.

### Electronics

* Arduino UNO with CNC Shield 3.0 (clone) and limit switches.
* Raspberry Pi 4.
* Pololu stepper drivers.
* Two buck converters, a capacitor and a transistor.
* Power supply.

Se details at the dedicated [documentation](doc/electronica/README.md) page.

### Structure and Mechanics

Our designs for parts are still being tested, find them in the "models" directory. See the README.md files therein.

Since the micropipette does not need as much structural stiffness in the XYZ axes as a spindle CNC machine, any CNC or 3D printer structure would work just fine.
However, it should withstand the force needed to place a pipette tip.

I other words, you are not bound to use our models, except for the pipette tool adapter (also refered to as the "S axis" elsewhere), attached to the CNC's head.


# Assembly guide

Assembly, set-up, and usage **documentation** is being written using [GitBuilding](https://gitbuilding.io/), and is accesible online through the following links:

* Published documentation (WIP): https://pipettin-bot.gitlab.io/pipettin-grbl-docs/
* Documentation sources: https://gitlab.com/pipettin-bot/pipettin-grbl-docs

Instructions to build and set-up the robot:

- Assembly: https://pipettin-bot.gitlab.io/pipettin-grbl-docs/guides/03_assembly_guide.html
- Setup and calibration:
    - https://pipettin-bot.gitlab.io/pipettin-grbl-docs/guides/usage/01_setup.html
    - https://pipettin-bot.gitlab.io/pipettin-grbl-docs/guides/usage/02_calibration.html

Assembly guide available [here](https://pipettin-bot.gitlab.io/pipettin-grbl-docs/) (WIP).

<!--
2) Guia de Construccion
 - Guia completa para armar el hardware
-->

# Software installation

Instructions to install and configure the software.

Currently a bit scattered:

- [GUIDES.md](./GUIDES.md)/Software installation
- https://pipettin-bot.gitlab.io/pipettin-grbl-docs/guides/assembly/03_electronics.html

Installation notes for all the required software/firmware.

## Raspberry Pi

<!-- Guia Instalacion de imagen raspberry 

Estaria bueno poner algo sobre resizear los .img y filesystems, 
para poder poner la imagen en tarjetas SD más pequeñas.
Por ejemplo: https://github.com/Drewsif/PiShrink
Tutorial muy detallado: https://raspberrypi.stackexchange.com/a/56312
-->

Requirements:

* The system's image, find it [here](https://drive.google.com/drive/folders/1sQWp9x0S_202jgzFJBe-YqoJQlnTlY16?usp=sharing).
* 16-64 GB micro SD card.
* Standard command-line tools in a GNU/Linux OS.

### Method 1: Burn the OS image

- [ ] To-do: re-upload this ISO after cleaning it up.

#### gunzip (old method)

Restore our SD card image to a 64 GB SD card using `dd` and `gunzip` from a terminal.

* Replace `YOUR_SD_CARD_DEVICE` above with the path to your SD card, found under `/dev` (for example `/dev/sdx`, as such, without the final slash).
* To find the path to your SD card: unplug your card, then run `lsblk`, plug in your card, and run `lsblk` again, and compare the outputs. The new device should be your SD card.
* **Warning**: If you get the path wrong, you may cause irreparable damage to your computer's filesystem.

```bash
# Replace "YOUR_SD_CARD_DEVICE" with your SD card's actual device name (you may use "lsblk" to find it).
gunzip --stdout pipettin_pi.img.gz | sudo dd bs=4M status=progress of=/dev/YOUR_SD_CARD_DEVICE
sync
```

Details at: https://www.raspberrypi.org/documentation/linux/filesystem/backup.md

#### pishrink ISO (new method)

We have used `pishrink` on out system image, reducing it to 13 GB. You can use any card with 16 GB or more, yay!

1. Insert the SD card into your computer.
2. Run the command below:
    * Replace `YOUR_SD_CARD_DEVICE` above with the path to your SD card, found under `/dev` (for example `/dev/sdx`, as such, without the final slash).
    * To find the path to your SD card: unplug your card, then run `lsblk`, plug in your card, and run `lsblk` again, and compare the outputs. The new device should be your SD card.
    * **Warning**: If you get the path wrong, you may cause irreparable damage to your computer's filesystem.

```bash
# Replace "YOUR_SD_CARD_DEVICE" with your SD card's actual device name (you may use "lsblk" to find it).
unzip -p pipettin_pi_pishrunk.img.zip | sudo dd bs=4M status=progress of=/dev/YOUR_SD_CARD_DEVICE && sync
```

3. Safely remove the SD card from your computer.
4. Insert it into your Raspberry Pi, and power it on.
5. Find its IP address, use `ssh` to log in, and check if the filesystem has expanded with `df -h`.
    - If not expanded, reboot the Pi. It sometimes takes a couple boots.
    - User and pass are the Pi's default.

### Create OS backup

Note: extra details at [raspberrypi.org archive](https://web.archive.org/web/20210419061127/https://www.raspberrypi.org/documentation/linux/filesystem/backup.md).

To create the compressed filesystem backup, we used:

```bash
# Replace "sda" with your SD card's actual device name (you may use "lsblk" to find it).
sudo dd bs=4M status=progress if=/dev/YOUR_SD_CARD_DEVICEYOUR_SD_CARD_DEVICE | gzip > pipettin_pi.img.gz
sync
```

Uncompressed (very large) images can be saved as well:

```bash
# Replace "sda" with your SD card's actual device name (you may use "lsblk" to find it).
sudo dd bs=4M status=progress if=/dev/YOUR_SD_CARD_DEVICE of=pipettin_pi.img
sync
```

Such an image can be automatically shrung using [PiShrink](https://github.com/Drewsif/PiShrink). The main benefit is that the filesystem is also shrunk, so it can be used in SD cards smaller than the original.

```bash
# Do a compressed backup before shrinking, just in case!
gzip -k pipettin_pi.img

# Shrink the image with PiShrink
sudo pishrink.sh pipettin_pi.img

# Compress the shrunk image, for sharing.
zip pipettin_pi.img.zip pipettin_pi.img
```

### Method 2: From scratch

This method tales a _long_ time, because the compilation of mongodb takes forever.

> These steps cover the broad process, but are not thorough. Effort is required. Good luck!

1. Install Raspi OS on an SD card, put it on the Pi, and power it on.
2. Connect through `ssh` and make a shallow clone of the repo at the home directory (i.e. `/home/pi`).
    - `git clone --depth 1 https://gitlab.com/pipettin-bot/pipettin-grbl.git`
2. Install and setup dependencies:
    - GUI (node and mongo): [README.md](./gui/README.md)
    - Commander (python): [README.md](./protocol2gcode/README.md)
    - PC**R** planner (R): [README.md](./Rscripts/README.md)
3. Install and enable the Systemd units: [README.md](./systemd.units/README.md)
4. Load the "default" objects into MongoDB: [README.md](./defaults/README.md)

- [ ] TO-DO: be more thorough.

## Arduino firmware

Requirements:

* The configured GBRL firmware is [here](./grbl).
* Arduino IDE.

<!-- Guia Instalacion de GRBL en Arduino UNO -->

> TO-DO

# [User guide](https://pipettin-bot.gitlab.io/pipettin-grbl-docs/guides/01_user_guide.html)

> TO-DO

<!-- Aca no explicaria nada de la arquitectura ni de los modulos que intervienen). Este deberia ser el readme.md principa. -->

Published with GitBuilding, available at: https://pipettin-bot.gitlab.io/pipettin-grbl-docs/guides/01_user_guide.html

Sources: https://gitlab.com/pipettin-bot/pipettin-grbl-docs

Instructions to set-up new platforms and tools, define protocols, and running them.

- Usage: https://pipettin-bot.gitlab.io/pipettin-grbl-docs/guides/01_user_guide.html
- Platform and tool set-up: 

## Web GUI guide

<!-- Manual de GUI -->

> TO-DO

## Machine calibration

Callibration instructions before protocol run.

### Tool calibration

A procedure is available at: [`doc/tool_callibration/README.md`](./doc/tool_callibration/README.md)

<!-- Calibracion del XYZ de las herramientas y sus offsets. -->

> TO-DO

### Workspace and platforms

A procedure is available at: [`doc/platforms/README.md`](./doc/platforms/README.md)

<!-- Calibracion del XYZ de los objetos en la mesa. -->

> TO-DO

### Editing platforms for multi-tool support

A procedure is available at: [`doc/tool_callibration/README.md`](./doc/tool_callibration/README.md)

<!-- Calibracion del Z de los objetos en la mesa para cada tool. -->

> TO-DO

### Pipette volumetric calibration

Calibration results are available at the [`calibration`](./calibration) directory (see its [README.md](./calibration/README.md)).

<!-- 
Calibracion de pipeta:

- Relacion Volumen-deplazamiento.
- Tip probe.
- Setup de las constantes en el driver (retraction, etc.).
- Setup de las correcciones en el driver (pipeteo de mas / de menos, etc.).
- Protocolo de calibracion con balanza analítica.
-->

> TO-DO


