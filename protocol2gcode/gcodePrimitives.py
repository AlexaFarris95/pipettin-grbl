def gcodeSetFeedrate(feedrate):
    """Set feedrate for G1 moves"""
    return "G0 F" + str(feedrate)


def gcodeSpindleEnable():
    """M3 as per http://linuxcnc.org/docs/html/gcode/m-code.html#mcode:m3-m4-m5"""
    return "M3 S0"


def gcodeSpindleDisable():
    """M5 as per http://linuxcnc.org/docs/html/gcode/m-code.html#mcode:m3-m4-m5"""
    return "M5"


def gcodeEjectTip(safe_z, safe_y, post_x, post_y, post_z_pre, post_z, feed_rate=200):
    """
    Tip ejection macro based on controlled crashing with a "post".

    @param safe_z Clearance Z position ofr the current workspace and tool.
    @param safe_y Clearance Y position ofr the current workspace and tool.
    @param post_x X-axis coordinate of the carriage when the pipette's eject button is under the ejection post.
    @param post_y Y-axis coordinate of the carriage when the pipette's eject button is under the ejection post.
    @param post_z_pre Z-axis coordinate of the carriage when the pipette's eject button is under the ejection post, but not in contact with it.
    @param post_z Z-axis coordinate of the carriage when the pipette's eject button is fully pressed the ejection post, and a tip is ejected.
    @param feed_rate
    """
    commands = [
        "; START tip-ejection macro.",
        f"G0 G90 Z{safe_z}; go to safe Z (workspace tip box)",
        f"G0 G90 Y{safe_y}; go to safe Y (parked tools)",
        f"G0 G90 X{post_x}; Align X position",
        f"G0 G90 Z{post_z_pre}; pre-approach Z position",
        f"G0 G90 Y{post_y}; approach the toolpost",
        f"G1 G90 Z{post_z} F{feed_rate}; eject the tip",
        f"G0 G90 Z{post_z_pre}; relax",
        f"G0 G90 Y{safe_y}; move away from the ejection post up to safe Y (parked tools)",
        # The safe_z distance will actually change during this GCODE.
        # Let the actionDISCARD_TIP function handle this.
        # f"G0 G90 Z{safe_z}; go to safe Z (workspace tip box)"
        "; END tip-ejection macro.",
    ]
    
    return commands
    # """Tip ejection by servo, controlled thourh pigpio"""
    # return ["Peject;"]
    # "M5 as per http://linuxcnc.org/docs/html/gcode/m-code.html#mcode:m3-m4-m5"
    # return ["M3 S20", "Wait;1", "M3 S80"]


def gcodeProbeZ(z_probe=12, x_probe=0, y_probe=0, z_scan=-2.5, probe_height=52, mode="G38.2"):
    """
    NO LONGER IN USE!!!
    This macro was meant to callibrate the pipette's Z position using a "thingy" of known height on the workspace.
    GRBL would probe the thing, stop, and overwrite the current Z position with the know height, using G92 (https://all3dp.com/2/g92-g-code-set-position/).
    In this way, the coordinate system would have it's lowest point at "0", and would match the surface of the workspace.
    
    For reference:
    "G38.2" probe until probe is ON, error if probe doesnt activate during probing
    "G38.3" probe until probe is ON, without error
    """
    return ["G90 G0 X{} Y{}".format(x_probe, y_probe),
            "G90 G0 Z{}".format(str(z_probe+1)),
            "G91 G0 Z-1",
            "{} Z{} F200".format(mode, str(z_scan)),
            "G92 Z{}".format(probe_height),
            "G90 G0 Z{}".format(str(probe_height + 10))
            ]


def gcodePipetteProbe(z_scan=-5, mode="$J=G91", feedrate=1000, axis="Z", prefix="PRB;"):
    """
    # "G38.2" probe until probe is ON, error if probe doesnt activate during probing 
    # "G38.3" probe until probe is ON, without error.
    # "$J=G91 Y-10": use a jogging motion (to be interrupted externally).
    # TODO: conseguir informacion del probe position con "$#".
    #  Ver: https://github.com/gnea/grbl/wiki/Grbl-v1.1-Commands#---view-gcode-parameters
    """
    # assert z_scan <= 0
    return f"{prefix}{mode} {axis}{z_scan} F{feedrate};probingtime_{abs(z_scan)/feedrate}"


def gcodeMove(x: float = None,
              y: float = None,
              z: float = None,
              s: float = None,
              _mode="G90 G0",  # G90 for absolute, G91 for relative
              _scale_servo=1):
    """gcode to move robot to xyz and servo to s, by pasting values of xyzs if specified """

    if x is None and y is None and z is None and s is None:
        print("No values specified, returning empty string.")
        return ""

    command = [_mode]  # Default is G90 G0,

    if x is not None:
        command.append("X" + str(x))

    if y is not None:
        command.append("Y" + str(y))

    if z is not None:
        command.append("Z" + str(z))

    if s is not None:
        command.append("S" + str(s/_scale_servo))

    return " ".join(command)


def gcodeHomeXYZ(which_axis="all"):
    """Returns $H for 'all' or either single axis homing commands for which_axis equal to x, y or z."""
    
    # Default GRBL homing command
    home_command = "$H"

    # Append single axis specifier if required
    if which_axis != "all" and which_axis.upper().find("XYZ") == -1:
        if which_axis.upper().find("X") >= 0:
            home_command += "X"
        if which_axis.upper().find("Y") >= 0:
            home_command += "Y"
        if which_axis.upper().find("Z") >= 0:
            home_command += "Z"

    return home_command


def gcodeG92(x: float = None,
             y: float = None,
             z: float = None):
    """Returns G92 setup command"""
    command = ["G92"]  # Default is G90 G0,

    if x is None and y is None and z is None:
        print("gcodeMove warning: no values specified.")
        return "; gcodeMove warning: no values specified"

    if x is not None:
        command.append("X" + str(x))

    if y is not None:
        command.append("Y" + str(y))

    if z is not None:
        command.append("Z" + str(z))

    return " ".join(command)


def toolchange_probe(x, y, z, y_final, y_clearance, z_parking_offset, safe_z, safe_y,
                     extra_scan=1, closeup_dist=10, extra_undock_dist=0,
                     mode_fwd="G38.2", mode_rev="G38.4", feedrate=200):
    """
    First align to the parked tool. Then, do a probing scan, looking for the parking post's limit switch.
    When it activates, back out up to the initial location.
    NOTE: The $6=0 GRBL setting is required to use a mechanical end-stop as a probe.
    For reference:
    - "G38.2" probe until probe is ON, error if probe does not activate during probing.
    - "G38.3" probe until probe is ON, without error.
    TODO: conseguir informacion del probe position con "$#".
    Ver: https://github.com/gnea/grbl/wiki/Grbl-v1.1-Commands#---view-gcode-parameters
    
    From LinuxCNC: https://linuxcnc.org/docs/2.6/html/gcode/gcode.html
    
    - G38.2 - probe toward workpiece, stop on contact, signal error if failure.
    - G38.3 - probe toward workpiece, stop on contact.
    - G38.4 - probe away from workpiece, stop on loss of contact, signal error if failure.
    - G38.5 - probe away from workpiece, stop on loss of contact.

    @param x: Carriage coordinate at the front of the parking.
    @param y: Carriage coordinate at the front of the parking.
    @param z: Carriage coordinate at the front of the parking.
    @param y_final:
    @param y_clearance: Length of the tool along the Y axis when it is loaded, measured from the front of the carriage.
    @param z_parking_offset: Difference between the Z coordinate for parking, relative to the Z coordinate for loading.
    @param safe_z: General Z clearance from platforms in the workspace.
    @param safe_y: General Y clearance from other parked tools.
    @param extra_scan: Tool-change probing extra scan distance, relative to the exact Y position for docking.
    @param closeup_dist: A fast move will be made between "y" and "y_final-closeup_dist", the rest of the distance to the probe will be done slowly.
    @param extra_undock_dist: After clicking the latch and probing back, Y axis will move to "y-extra_undock_dist", to ensure undocking from the toolpost.
    @param mode_fwd: Forward probing mode.
    @param mode_rev: Reverse probing mode.
    @param feedrate: Probing feedrate.
    @return:
    """
    command = ["; START tool-change macro.",
               "; Safety clearance moves.",
               f"G90 G0 Z{safe_z}; move to the safe Z height.",
               f"G90 G0 Y{safe_y}; move to the Y coordinate clear of other parked tools.",
               "; Alignment moves.",
               f"G90 G0 X{x}; move to the X position in front of the parking post.",
               f"G90 G0 Y{y} Z{z}; move to the Y position in front of the parking post, and align to the post's Z.",
               "; Docking moves.",
               f"G90 G0 Y{y_final - closeup_dist}; insert the alignment rods.",
               f"{mode_fwd} Y{y_final + extra_scan} F{feedrate}; probe the remaining Y distance.",
               f"{mode_rev} Y{y} F{feedrate}; probe back.",
               f"G90 G0 Y{y-extra_undock_dist}; back away from the parking area, undocking the tool from the post.",
               "; Safety clearance moves.",
               f"G90 G0 Y{y - y_clearance}; back out of the way of other tools.",
               f"G90 G0 Z{safe_z}; move to the safe Z height.",
               "; END tool-change macro."]
    return command
