from time import sleep
from math import ceil, floor, pi
import traceback
from pprint import pprint

try:
    from RPi import GPIO
except ImportError:
    print("Dummy GPIO")
    from dummyGPIO import GPIO

try:
    import pigpio as pigpio
except ImportError:
    print("Dummy pigpio")
    from dummyGPIO import pigpio


class LimitSwitchException(Exception):
    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        if self.message:
            return 'Exception message: {0} '.format(self.message)
        else:
            return 'LimitSwitchException has been raised!'


class Pipette2(object):
    def __init__(self, pipettes=None,
                 home_pipette_at_init = False,
                 dry=False,
                 verbose=False):
        """
        Pipette controller class. Uses GPIO and pigpio to control a stepper driver,
        and uses GPIO to read signals from limit switches.
        """

        self.verbose = verbose

        # Set up an external stop signal
        self.stop_signal = False

        if not dry:
            if self.verbose:
                print("\nSAxisPigpio:\n    Pipette axis init")
        else:
            if self.verbose:
                print("\nSAxisPigpio:\n    Pipette axis init (dry run)")

        # Set default pipettes if unspecified
        if pipettes is None:
            self.pipettes = {
                # "syringe_3mL": {
                #     "vol_max": 3000,  # microliters
                #     "scaler":    20     # microliters / millimeter
                # },
                # "syringe_1mL": {
                #     "vol_max": 1000,  # microliters
                #     "scaler": 20        # microliters / millimeter
                # },
                # "p1000": {
                #     "vol_max": 1000,  # microliters
                #     "scaler": 20      # microliters / millimeter. TO-DO: measure p1000 shaft diameter
                # },
                # "none" :{
                #     "vol_max": 10,                          # [microliters] maximum pipette volume (as indicated by dial) with some tolerance subtracted.
                #     "scaler": 1,                            # [microliters/millimeter] conversion factor, from mm (shaft displacement) to uL (corresponding volume).
                #     "tipSealDistance": 1,                   # [millimeters] distance the pipette must enter the tip to properly place it.
                #     "backslash_compensation_volume": 1,     # [microliters] extra move before poring volume, corresponds to backslash error after drawing (only applies after a direction change).
                #     "extra_draw_volume": 1,                 # [microliters] extra volume that the pipette needs to draw (to avoid the "under-drawing" problem). See: calibration/data/21-08-17-p200-balanza_robada/README.md
                #     "current_volume": None
                # },
                "p200": {
                    # Physical parameters
                    # These defaults are also defined in gcodeBuilder.py
                    "vol_max": 170,                         # [microliters] maximum pipette volume (as indicated by dial) with some tolerance subtracted.
                    "scaler": pi*((4/2)**2),                # [microliters/millimeter] conversion factor, from mm (shaft displacement) to uL (corresponding volume).
                    # Según eso, el desplazamiento máximo
                    # del émbolo es 15.91 mm.
                    "tipLength": 50,                        # [millimeters] total length of the pipette's tip.
                    "tipSealDistance": 6,                   # [millimeters] distance the pipette must enter the tip to properly place it.
                    "tip_probing_distance": 12,             # Clearance distance before probing
                    "probe_extra_dist": 5,                  # Extra distance to probe, beyond the "defaultbottomposition".
                    "backslash_compensation_volume": 0.1,   # [microliters] extra move before poring volume, corresponds to backslash error after drawing (only applies after a direction change).
                    "extra_draw_volume": 8,                 # [microliters] extra volume that the pipette needs to draw (to avoid the "under-drawing" problem). See: calibration/data/21-08-17-p200-balanza_robada/README.md
                    "back_pour_correction": 4,              # [microliters] volume that is returned to the source tube after pipetting into a new tip (a sort of backslash correction).
                    "current_volume": None,
                    "retraction_displacement": 18.5,        # [millimeters] Downward displacement after hitting the limit switch, meant to push the pipettes shaft precisely up to its first stop.
                    # Coordinates and offsets
                    # Pipette tip offset, set to zero on the first tool (by definition).
                    "tool_offset": {"x": 0, "y": 0, "z": 0, "z_bare": 0, "z_tip": 0},
                    "tool_post": {"x": 13, "y": 280, "z": 20, "y_final": 314, "y_clearance": 110, "z_parking_offset": 2},
                    "eject_post": {"post_x": 180, "post_y": 316,
                                   "post_z_pre": 62, "post_z": 71,
                                   "feed_rate": 400},
                    # Driver parameters
                    "driver": "A4988",          # Name of the stepper driver, according to "self.RESOLUTION"
                    "microstep": "1/16",        # Default microstep setting
                    "delay_default": 0.0208,    # ?
                    "SPR": 200,                 # The stepper motor steps per revolution (200 for a 1.8º stepper).
                    "MPR": 2,  # 8              # [millimeters] Displacement per revolution of the lead screw.
                    "MODE": [25, 8, 7],         # [MS1, MS2, MS3] GPIO pins for Microstepping
                    "limit_pin": 14,            # GPIO limit pin number.
                    "DIR": 26,                  # GPIO step direction pin number.
                    "STEP": 19,                 # GPIO step pin number.
                    "PRB": 23,                  # GPIO tip probe pin number.
                    "EN": 6,                    # GPIO enable pin number.
                    "pwm_freq": 500,            #
                    "pwm_duty": 128,            #
                    "max_speed": 8,             #
                    "accel_interval": 20/1000,  #
                    # State tracking
                    "state": {"s": 0, "vol": 0,  # pipette axis position in mm (s) and microliters (vol).
                              "tip_vol": 0,
                              "lastdir": None,
                              "x": None,
                              "y": None,
                              "z": None,
                              "platform": None,
                              "tube": None,
                              "paused": False,
                              "alarm": False
                              }
                },
                "p20": {
                    # Physical parameters
                    # These defaults are also defined in gcodeBuilder.py
                    "vol_max": 19,                          # [microliters] maximum pipette volume (as indicated by dial) with some tolerance subtracted.
                    # No pude medir con precisión 1.30 mm de diametro.
                    # El desplazamiento del émbolo es 15.7 mm (+/- 0.02 mm).
                    # Esa cuenta da 20.8 uL por desplazamiento, es razonable.
                    # Puede haber errores en uno u otro número,
                    # así que elegí 1.28 porque da más cerca de 20 uL (20.2 uL).
                    "scaler": pi*((1.28/2)**2),             # microliters / millimeter,                # [microliters/millimeter] conversion factor, from mm (shaft displacement) to uL (corresponding volume).
                    "tipLength": 50,                        # [millimeters] total length of the pipette's tip.
                    "tipSealDistance": 6,                   # [millimeters] distance the pipette must enter the tip to properly place it.
                    "tip_probing_distance": 12,  # Clearance distance before probing
                    "probe_extra_dist": 5,  # Extra distance to probe, beyond the "defaultbottomposition".
                    "backslash_compensation_volume": 0.1,   # [microliters] extra move before poring volume, corresponds to backslash error after drawing (only applies after a direction change).
                    "extra_draw_volume": 20*0.04,           # [microliters] extra volume that the pipette needs to draw (to avoid the "under-drawing" problem). See: calibration/data/21-08-17-p200-balanza_robada/README.md
                    "back_pour_correction": 20*0.025,       # [microliters] volume that is returned to the source tube after pipetting into a new tip (a sort of backslash correction).
                    "current_volume": None,
                    "retraction_displacement": 19,        # [millimeters] Downward displacement after hitting the limit switch, meant to push the pipettes shaft precisely up to its first stop.
                    # Coordinates and offsets
                    # Pipette tip offset, relative to the offset in the first tool.
                    "tool_offset": {
                        # XY offset, calculated as the difference of equivalent positions (e.g. over the exact same physical tip).
                        # The difference is calculated between the reference pipette and this pipette (e.g. xy_offset = p20_xy_coords - p200_xy_ref_coords).
                        "x": 2, "y": 4, 
                        # Z offset between the reference pipette and this pipette, 
                        # when no tips are placed on neither pipette.
                        "z_bare": -6, 
                        # Z offset between the reference pipette and this pipette, 
                        # when tips _are_ placed on both pipettes.
                        "z_tip": 9.8
                        },
                    "tool_post": {"x": 316, "y": 280, "z": 17,
                                  "y_final": 313, "y_clearance": 110, "z_parking_offset": 2},
                    "eject_post": {"post_x": 179, "post_y": 315,
                                   "post_z_pre": 65, "post_z": 73,
                                   "feed_rate": 400},
                    # Driver parameters
                    "driver": "A4988",          # Name of the stepper driver, according to "self.RESOLUTION"
                    "microstep": "1/16",        # Default microstep setting
                    "delay_default": 0.0208,    # ?
                    "SPR": 200,                 # The stepper motor steps per revolution (200 for a 1.8º stepper).
                    "MPR": 2,  # 8              # [millimeters] Displacement per revolution of the lead screw.
                    "MODE": [25, 8, 7],         # [MS1, MS2, MS3] GPIO pins for Microstepping
                    "limit_pin": 14,            # GPIO limit pin number.
                    "DIR": 20,                  # GPIO step direction pin number.
                    "STEP": 16,                 # GPIO step pin number.
                    "PRB": 24,                  # GPIO tip probe pin number.
                    "EN": 6,                    # GPIO enable pin number.
                    "pwm_freq": 500,            #
                    "pwm_duty": 128,            #
                    "max_speed": 8,             #
                    "accel_interval": 20/1000,  #
                    # State tracking
                    "state": {"s": 0, "vol": 0,  # pipette axis position in mm (s) and microliters (vol).
                              "tip_vol": 0,
                              "lastdir": None,
                              "x": None,
                              "y": None,
                              "z": None,
                              "platform": None,
                              "tube": None,
                              "paused": False,
                              "alarm": False
                              }
                }
            }                  # available micropipette models and characteristics

        # MS sin configurations for different drivers

        self.RESOLUTION = {
            "DRV8825": {'1':    [1, (0, 0, 0)],
                        '1/2':  [2, (1, 0, 0)],
                        '1/4':  [4, (0, 1, 0)],
                        '1/8':  [8, (1, 1, 0)],
                        '1/16': [16, (0, 0, 1)],
                        '1/32': [32, (1, 0, 1)]
                        },
            "A4988": {'1':    [1, (0, 0, 0)],
                      '1/2':  [2, (1, 0, 0)],
                      '1/4':  [4, (0, 1, 0)],
                      '1/8':  [8, (1, 1, 0)],
                      '1/16': [16, (1, 1, 1)]
                      }
        }

        if not dry:
            # Setup GPIO pinmode numbering. TODO: cambiar todo para que se use pigpio.
            # See: https://raspberrypi.stackexchange.com/a/12967
            GPIO.setmode(GPIO.BCM)

            # Connect to pigpiod daemon
            self.pi = pigpio.pi()  # pigpio MUST BE ALREADY RUNNING with PCM clock: $ sudo pigpiod
            self.pi.wave_clear()   # clear existing waves
        else:
            if self.verbose:
                print("\nSAxisPigpio:\n    Dry run: Skipped GPIO setup.")

        # Dry argument
        self.dry = dry
        # Cleanup flag
        self.clean = False
        # Homing at startup flag
        self.home_pipette_at_init = home_pipette_at_init
        # Current pipette variable
        self.pipette_model = None

    def configure_pipette(self, pipette_model=None):
        """
        Reconfigure GPIO and pigpio pins to a new pipette.
        For example: configure_pipette("p20")
        """

        # Check if change is unnecessary
        if pipette_model is None or pipette_model == "None":
            if self.verbose:
                print(f"\nSAxisPigpio:\n    Skipping configuration for 'None' tool: {pipette_model}")
                print("\nSAxisPigpio:\n    Current config: " + str(self.pipette_model))
            return True

        # Check if change is unnecessary
        elif pipette_model == self.pipette_model:
            if self.verbose:
                print("\nSAxisPigpio:\n    Pipette axis already configured! Pipette: " + str(self.pipette_model))
                print("\nSAxisPigpio:\n    MS pin pattern: " + str(self.ms_pin_patterns[self.microstep][1]))
            return True

        else:
            if self.verbose:
                print("\nSAxisPigpio:\n    Setting up pipette with model: " + pipette_model)
            self.pipette_model = pipette_model
            self.pipette = self.pipettes[self.pipette_model]

        # Configure the mm to uL scaler
        self.pipette_scaler = self.pipette["scaler"]

        # Get microstepping settings
        self.driver = self.pipette["driver"]                            # Stepper driver model.
        self.MODE = self.pipette["MODE"]                                # Microstep GPIO pin numbers.
        self.ms_pin_patterns = self.RESOLUTION[self.driver]             # Pin pattern for the driver.
        self.microstep = self.pipette["microstep"]                      # Microstep choice ("1/8").
        self.microstep_int = self.ms_pin_patterns[self.microstep][0]    # Microsteps per step.

        # Calculate displacement per step
        self.SPR = self.pipette["SPR"]  # Steps per Revolution of the motor (360 / ??)
        self.MPR = self.pipette["MPR"]  # Millimeters per revolution of the screw
        self.MPS = (self.MPR / self.SPR) / self.microstep_int  # Millimeters per step

        self.limit_pin = self.pipette["limit_pin"]  # limit switch (pressed = LOW)
        self.DIR = self.pipette["DIR"]              # Direction GPIO Pin
        self.STEP = self.pipette["STEP"]            # Step GPIO Pin
        self.EN = self.pipette["EN"]                # Stepper enable Pin
        self.pwm_freq = self.pipette["pwm_freq"]    # PWM frequency (options are limited to a set, except for hardware PWM on pin 18)
        self.pwm_duty = self.pipette["pwm_duty"]    # PWM duty cycle
        self.accel_interval = self.pipette["accel_interval"]  # ms spent in each speed before increasing, less is more acceleration
        self.probe_pin = self.pipette["PRB"]        # Probe GPIO Pin
        self.max_speed = self.pipette["max_speed"]  # Max pipette speed

        # Setup limit pin
        # These are normal open endstops, which means active high.
        # Setup with an internal pull-down resistor, to minimize EMI noise.
        GPIO.setup(self.limit_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        
        # Setup probe pin
        # These are normal closed opto-endstops, which means active low.
        GPIO.setup(self.probe_pin, GPIO.IN)
        # Repeat for each one
        for p in list(self.pipettes):
            try:
                pin = self.pipettes[p]["PRB"]
                GPIO.setup(pin, GPIO.IN)
            except Exception:
                pass
        
        # Setup enable stepper enable
        GPIO.setup(self.EN, GPIO.OUT, initial=GPIO.HIGH)  # Default: steppers disabled (EN pin is active low).
        # Just in case:
        self.disable_stepper()
        
        # Set up DIR and STEP pins as output0
        self.pi.set_mode(self.DIR, pigpio.OUTPUT)
        self.pi.set_mode(self.STEP, pigpio.OUTPUT)

        # Apply micro step choice to pins
        for i in range(3):
            ms_pin = self.MODE[i]
            ms_pin_config = self.ms_pin_patterns[self.microstep][1][i]
            self.pi.set_mode(ms_pin, pigpio.OUTPUT)
            self.pi.write(ms_pin, ms_pin_config)

        # Set duty cycle and frequency. NOTE: ¿why do I need this?
        self.pi.set_PWM_dutycycle(self.STEP, 0)              # 0 = Stepper Off
        self.pi.set_PWM_frequency(self.STEP, self.pwm_freq)  # 500 = 500 Hz (steps per second)

        if self.verbose:
            print("\nSAxisPigpio:\n    Pipette axis ready! Pipette: " + str(self.pipette_model))
            print("\nSAxisPigpio:\n    MS pin pattern: " + str(self.ms_pin_patterns[self.microstep][1]))
            
    def enable_stepper(self):
        GPIO.output(self.EN, GPIO.LOW)
    
    def disable_stepper(self):
        GPIO.output(self.EN, GPIO.HIGH)

    def stepCalculation(self, displacement):
        """
        Convert millimeter displacement to stepper steps considering microstepping.
        Positive displacements load volume.
        Negative displacements eject volume.
        """
        microstep = self.microstep_int  # get microstep as int, for example: 32
        step_count_float = displacement * (self.SPR / self.MPR) * microstep  # compute step count from "mm" displacement
        step_count = floor(step_count_float)  # TODO: correct this approximation on millimeters to step rounding
        return step_count

    def probe_tip_wait(self, timeout=10, time_step=0.01):
        """
        This function is meant for probing the tip placement from outside the class,
        to interrupt a pseudo-probe GCODE based on a Jog command and a feed-hold.
        """
        print("\nSAxisPigpio:\n    Initial tip GPIO status: " + str(GPIO.input(self.probe_pin)))

        t = 0
        pin_status = GPIO.input(self.probe_pin)
        while pin_status == GPIO.LOW:
            t += time_step
            if t >= timeout:
                print("\nSAxisPigpio:\n    probe_tip_wait: TIMED-OUT. GPIO status: " + str(pin_status) + ", in time " + str(t))
                return False
            sleep(time_step)
            pin_status = GPIO.input(self.probe_pin)

        print("\nSAxisPigpio:\n    probe_tip_wait: PROBE FIRED. GPIO status: " + str(pin_status) + ", in time " + str(t))
        return True

    def displace(self,
                 displacement,
                 max_speed=None,
                 accel_interval=None,
                 safety_raise=False,
                 safety_lower=False):
        """
        Move S axis by the specified volume, at max speed in mm/s.
        :param displacement: volume in microliters. Positive displacements load volume. Negative displacements eject volume.
        @param safety_lower:
        @param safety_raise:
        @param displacement:
        @param max_speed:
        @param accel_interval:
        """
        
        # Enable steppers (just in case, not disabled later on in this function)
        self.enable_stepper()
        
        # Calculate shaft displacement in millimeters using the microliter to mm conversion factor.
        displacement_mm = float(displacement) / self.pipette_scaler

        # Set max speed from defaults
        if max_speed is None:
            max_speed = self.max_speed

        if self.dry:
            if self.verbose: print("\nSAxisPigpio:\n    //// Dry run, not displacing pipette axis by: ", str(displacement_mm))
        else:
            try:
                if abs(displacement_mm) > 80:  # TODO: figure out why greater values make pigpio crash
                    raise ValueError("".join(["PROTOCOL ERROR: displacement_mm is greater than 80,",
                                              " this can fuck up a pigpio chain.",
                                              "\n  Doing nothing and cleaning up :("]))

                step_count = self.stepCalculation(displacement_mm)
                if self.verbose:
                    print(f"\nSAxisPigpio:\n    //// displacement_mm is {displacement_mm}, step count is {step_count}, direction is {step_count < 0}.")

                # Assign default accel_interval if omitted
                if accel_interval is None:
                    accel_interval = self.accel_interval

                # Set direction
                self.pi.write(self.DIR, step_count < 0)  # Set movement direction on the DIR pin.
                # TODO: be smarter about stepper direction.
                # Create ramp for the steps
                ramp = self.generate_ramp(abs(step_count), max_speed=max_speed, accel_interval=accel_interval)
                # Run the ramp
                self.run_ramp(ramp, safety_raise=safety_raise, safety_lower=safety_lower)  # Kick it!

                # Register new volume
                if self.pipettes[self.pipette_model]["current_volume"] is not None:
                    # reverses s_axis_movement calculation in gcodeBuilder.py to volume units
                    self.pipettes[self.pipette_model]["current_volume"] += float(displacement) * self.pipette["scaler"]

            except LimitSwitchException as e:
                print(e)
                print("\nSAxisPigpio:\n    //// Pipetting error: something happened during stepper displacement.")
                print("\nSAxisPigpio:\n    ////   Interrupting move and advancing to limit switch safety :(")
                self.pi.wave_clear()
                sleep(1)
                self.home_pipette()
                self.close()
                raise LimitSwitchException("//// Limit switch activated during displacement.")

            except Exception as e:
                print(e)
                print("\nSAxisPigpio:\n    //// Pipetting error: something happened during stepper displacement call.")
                print("\nSAxisPigpio:\n    ////   Doing nothing and cleaning up :(")
                self.close()
                raise Exception("//// Error during displacement")

    def home_pipette(self,
                     raise_displacement=25,         # Upward homing movements occur in segments this long (pigpio ramps can't be infinite).
                     retraction_displacement=None,  # Downward movement distance after homing and pull-off. Pipette-dependent.
                     lower_displacement=2,          # Downward pull-off movement distance after triggering the limit switch.
                     max_speed=None,
                     limit_switch_direction=0):
        """
        Retract S axis up to limit switch and move down.
        """
        try:
            # Set max speed from defaults
            if max_speed is None:
                max_speed = self.max_speed

            # Default to the current pipette's definition
            if retraction_displacement is None:
                retraction_displacement = self.pipette["retraction_displacement"]
                if self.verbose:
                    print("\nSAxisPigpio:\n    Setting retraction_displacement to: " + str(retraction_displacement))

            if self.dry:
                if self.verbose:
                    print("\nSAxisPigpio:\n    Dry run, not homing pipette axis.")
            else:
                # Enable steppers (just in case, not disabled later on in this function)
                self.enable_stepper()
                # Count steps
                step_count = self.stepCalculation(raise_displacement)
                # Set direction
                self.pi.write(self.DIR, limit_switch_direction)  # Set direction. TODO: be smarter about stepper direction
                # Create ramp for the steps
                ramp = self.generate_ramp(abs(step_count), max_speed)
                # Run the ramp
                while GPIO.input(self.limit_pin) == GPIO.HIGH:
                    # run_ramp wil automatically stop if a limit switch is triggered.
                    self.run_ramp(ramp, safety_raise=True)  # Kick it!
                if self.verbose:
                    print("\nSAxisPigpio:\n    Pipette moved up to limit switch.")

                # Retract
                if self.verbose:
                    print(f"\nSAxisPigpio:\n    Now retracting by {lower_displacement} mm")
                step_count = self.stepCalculation(lower_displacement)
                self.pi.write(self.DIR, not limit_switch_direction)
                ramp = self.generate_ramp(abs(step_count), max_speed)
                while GPIO.input(self.limit_pin) == GPIO.LOW:
                    # run_ramp wil automatically stop if a limit switch is triggered.
                    self.run_ramp(ramp, safety_lower=True)
                if self.verbose:
                    print("\nSAxisPigpio:\n    Pipette just under the limit switch.")

                # Extra retraction,
                # to lower the pipette's shaft up to the first stop.
                if self.verbose:
                    print(f"\nSAxisPigpio:\n    Extra retraction by {retraction_displacement} mm")
                step_count = self.stepCalculation(retraction_displacement)
                self.pi.write(self.DIR, not limit_switch_direction)
                ramp = self.generate_ramp(abs(step_count), max_speed)
                # run_ramp wil automatically stop if a limit switch is triggered.
                self.run_ramp(ramp)

                # Register home position
                self.pipette["current_volume"] = 0
        except Exception as e:
            print("\nSAxisPigpio:\n    home_pipette: unhandled exception during homing:\n    ", e)
            tb = traceback.format_exc()
            print(tb)
        except KeyboardInterrupt as e:
            # https://stackoverflow.com/a/13181036
            print("\nSAxisPigpio:\n    home_pipette: keyboard interrupt:\n    ", e)
            tb = traceback.format_exc()
            print(tb)
        finally:
            print("\nSAxisPigpio:\n    home_pipette: cleaning up!\n")
            self.pi.wave_tx_stop()
            self.pi.wave_clear()

    def run_ramp(self, ramp, safety_raise=False, safety_lower=False):
        """
        Process a ramp into a PWM chain, and transmit it.
        https://www.raspberrypi.org/forums/viewtopic.php?p=994373
        :param safety_raise: whether the move is for safety raising
        :param safety_lower: whether the move is for safety lowering
        :param ramp: list of FREQUENCY-STEPS pairs lists
        :return: yo momma so fat jokes
        """
        
        # Enable steppers (just in case, not disabled later on in this function)
        self.enable_stepper()

        ramp_length = len(ramp)
        wave_id = [-1] * ramp_length
        # self.pi.set_mode(self.STEP, pigpio.OUTPUT)  # Already done by __init__

        # Clear existing waves
        self.pi.wave_clear()

        # generate a wave per frequency
        for i in range(ramp_length):
            # Get the frequency from the list of FREQUENCY-STEPS pairs lists.
            freq = ramp[i][0]
            micros = int(500000 / freq)

            # Initialize empty waveform list
            wave_form = []

            # Create a pulse:
            # https://abyz.me.uk/rpi/pigpio/python.html#pigpio.pulse
            # gpio_on:= the GPIO to switch on at the start of the pulse.
            # gpio_off:= the GPIO to switch off at the start of the pulse.
            # delay := the delay in microseconds before the next pulse.
            # Reference:                  ON:             OFF:            DELAY:
            wave_form.append(pigpio.pulse(1 << self.STEP, 0             , micros))
            wave_form.append(pigpio.pulse(0             , 1 << self.STEP, micros))
            # I am guessing that the "0" values mean "do nothing", since at the start of the pulse
            # we want to turn a pin ON (and no pins OFF), and the inverse for the second pulse.

            # Adds a list of pulses to the current waveform:
            # https://abyz.me.uk/rpi/pigpio/python.html#wave_add_generic
            self.pi.wave_add_generic(wave_form)

            # Create a waveform from the data provided by the prior calls to the wave_add_* functions:
            # https://abyz.me.uk/rpi/pigpio/python.html#wave_create
            wave_id[i] = self.pi.wave_create()

        # generate a chain of waves
        # https://abyz.me.uk/rpi/pigpio/python.html#wave_chain
        # The following command codes are supported:
        # | Name         | Cmd & Data | Meaning                        |
        # | ----------   | ---------- | ------------------------------ |
        # | Loop Start   | 255 0      | Identify start of a wave block |
        # | Loop Repeat  | 255 1 x y  | loop x + y*256 times           |
        # | Delay        | 255 2 x y  | delay x + y*256 microseconds   |
        # | Loop Forever | 255 3      | loop forever                   |
        #
        # If present Loop Forever must be the last entry in the chain.
        #
        # The code is currently dimensioned to support a chain with roughly 600 entries and 20 loop counters.
        chain = []
        for i in range(ramp_length):

            steps = ramp[i][1]  # Get the number of steps from the frequency-steps pairs.

            # # The bitwise AND operator (&) performs logical conjunction on the corresponding bits of its operands.
            # # For each pair of bits occupying the same position in the two numbers,
            # # it returns a one only when both bits are switched on.
            # x = steps & 255  # Esto es como hacer el "resto" de dividir por 256 (se podria usar el módulo "%").
            # y = steps >> 8   # Intuyo que esto es como la división entera.
            # new_chain = [255, 0,        # Start loop.
            #              wave_id[i],    # This "wave" will be looped.
            #              255, 1, x, y]  # Repeat the loop "N = x + y*256" times and end the loop.
            # chain += new_chain
            # print("i value: " + str(i))
            # print(new_chain)
            # # Problematic "311" value in "y": [..., 255, 0, 10, 255, 1, 24, 311, ...]
            # # Error: "ValueError: byte must be in range(0, 256)"
            # # Bug appeared on setting MPR to 2 (previously 8).
            # # El problema entonces es que no se puede repetir más de 255+256*256 veces un loop (o algo así).

            # # Para hacer pruebitas:
            # steps = 37 + 256*800
            # wave_id = ["asd"]
            # i = 0
            # chain = []

            # Vamos a emparcharlo, partiendo la wave en varias waves.
            # Hago nuevas variables para eso:
            new_x = steps & 255
            new_y = steps >> 8
            # Voy agregando waves con y=255 hasta que la division entera da cero
            while (new_y >> 8) > 0:
                new_chain = [255, 0,
                             wave_id[i],
                             255, 1, 0, 255]
                chain += new_chain
                new_y -= 255  # le resto lo que agregué recién al wave
                # print(new_chain)
            # Finalmente agrego el resto
            if new_y > 0 or new_x > 0:
                new_chain = [255, 0,
                             wave_id[i],
                             255, 1, new_x, new_y]
                chain += new_chain
                # print(new_chain)

        # print("\nDone with chain:\n")
        # print(chain)

        # Limit switch check, do not move if it is active and there is no safe direction for movement.
        if GPIO.input(self.limit_pin) == GPIO.LOW and not (safety_raise or safety_lower):
            print("\nSAxisPigpio:\n    Limit switch is active without a safe direction! The stepper was not moved.")
            # raise LimitSwitchException("".join(["PROTOCOL ERROR: Limit switch is active!",
            #                                     " stepper was not moved."]))
        else:
            # Transmit chain.
            self.pi.wave_chain(chain)

        # Decide how to handle a limit switch triggering.
        # ¿Is it safe to move upwards when the limit switch is active? ¿Or is it safe to move downwards?
        # It is desirable only if it is a homing move.
        # If it is a pipetting move, then something is wrong (i.e. machine limits were exceeded).
        # Also, interrupt the movement on external signals.
        if safety_raise:
            # Sleep while transmitting
            while self.pi.wave_tx_busy():
                # Interrupt chain if limit switch is activated.
                if GPIO.input(self.limit_pin) == GPIO.LOW:
                    self.pi.wave_tx_stop()
                # Interrupt chain if required externally
                if self.stop_signal:
                    self.stop_signal = False  # Reset the flag
                    self.pi.wave_tx_stop()
                sleep(0.001)
        elif safety_lower:
            # Sleep while transmitting, OR interrupt chain if limit switch is released.
            while self.pi.wave_tx_busy():
                # Interrupt chain if limit switch is activated.
                if GPIO.input(self.limit_pin) == GPIO.HIGH:
                    self.pi.wave_tx_stop()
                # Interrupt chain if required externally
                if self.stop_signal:
                    self.stop_signal = False  # Reset the flag
                    self.pi.wave_tx_stop()
                sleep(0.001)
                continue
        else:
            # Sleep while transmitting and interrupt chain if limit switch is activated.
            while self.pi.wave_tx_busy():
                #if (GPIO.input(self.limit_pin) == GPIO.LOW) or self.stop_signal:
                if self.test_gpio(self.limit_pin, GPIO.LOW, 5) or self.stop_signal:
                    self.stop_signal = False  # Reset the flag
                    self.pi.wave_tx_stop()
                    print("\nSAxisPigpio:\n    WARNING, stepper displacement cancelled prematurely.")
                    # self.close()
                    # raise LimitSwitchException("".join(["PROTOCOL ERROR: Limit switch activated!",
                    #                                     " stepper displacement cancelled prematurely.",
                    #                                     " Cleaning up and closing."]))
                sleep(0.001)

        # Cleanup
        for i in range(ramp_length):
            self.pi.wave_delete(wave_id[i])  # Delete all waves

        return True

    def test_gpio(self, pin, value=GPIO.LOW, samples=3, sleep_time=0.0001):
        """Sample a RPi.GPIO pin a few times, compare to the test_value, and report if all comparisons were True."""
        count = 0
        for i in range(samples):
            test = (GPIO.input(pin) == value)
            count = count + int(test)
            sleep(sleep_time)
        if count == 5:
            # No "False" samples.
            return True
        elif count == 0:
            # No "True" samples.
            return False
        else:
            # Mixed results... noise detected!
            if self.verbose:
                print("\nSAxisPigpio:\n    sample_gpio WARNING, GPIO input from pin " + str(pin) + 
                      " is probably noisy (at " + str(count) + "/" + str(samples) + " True samples).")
            return False
        

    def generate_ramp(self, step_count, max_speed=None, accel_interval=None):
        """
        Generate a ramp as a list of frequency-steps pairs lists

        step_count = 200
        ramp = generate_ramp(step_count)
        print(step_count)
        print(ramp)
        print(sum(i[1] for i in ramp))

        for s in [123,465,98,231,99789]:
            ramp = generate_ramp(s)
            print(sum(i[1] for i in ramp) == s)
        """

        # Set max speed from defaults
        if max_speed is None:
            max_speed = self.max_speed

        # Assign default accel_interval if omitted
        if accel_interval is None:
            accel_interval = self.accel_interval

        # Crunch the numbers (?)
        max_speed_freq = max_speed / self.MPS
        time_interval = accel_interval  # ms spent in each speed before increasing, less is more acceleration
        default_speeds = [10, 20, 50, 160, 320, 500, 800, 1000, 2000, 4000, 8000, 12000]
        speeds = [speed for speed in default_speeds if speed <= max_speed_freq]
        if self.verbose:
            print(f"\nSAxisPigpio:\n    Speed range for displacement at max_speed {max_speed}: ", speeds, "\n")
        accel_steps = [ceil(time_interval * speed) for speed in speeds]

        steps_left = step_count
        next_idx = 0
        ramp = []

        if accel_steps[0]*2 >= step_count:
            return [[speeds[0], step_count]]

        for i in range(speeds.__len__()):
            ramp.append([speeds[i], accel_steps[i]])      # append at current speed and end the loop
            steps_left = steps_left - accel_steps[i] * 2  # compute how many steps are left considering accel symmetry

            next_idx = min(i+1, speeds.__len__()-1)
            if steps_left <= accel_steps[next_idx] * 2:   # if steps left won't fit in current symmetrical speed step
                break
            else:
                continue  # if the loop completes, max speed is reached, assign remaining steps to max

        ramp.append([speeds[next_idx], steps_left])

        ramp.extend(ramp[:-1][::-1])

        return ramp

    def close(self):
        print("\nSAxisPigpio:\n    close: cleaning up!")
        self.clean = True  # Cleanup flag
        self.disable_stepper()
        self.pi.wave_clear()
        self.pi.stop()
        GPIO.cleanup()
