# Functions for sending commands to GRBL and the SAxis controller.
from pipetteDriver import Pipette2
from gcodeCommander_utils import serial_reader, serial_writer, GrblError
# from driverGcodeSender2_utils import PollThreadError
# from driverGcodeSender2_utils import GrblError
import serial as serial
import time
import threading
import queue
import socketio
import re
import sys
import pprint
from math import floor, ceil
import subprocess
import traceback

try:
    from RPi import GPIO
except ImportError:
    print("Error: No GPIO python module found.")


class Commander(object):
    """A class for driving the Pipettin-GRBL bot v1"""

    def __init__(self,
                 protocol=None,
                 serial_device_path='/dev/ttyACM0',
                 baudrate=115200,
                 serial_object=None,
                 RX_BUFFER_SIZE=128,
                 serial_response_delay=0.1,
                 stepper_idle_timeout=255,
                 pipette_model="p200",
                 pipettes=None,
                 socket_port=3333,
                 sio_object=None,
                 dry=False, semi_dry=False,  # TODO: use "$C" to test gcode on GRBL w/o moving the motors
                 home_pipette_at_init=False,
                 reset_bcm_pin=17,
                 interactive=False,
                 verbose=True):

        if protocol is None:
            protocol = []
        
        # Save calling arguments
        self.protocol = protocol
        self.serial_device_path = serial_device_path
        self.baudrate = baudrate
        self.serial = serial_object
        self.RX_BUFFER_SIZE = RX_BUFFER_SIZE
        self.serial_response_delay = serial_response_delay
        self.stepper_idle_timeout = stepper_idle_timeout
        self.pipette_model = pipette_model
        self.socket_port = socket_port
        self.sio = sio_object
        self.dry = dry
        self.semi_dry = semi_dry
        self.home_pipette_at_init = home_pipette_at_init
        self.reset_bcm_pin = reset_bcm_pin
        self.interactive = interactive
        self.verbose = verbose

        # Killed flag
        # It is activated by a SIO event.
        # It is watched by the protocol loop, and by the human intervention action.
        self.killed = False

        # Setup GPIO pins for Arduino reset
        if self.verbose:
            print("\ndriver message:\n    init: Setup GPIO pins for Arduino reset function...")
        # Broadcom pin-numbering scheme
        GPIO.setmode(GPIO.BCM)
        # Initial value low: https://raspi.tv/2013/rpi-gpio-basics-5-setting-up-and-using-outputs-with-rpi-gpio
        GPIO.setup(reset_bcm_pin, GPIO.OUT, initial=GPIO.LOW)
        # Just in case
        GPIO.output(reset_bcm_pin, GPIO.LOW)

        # Setup pipette
        self.current_tool = None
        self.pipette = Pipette2(pipettes=pipettes,  # If "None", defaults will be used.
                                verbose=self.verbose)
        # Get active pipette set
        self.pipettes = self.pipette.pipettes
        # Get active pipette models
        self.pipette_models = list(self.pipettes)
        # Configure active pipette model
        self.pipette.configure_pipette(self.pipette_model)
        
        # Setup pyserial interface to GRBL
        if self.serial is not None:
            if self.verbose:
                print("\ndriver message:\n    init: Using provided pyserial object.")
            if not self.serial.is_open:
                self.serial.open()
        else:
            if self.verbose:
                print("\ndriver message:\n    init: Connecting to" + self.serial_device_path + " at " + str(baudrate))
            self.serial = serial.Serial(timeout=5)  # podria ser 0 para non blocking
            self.serial.baudrate = baudrate
            self.serial.dtr = None
            self.serial.port = self.serial_device_path
            self.serial.open()

        # Connect to the Web GUI's websocket and initialize asynchronous listeners
        self.socket_abort = False
        self.socket_pause = False
        if self.sio is not None:
            # Use the provided socket object
            # self.sio = self.sio  # Commented because it is now dumb (though harmless).
            # Flag use of the socket
            self.socket_use = True
        elif self.socket_port is not None:
            # Setup new socket
            self.sio = socketio.Client()
            self.sio.connect('http://localhost:' + str(self.socket_port))
            # Flag use of the socket
            self.socket_use = True
            if self.verbose:
                print("\ndriver message:\n    init: Using websocket")
        else:
            # Else flag socket disabled
            self.socket_use = False
            if self.verbose:
                print("\ndriver message:\n    init: Using dummy websocket")

        # Register socket event handlers
        if self.socket_use:
            # Register websocket event listener function for "start human intervention"
            self.sio.on('human_intervention_continue', self.continue_message_handler)
            # Register websocket event listener function for "cancelled human intervention"
            self.sio.on('human_intervention_cancelled', self.abort_message_handler)
            # Register websocket event listener function for "kill_commander"
            self.sio.on('kill_commander', self.kill_commander)

        # Threading setup
        if self.verbose:
            print("\ndriver message:\n    init: Setting up monitor thread events and queues...")
        
        # Events
        
        # Green light for the thread loops
        self.running_event = threading.Event()
        
        # Set by the serial_reader when a status report indicates IDLE state.
        self.idle_event = threading.Event()      
        # Set by the serial_reader when a status report indicates an ALARM state.
        self.alarm_event = threading.Event()
        
        # This event is cleared by the serial_reader when GRBL sends a "'$X' TO UNLOCK" message.
        # It is set by the serial_reader when GRBL sends a "CAUTION: UNLOCKED" message.
        self.unlocked_event = threading.Event() 
        
        # This event is set by the serial_reader when GRBL sends a "reset to continue" message.
        # The Arduino should be hard-reset through the GPIO pin, or soft-reset with a 'ctrl-x' command.
        self.reset_event = threading.Event()
        
        # To force one protocol at a time.
        self.protocol_event = threading.Event()
        
        # To monitor GRBL probe
        self.probe_event = threading.Event()
        
        self.gcode_unlock = threading.Event()  # used to prevent adding items to input_queue
        # Queues
        self.output_queue = queue.Queue(10000)  # GRBL messages go here
        self.ok_queue = queue.Queue(10000)  # To count OK messages from GRBL
        self.input_queue = queue.Queue(10000)  # GRBL commands go here
        self.request_queue = queue.Queue(1)  # To force one GRBL status request at a time
        self.error_queue = queue.Queue(10000)

        # Define GRBL handler threads (a writer and a monitor/message polling thread)
        # TODO: might want to move to multiprocessing: https://stackoverflow.com/a/7752174
        self.reader_thread = threading.Thread(target=serial_reader, args=(self.serial,
                                                                          self.output_queue,
                                                                          self.input_queue, self.ok_queue,
                                                                          self.request_queue, self.error_queue,
                                                                          self.protocol_event,
                                                                          self.idle_event, self.running_event,
                                                                          self.alarm_event, self.unlocked_event,
                                                                          self.reset_event, self.probe_event,
                                                                          self.sio, self.socket_use,
                                                                          self.serial_response_delay,
                                                                          self.verbose))
        self.writer_thread = threading.Thread(target=serial_writer, args=(self.serial,
                                                                          self.input_queue, self.ok_queue,
                                                                          self.request_queue, self.error_queue,
                                                                          self.protocol_event,
                                                                          self.idle_event, self.running_event,
                                                                          self.alarm_event, self.unlocked_event,
                                                                          self.reset_event, self.probe_event,
                                                                          self.gcode_unlock,
                                                                          self.serial_response_delay,
                                                                          self.verbose,
                                                                          RX_BUFFER_SIZE))
        # Set as "daemons", so they are killed when the main thread exits.
        self.reader_thread.daemon = True
        self.writer_thread.daemon = True
        
        """ About "daemon" threads: https://docs.python.org/3/library/threading.html
        A thread can be flagged as a “daemon thread”. The significance of this flag is that the entire Python program exits when only daemon threads are left. The initial value is inherited from the creating thread. The flag can be set through the daemon property or the daemon constructor argument.

        Note:

        Daemon threads are abruptly stopped at shutdown. Their resources (such as open files, database transactions, etc.) may not be released properly. If you want your threads to stop gracefully, make them non-daemonic and use a suitable signalling mechanism such as an Event.

        There is a “main thread” object; this corresponds to the initial thread of control in the Python program. It is not a daemon thread.
        """
        
        # Cleanup flag (True would mean the Commander has been "cleaned up" and needs re-instantiating).
        self.__clean = False

    def soft_reset(self):
        self.serial.write('0x18'.encode())  # Send "ctrl-x" (0x18) control charachter / realtime command, to soft-reset GRBL.
        
    def feed_hold(self):
        self.serial.write('!'.encode())     # Send "!" feed-hold realtime command.
    
    # Define kill signal handler
    def kill_commander(self, msg):
        """
        Handles a "kill_commander" event from the socket. Sent by "gui/lib/commander.js".
        """

        print("\ndriver message:\n    kill_commander: call initiated with message: " + str(msg) + ". Current kill value: " + str(self.killed))
        
        # If this is the first time we are killing this
        if not self.killed:
            
            print("\ndriver message:\n    kill_commander: kill_commander activated for the first time, skipping further protocol.")
            
            # Force protocol while loop in run_commander (below) to end prematurely, 
            # skipping the rest of the protocol (but completing the current step).
            self.killed = True
            
            # Let the threads stop (does not kill them).
            self.running_event.clear()
            
            # Stop the current piwave in the pipette driver.
            # # This is not really as in threading, we are in a function called by a socket event.
            # # I dont know if it can work, but it is worth a shot!
            # self.pipette.stop_signal = True  # Flag method
            self.pipette.pi.wave_tx_stop()     # Direct method
            
            # Send a feed-hold command, followed by a soft-reset.
            self.feed_hold()   # Send "!" feed-hold.
            time.sleep(0.5)    # Wait a bit for deceleration.
            self.soft_reset()  # Send "ctrl-x" (0x18) control charachter, to soft-reset GRBL.

            self.sio_emit('alert', {'text': "kill_commander: threads stopped, raised flag for breaking the protocol loop, tried to stop GRBL and the pipette.", 'type': 'alarm'})
            # This should leave GRBL in "alarm" state, but surely stopped and unlocked.
        
        # If this is the second time killing it
        else:
            
            print("\ndriver message:\n    kill_commander: activated for the second time, cleaning up and restarting the service.")

            # Hard-reset the arduino immediately.
            self.arduino_GPIO_reset()
            
            # And raise an error
            # raise GrblError("\ndriver message:\n    kill_commander: activated for a second time, forcing hard-reset on GRBL.")
            
            # TODO: should it cleanup here?
            
            try:
                self.cleanup()
            except Exception:
                print("\ndriver message:\n    kill_commander: tried to cleanup but failed for some reason.")
            
            # Restart using systemd
            self.sio_emit('alert', {'text': "kill_commander: restarting the commander through systemd... please wait a few seconds.", 'type': 'alarm'})
            subprocess.call("systemctl --user restart commander.service", shell=True)
            time.sleep(0.1)
            
            # I'm hoping that if the above does not work, this will kill daemonized threads.
            # Will this even run?
            print("\ndriver message:\n    kill_commander: got to sys.exit. :shrug:")
            self.sio_emit('alert', {'text': "kill_commander: ¿does systemd suck at stopping our program? it seems so!", 'type': 'alarm'})
            sys.exit(1)  # https://www.geeksforgeeks.org/python-different-ways-to-kill-a-thread/

    def sio_emit(self, what, data):
        if self.socket_use:
            self.sio.emit(what, data)
        else:
            print(what)
            print(data)

    def sio_disconnect(self):
        if self.socket_use:
            if self.verbose:
                print("\ndriver message:\n    sio_disconnect: Stopping websocket")
            self.sio.disconnect()

    # Define socket handlers
    def abort_message_handler(self, msg):
        """Message handler for the "cancelled" button after human interaction pause"""
        print('abort_message_handler: Received cancel message: ', msg)
        self.socket_abort = True

    # Define socket handlers
    def continue_message_handler(self, msg):
        """Message handler for the "continue" button after human interaction pause"""
        print('continue_message_handler: Received continue message: ', msg)
        self.socket_pause = False

    def arduino_GPIO_reset(self):
        try:
            # Repeat setup just in case
            GPIO.setmode(GPIO.BCM)
            # Initial value low: https://raspi.tv/2013/rpi-gpio-basics-5-setting-up-and-using-outputs-with-rpi-gpio
            GPIO.setup(self.reset_bcm_pin, GPIO.OUT, initial=GPIO.LOW)
            # Just in case
            GPIO.output(self.reset_bcm_pin, GPIO.LOW)

            # Reset
            GPIO.output(self.reset_bcm_pin, GPIO.HIGH)
            time.sleep(0.1)
            GPIO.output(self.reset_bcm_pin, GPIO.LOW)
            time.sleep(1)
        except Exception as e:
            print("\ndriver message:\n    arduino_GPIO_reset: failed to hard-reset the arduino: " + e)
            tb = traceback.format_exc()
            print(tb)

    def sio_emit_wpos(self, grbl_message):
        if grbl_message.upper().find('WPOS') > -1:
            wpos = re.search(r"WPos:(-?[\d\.]+),(-?[\d\.]+),(-?[\d\.]+[\|>])",
                             grbl_message, re.IGNORECASE)

            self.sio_emit('tool_data', {'position': {
                "x": wpos.group(1),
                "y": wpos.group(2),
                "z": wpos.group(3),
                "p": None  # TODO: conseguir info de la posicion de la pipeta del SAxis driver y pasarla acá
            }})

    def sio_emit_p_pos(self, pipette_position):

        if pipette_position is not None:
            pipette_position = round(pipette_position*100)/100

        self.sio_emit('tool_data', {'position': {
            "p": pipette_position
        }})

    def update_sio_wpos(self, timeout=40/1000):
        """The timeout corresponds to double the expected 20 ms response time to a status request to GRBL (plus 5 ms)."""
        
        # Use the existing "reader thread" if it is running.
        if self.running_event.is_set():
            self.status(timeout)
        # Else run a "reader" over here:
        else:
            endtime = time.time() + timeout
            poll = True
            while poll:
                # Just in case
                # See: https://github.com/michaelfranzl/gerbil/blob/e6828fd5a682ec36970de83d38a0bea46b765d8d/interface.py#L69
                # Cleanup serial buffers: https://pyserial.readthedocs.io/en/latest/pyserial_api.html
                # Clear output buffer, aborting the current output and discarding all that is in the buffer.
                self.serial.reset_output_buffer()
                # Flush input buffer, discarding all its contents.
                self.serial.reset_input_buffer()  # Flush startup text in serial input
                
                while self.serial.in_waiting:
                    discard = self.serial.readline().decode()

                # Request status report
                self.serial.write('?'.encode())  # Send "?"
                self.serial.flush()              # Wait until all data is written
                time.sleep(40/1000)              # Wait for GRBL to react to the status rquest (double the expected maximum of 20 ms).

                while self.serial.in_waiting:
                    poll_line = self.serial.readline().decode()
                    grbl_message = str(poll_line).strip()
                    # Check if the message is a status report
                    report = re.search(r"^<.+>$", grbl_message, re.IGNORECASE)
                    idle = grbl_message.lower().find("idle") > -1
                    alarm = grbl_message.upper().find("ALARM") > -1
                    error = grbl_message.upper().find("ERROR") > -1

                    # If alarm, emit and stop
                    if report and alarm:
                        if self.verbose:
                            print("\ndriver message:\n    update_sio_wpos got an alarm state.")
                        self.sio_emit('alert', {'text': "update_sio_wpos: ALARM message from GRBL: '" + re.sub("[<>]", "", grbl_message), 'type': 'alarm'})
                        while self.serial.in_waiting:
                            discard = self.serial.readline().decode()
                        poll = False

                    # If WPOS, send it to the websocket
                    elif report and idle:
                        if self.verbose:
                            print("\ndriver message:\n    update_sio_wpos: Emitting WPOS from status: " + grbl_message)
                        self.sio_emit_wpos(grbl_message)
                        while self.serial.in_waiting:
                            discard = self.serial.readline().decode()
                        poll = False

                # Timeout check
                if endtime - time.time() <= 0.0:
                    if self.verbose:
                        print("\ndriver message:\n    update_sio_wpos timed out.")
                    poll = False

    def fast_jog(self, fast_grbl_command, jog_feed_rate='F1000', status_tries=5):
        if self.verbose:
            print("\ndriver message:\n    fast_jog: Preparing Jog command: " + "$J=" + fast_grbl_command + '\n')

        # "Flush input buffer, discarding all its contents."
        # https://pyserial.readthedocs.io/en/latest/pyserial_api.html
        # NOTE: I don't recall why this was important.
        # self.serial.reset_input_buffer()

        # Build command
        coord = re.search(r"([XYZ]-{0,1}[\d\.]+)", fast_grbl_command, re.IGNORECASE).group(1)
        command = "$J=G91 " + coord + ' ' + jog_feed_rate + '\n'

        # An "ok" will be received, so we must register a command in the input_queue to avoid errors.
        # It has no side effects, the jog commands are ignored by the serial_writer.
        if self.verbose:
            print("\ndriver message:\n    fast_jog: sending Jog command to input_queue: " + "$J=" + fast_grbl_command + '\n')
        self.input_queue.put(command)

        # Send Jog command
        self.serial.write(str.encode(command))

        # "Wait until all data is written"
        # https://pyserial.readthedocs.io/en/latest/pyserial_api.html
        if self.verbose:
            print("\ndriver message:\n    fast_jog: Waiting until all data is written. Flushing...")
        self.serial.flush()

        # Wait for an IDLE GRBL (using the DWELL command).
        if self.verbose:
            print("\ndriver message:\n    fast_jog: Waiting for input_queue join...")
        self.wait_for_grbl(sleepy_time=0)
        
        # To avoid the character counting error in serial_writer,
        # get the 'ok' queue item and mark it done:
        self.ok_queue.get()
        self.ok_queue.task_done()

    def wake_grbl(self):
        if self.verbose:
            print("\ndriver message:\n    wake_grbl: Waking GRBL...")

        # Wake up GRBL
        self.serial.write(str.encode("\r\n\r\n"))
        # Wait for grbl to initialize  # TODO: improve logic, sleep is unreliable...
        time.sleep(2)

        if self.verbose:
            print("\ndriver message:\n    wake_grbl: Resetting input buffer...")

        # Just in case
        # See: https://github.com/michaelfranzl/gerbil/blob/e6828fd5a682ec36970de83d38a0bea46b765d8d/interface.py#L69
        # Cleanup serial buffers: https://pyserial.readthedocs.io/en/latest/pyserial_api.html
        # Clear output buffer, aborting the current output and discarding all that is in the buffer.
        self.serial.reset_output_buffer()
        # Flush input buffer, discarding all its contents.
        self.serial.reset_input_buffer()  # Flush startup text in serial input

    def status(self, timeout=None, raise_error=False):
        """
        Report utility function.

        See: https://github.com/gnea/grbl/wiki/Grbl-v1.1-Interface#real-time-status-reports
        """
        
        if self.verbose and self.request_queue.unfinished_tasks > 0:
            print("\ndriver message:\n    status Warning: waiting before sending request. The request_queue has pending status requests: " + str(self.request_queue.unfinished_tasks))

        # Get the one queue item, thereby blocking other requests, and/or wait until other requests are done.    
        self.request_queue.get()
        self.serial.write('?'.encode())  # Send "?"
        
        # Setup timed out flag
        timed_out = False

        if timeout is None:
            # Wait for task_done in serial_reader to update with no time limit.
            self.request_queue.join()
        else:
            endtime = time.time() + timeout
            # Wait for request_queue.task_done() in serial_reader (meaning a status report arrived).
            while self.request_queue.unfinished_tasks > 0:
                if endtime - time.time() <= 0.0:
                    if raise_error:
                        raise GrblError("\ndriver message:\n    status ERROR: timed out waiting for status report.")
                    else:
                        print("\ndriver message:\n    status: Timed out waiting for status report.")
                        timed_out = True
                        break
                time.sleep(self.serial_response_delay/4)

        # Flag updated events
        if timed_out:
            idle, alarm = None, None
        else:
            idle = self.idle_event.is_set()
            alarm = self.alarm_event.is_set()

        # Put back the item, releasing status requests in other functions.
        self.request_queue.put("idle")

        return idle, alarm

    def wait_for_idle(self, timeout=None, raise_error=False):
        """
        Report utility function. Makes status requests until GRBL reports IDLE state.
        Timeout is optional, set to None to keep asking for reports. This will block your thread.
        """
        
        # Request a status update
        idle, alarm = self.status(timeout=self.serial_response_delay,
                                  raise_error=False)

        # If there is no timeout, request a status reports with minimal timeout,
        # and keep doing it until (idle) forever.
        if timeout is None:
            if self.verbose:
                print("\ndriver message:\n    wait_for_idle: Waiting for idle (no time out).")
            while not idle:
                idle, alarm = self.status(timeout=self.serial_response_delay,
                                          raise_error=False)
                if alarm:
                    print("\ndriver message:\n    wait_for_idle: Alarm fired while waiting for idle: " + str(timeout))
                    if raise_error:
                        raise GrblError("\ndriver message:\n    wait_for_idle ERROR: alarm while waiting for idle status report.")
                    break
        # If there is a timeout, use it:
        else:
            if self.verbose:
                print("\ndriver message:\n    wait_for_idle: Waiting for idle with timeout: " + str(timeout))
            endtime = time.time() + timeout
            while not idle:
                idle, alarm = self.status(timeout=self.serial_response_delay,
                                          raise_error=False)
                if endtime - time.time() <= 0.0:
                    print("\ndriver message:\n    wait_for_idle WARNING: timed out waiting for status report.")
                    if raise_error:
                        raise GrblError("\ndriver message:\n    wait_for_idle ERROR: timed out waiting for status report.")
                    else:
                        break
        
        # Return last update result
        return idle, alarm

    def queue_grbl_command(self, line):
        """
        Use this function to mediate sending of GCODE commands.
        It only wraps a few common commands.
        It ignores jogging commands.
        """
        
        if self.dry:
            if self.verbose:
                print('\nqueue_grbl_command:\n   queue_grbl_command: Dry run, skipping command: ' + line.strip())
            return None
        
        # Strip all EOL characters and whitespace for consistency
        command = line.strip()
        if command.startswith(';'):
            if self.verbose:
                print('\nqueue_grbl_command:\n   queue_grbl_command: ignoring ";" command: ' + command)
        # Remove comments from command
        if command.find(';') > -1:
            command = command.split(";")[0]
            if self.verbose:
                print('\nqueue_grbl_command:\n   queue_grbl_command: removed comments from command: ' + command)
        
        if self.verbose:
            print('\nqueue_grbl_command:\n   queue_grbl_command: Sending command to serial_writer queue: ' + command)
        
        # Ignore JOG commands.
        # # Jogs should be sent directly to the serial interface, 
        # # because commands starting with "$" are handled specially below (as settings or homing),
        # # and it didn't make sense to go through all the serial_writer stuff for a quick Jog.
        if command.startswith("$J"):
            print('\nqueue_grbl_command:\n   queue_grbl_command: Jogging command ignored: ' + command)
            return command
        # # Note: The serial_reader will produce a warning if an "ok" is received without
        # # a corresponding task in the input_queue (i.e. when a Jog is sent through the serial interface).

        # All commands will wait here for a green light, except for "$X".
        # # If it is the unlock command, proceed to send it immediately.
        # # The gcode_unlock flag may have been set by a "$" setting or homing line sent previously.
        # # Before sending any further GCODE, we wait for the serial_writer to re-set the gcode_unlock event
        # # (which happens after the OK is received).
        # # However, it makes sense that this should _not_ apply to an unlock command.
        if not command.startswith("$X"):
            if self.verbose:
                print('\nqueue_grbl_command:\n   queue_grbl_command: Waiting for gcode_unlock: ' + command)
            self.gcode_unlock.wait()  # Wait for it to be "set"...
        
        # Accordingly, if a setting line subsequent protocol parsing must be blocked.
        # # The blockade is eventually released by the "serial_writer" thread,
        # # after receiving and parsing the "$" command, and getting an "ok" response.
        if command.startswith("$"):
            if self.verbose:
                print('\nqueue_grbl_command:\n   queue_grbl_command: Sending $ command, locking further gcode. ' + command)
            self.gcode_unlock.clear()

        # Send the command to the queue read by the "serial_writer" thread
        self.input_queue.put(command)

        # Hold the main thread until gcode unlocks
        if self.verbose:
            print('\nqueue_grbl_command:\n   queue_grbl_command: Waiting for gcode_unlock: ' + command)
        #  Wait for gcode_unlock to be set by the serial_writer.
        self.gcode_unlock.wait()  # Wait for it to be "set"... (or just continue if it is).
        
        # Done!
        if self.verbose:
            print('\nqueue_grbl_command:\n   queue_grbl_command: Done sending command: ' + command)

        return command
    
    def wait_for_grbl(self, sleepy_time=0, dwell_time=0.001):
        """
        Wait for an "empty" and "done" input_queue (no grbl commands in queue).
        Does not imply IDLE state, unless no other GCODE is queued after this command.
        """
        
        # Sleep some time just in case
        if sleepy_time is None:
            # Set a default value if None.
            sleepy_time = self.serial_response_delay
        time.sleep(sleepy_time)
        
        if self.verbose:
            print("\ndriver message:\n    wait_for_grbl: sending G4 dwell command.")
        
        # Queue the DWELL command
        self.queue_grbl_command("G4 P" + str(dwell_time))  # Previously P0
        
        # Wait here
        # # while the input_queue is still being processed (i.e. not enough "get" calls yet),
        # # and while the tasks have not been marked as done (by an "ok" response)
        # # just wait...
        while (not self.input_queue.empty()) or (self.input_queue.unfinished_tasks > 0):
            if self.verbose:
                print("\ndriver message:\n    wait_for_grbl: waiting for empty input_queue, and requesting status report.")
            # Request a status, and timeout (without raising errors) after self.serial_response_delay
            self.status(timeout=self.serial_response_delay*2, raise_error=False)
            
            # Check if alarms were set
            if self.alarm_event.is_set():
                raise GrblError("\ndriver message:\n    wait_for_grbl: alarm event raise while waiting for input_queue join")

            # Wait a second before checking again
            time.sleep(1)
        else:
            if self.verbose:
                print("\ndriver message:\n    wait_for_grbl: done waiting for empty input_queue :)")

    def start_threads(self, restart=True):
        # If the threads are already running, do nothing.
        if self.running_event.is_set():
            print("\ndriver message:\n    start_threads: threads already running, doing nothing!")
            return False
        
        # Set initial state of events and queues
        if self.verbose:
            print("\ndriver message:\n    start_threads: Setting flags and queues...")
        self.alarm_event.clear()  # GRBL ALARM status flag.
        self.idle_event.clear()  # GRBL IDLE status flag.
        self.gcode_unlock.set()  # Ready for GCODE
        self.unlocked_event.set()  # GRBL not locked
        self.reset_event.clear()  # GRBL not reset
        self.probe_event.clear()  # GRBL not reset
        
        # Wake GRBL and reset serial buffers (just in case)
        self.wake_grbl()
        
        # Prime the report request queue
        if not self.request_queue.empty():
            self.request_queue.get()
        if self.request_queue.unfinished_tasks > 0:
            self.request_queue.task_done()
        self.request_queue.put("init")
        
        # Start the GRBL handler threads
        if self.verbose:
            print("\ndriver message:\n    start_threads: Starting threads...")
        self.reader_thread.start(), self.writer_thread.start()  # call the thread functions
        
        # Give the green light
        if self.verbose:
            print("\ndriver message:\n    start_threads: Setting thread run event...")
        self.running_event.set()  # release the "wait" on the thread functions
        
        # Check GRBL status, and fix alarm/reset state if any.
        if restart:
            restart_status = self.restart_grbl()
            if restart_status:
                while not self.error_queue.empty():
                    error = self.error_queue.get()
                    print("\ndriver message:\n    start_threads: Cleared error queue after restart_grbl call.")
            
        # Done!
        if self.verbose:
            print("\ndriver message:\n    start_threads: done!")
        return True

    def restart_grbl(self, error=True):
        # Update status report
        if self.verbose:
            print("\ndriver message:\n    restart_grbl: Waiting for status report 1 (for 1 second)...")
        # Request a status update
        idle, alarm = self.status(timeout=1,  # self.serial_response_delay*2,
                                  raise_error=False)
        
        # If timed-out, soft-reset.
        if idle is None:
            self.soft_reset()
            # Wait for a bit, just in case.
            time.sleep(2)
        
        # Update status report
        if self.verbose:
            print("\ndriver message:\n    restart_grbl: Waiting for status report 2...")
        # Request a status update
        idle, alarm = self.status(timeout=1,
                                  raise_error=False)
        
        # If timed out again, hard-reset.
        if idle is None:
            print("\ndriver message:\n    restart_grbl: Alarm status detected, hard-resetting the Arduino...")
            self.arduino_GPIO_reset()
            time.sleep(4)
        
        # Update status report
        if self.verbose:
            print("\ndriver message:\n    restart_grbl: Waiting for status report 3...")
        # Request a status update
        idle, alarm = self.status(timeout=1,
                                  raise_error=False)
        
        # Raise error if it happens again
        if idle is None:
            if error:
                raise GrblError("\ndriver message:\n    restart_grbl ERROR: could not get a status report from GRBL.")
            else:
                print("\ndriver message:\n    restart_grbl ERROR: could not get a status report from GRBL.")
                return False
        
        # Update status report
        if self.verbose:
            print("\ndriver message:\n    restart_grbl: Waiting for status report 4...")
        # Request a status update
        idle, alarm = self.status(timeout=1,
                                  raise_error=False)
        
        # If in alarm status, send unlock code.
        if alarm:
            print("\ndriver message:\n    restart_grbl GRBL in Alarm/Error state: trying to unlock with $X...")
            # Send unlock code to GRBL input_queue.
            # Since it is a setting line, it will wait/block for an "ok".
            self.queue_grbl_command("$X")
            time.sleep(1)
        
        # Update status report
        if self.verbose:
            print("\ndriver message:\n    restart_grbl: Waiting for status report 5...")
        # Request a status update
        idle, alarm = self.status(timeout=1,
                                  raise_error=False)
        
        # Handle other possible outcomes
        if alarm is None:
            if error:
                raise GrblError("\ndriver message:\n    restart_grbl ERROR: could not get a status report from GRBL.")
            else:
                print("\ndriver message:\n    restart_grbl did not complete successfully; idle:" + str(idle) + ", alarm:" + str(alarm))
                return False
        elif alarm:
            if error:
                raise GrblError("\ndriver message:\n    restart_grbl ERROR: could not clear ALARM state from GRBL.")
            else:
                print("\ndriver message:\n    restart_grbl did not complete successfully; idle:" + str(idle) + ", alarm:" + str(alarm))
                return False
        
        if idle and not alarm:
            if self.verbose:
                print("\ndriver message:\n    restart_grbl completed successfully! idle:" + str(idle) + ", alarm:" + str(alarm))
            return True
        else:
            if error:
                raise GrblError("\ndriver message:\n    restart_grbl ERROR: unexpected state at the end of restart_grbl(); idle:" + str(idle) + ", alarm:" + str(alarm))
            else:
                print("\ndriver message:\n    restart_grbl did not complete successfully; idle:" + str(idle) + ", alarm:" + str(alarm))
                return False

    def check_grbl_status(self):
        # Update status report
        print("\ndriver message:\n    check_grbl_status: Waiting for status report 1...")
        idle, alarm = self.wait_for_idle(timeout=3)
        # Return if everything checks out
        if idle and not alarm:
            return True

        # Try resetting the arduino if the reset flag is up.
        if self.reset_event.is_set():
            print("\ndriver message:\n    check_grbl_status: Alarm status detected, hard-resetting the Arduino...")
            self.arduino_GPIO_reset()
        
        # Wait for the reset, and at least one update.
        print("\ndriver message:\n    check_grbl_status: Waiting for status report 2...")
        idle, alarm = self.wait_for_idle(timeout=2)
        
        # Si está bloqueado, pero tampoco se requiere reset,
        # significa que leímos el mensaje de "$X for unlock" (ver serial_reader).
        if self.alarm_event.is_set() and not self.reset_event.is_set():
            print("\ndriver message:\n    check_grbl_status: GRBL Alarm/Error, trying to unlock with $X...")

            # Send unlock code to GRBL input_queue.
            # Since its a setting line, it will wait/block for an "ok".
            self.queue_grbl_command("$X")

            # Wait for idle
            print("\ndriver message:\n    check_grbl_status: Waiting for idle GRBL report 3...")
            idle, alarm = self.wait_for_idle(timeout=2)

        if idle and not alarm:
            return True
        else:
            raise GrblError("\ndriver message:\n    check_grbl_status: ERROR: GRBL couldnt be set to IDLE.")

    def steppers_on(self):
        # Set the stepper idle timeout in GRBL (enable steppers if 255)
        if self.stepper_idle_timeout is not None:
            # Type and range checks
            assert isinstance(self.stepper_idle_timeout, int) and 0 <= self.stepper_idle_timeout <= 255
            # input_queue.put("$1=" + str(stepper_idle_timeout) + "\n")
            self.queue_grbl_command("$1=" + str(self.stepper_idle_timeout) + "\n")
            time.sleep(self.serial_response_delay)

        return self.stepper_idle_timeout

    def steppers_off(self, direct=False):
        if not direct:
            # Send through the threads
            print("\ndriver message:\n    steppers_off: Disabling steppers through the threads...")
            self.queue_grbl_command("$1=20")
            self.queue_grbl_command("G91 G1 Z-0.01 F1000")
        else:
            # Send directly to the interface
            try:
                print("\ndriver message:\n    steppers_off: Disabling steppers directly...")
                self.serial.write(str.encode("$1=20"))
                self.serial.write(str.encode("G91 G1 Z-0.01 F1000"))
            except:
                print("\ndriver message:\n    steppers_off: Failed to disable steppers directly...")

    def run_protocol(self,
                     wait_prefix="Wait;",
                     human_intervention_prefix="HUMAN",
                     s_axis_home_prefix="Phome;",
                     s_axis_displacement_prefix="Pmove;",
                     # s_axis_eject_prefix="Peject;",
                     toolchange_prefix="toolchange;",
                     tip_probe_prefix="PRB;"
                     ):
        
        # Prepare exit code variable
        exit_code = 0
        
        # If another protocol is not in progress
        if not self.protocol_event.is_set():
            # Block other protocols from starting
            self.protocol_event.set()
        else:
            # Warn the user and return, doing nothing.
            self.sio_emit('alert',
                          {'text': "run_protocol: a protocol is already in progress. If this is a mistake, you can kill it.",
                           'type': 'alarm'}
                          )
            return False

        if self.verbose:
            # Get the command list, and print it
            pprint.pprint(self.protocol)

        try:
            # Machine setup
            print("\ndriver message:\n    run_protocol: Starting protocol!")
            
            print("\ndriver message:\n    run_protocol: Waking GRBL.")
            self.wake_grbl()
            
            if not self.running_event.is_set():
                print("\ndriver message:\n    run_protocol: Starting threads.")
                self.start_threads()
            
            print("\ndriver message:\n    run_protocol: Check GRBL status.")
            self.check_grbl_status()
            
            print("\ndriver message:\n    run_protocol: Turning on the steppers.")
            self.steppers_on()

            # Run protocol
            # Setup protocol line counter
            i = 0

            # Iterate over protocol lines
            while i < self.protocol.__len__() and not self.killed:

                # Get a new protocol line
                line = self.protocol[i]

                # Print protocol step
                if self.verbose:
                    print("\ndriver message:\n    run_protocol: Protocol step {} of {}".format(i + 1, self.protocol.__len__()))
                    print("\ndriver message:\n    run_protocol: Now parsing command: {protocol}\n".format(protocol=self.protocol[i]))

                # Check for errors in threads, and stop if any are found.
                if not self.error_queue.empty():
                    while not self.error_queue.empty():
                        error = self.error_queue.get()
                        print("\ndriver message:\n    run_protocol: Errors detected by the threading functions: " + error)
                    raise GrblError("run_protocol: Alert during protocol streaming: errors detected by the threading functions.")

                # Check for alarms or errors from GRBL
                if self.alarm_event.is_set():
                    raise GrblError("\ndriver message:\n    run_protocol: alarm event raised while running protocol.")

                # Interactive mode ####
                # Offer a rudimentary menu if interactive mode is enabled, useful for debugging.
                if self.interactive:
                    print(
                        "\ndriver message:\n" +
                        "    run_protocol: Enter single command and press enter (gcode, q: quit, n: next, s: skip).\n" +
                        "    run_protocol: Current command:{}".format(self.protocol[i]))
                    line = input(" > ")
                
                    if line == "?":
                        self.serial.write(str.encode("?"))
                        time.sleep(0.2)  # TODO: improve logic, sleep is unreliable...
                        continue
                
                    # Skip a line from the protocol
                    elif line == "s":
                        if i == self.protocol.__len__() - 1:
                            i = 0
                            input("Reached end of protocol. Press enter to start over...")
                        else:
                            print("\ndriver message:\n    run_protocol: Skipping line {}: {}".format(i, self.protocol[i]))
                            i += 1
                        continue
                
                    # Quit
                    elif line == "q":
                        print("run_protocol: Terminating interactive session...")
                        break
                
                    # Send next line from the protocol
                    elif line == "n" or line == "":
                        if i == self.protocol.__len__() - 1:
                            input("run_protocol: Reached end of protocol, press enter to start over...")
                            i = 0
                            continue
                        else:
                            line = self.protocol[i]
                            print("\ndriver message:\n    run_protocol: Sending protocol line: {}".format(line))
                            # i += 1
                
                    # Send the input to GRBL
                    else:
                        # Decrement the line index here,
                        # as it will be undesirably incremented
                        # at the end of the loop
                        i -= 1

                # Process commands ####

                # First, wait for gcode block release (by the serial_writer) if any.
                # # It can only come from having sent homing or setting commands.
                # # This line is necessary because "idle_event.wait()" was not enough, the issue being
                # # that "idle_event" could be mistakenly set during a short period after sending a $H command,
                # # because there is a lag between serial.write and a non-idle status request response.
                # # This leads to "leaky" execution of the next protocol line
                # # (xej: the line that followed homing command).
                # # TODO: replace this logic with waiting for an "ok" from a dwell command sent after $H.
                if self.verbose and not self.gcode_unlock.is_set():
                    print("\ndriver message:\n    run_protocol: gcode_unlock is not set, waiting before sending another commmand...")
                while not self.gcode_unlock.is_set():
                    time.sleep(self.serial_response_delay/4)  # TODO: possible speed-up point.
                    if self.alarm_event.is_set():  # Just in case
                        raise GrblError("run_protocol: GRBL Alarm/Error ocurred while waiting for gcode_unlock event, protocol terminated at step {} of {}".format(i + 1, self.protocol.__len__()))
                # # Note: upon a setting line, the serial_writer ends up waiting for:
                # # input_queue.join()  # gather "ok"s
                # # idle_event.wait()   # ensure idle state

                # Still, the same could happen when a "move" gcode is sent, resulting in the same problem,
                # # even though we are waiting for "idle_event" to be set.
                # # A solution would be to lock "tools" (non GRBL commands) until all "ok"s are collected
                # # and GRBL reports IDLE. That is marked by a "joined" input_queue, therefore:
                # input_queue.join()
                # # But this must be checked in each tool's "if" clause.
                # # I do not use "gcode_unlock.wait()" because updating that one depends on sending
                # # gcode with input_queue.put(), while input_queue.join() only depends on the serial_reader.

                # Skip everything if dry mode is on
                if self.dry:
                    if self.verbose:
                        print("\ndriver message:\n    run_protocol: dry mode; skipping line: " + str(i) + line)
                    print(i, line)
                    i += 1
                    continue
                # Skip protocol comments
                if line.startswith(";"):
                    if self.verbose:
                        print("\ndriver message:\n    run_protocol: skipping comment line: " + str(i) + line)
                    i += 1
                    continue
                # Skip homing if semi_dry mode
                elif line.strip().startswith("$H") and self.semi_dry:
                    pass  # Move on

                # Wait action
                elif line.startswith(wait_prefix):
                    self.wait(line)
                
                # Human action
                elif line.startswith(human_intervention_prefix):
                    self.human(line, i)
                
                # Pipette home action
                elif line.startswith(s_axis_home_prefix):
                    self.pipette_home(line, i)
                
                # Pipette move actions
                elif line.startswith(s_axis_displacement_prefix):
                    self.pipette_displace(line, i)
                    
                # Pipette tip probe actions
                elif line.startswith(tip_probe_prefix):
                    self.tip_probe(line, i) 
                
                # Pipette tip eject actions:
                # elif line.startswith(s_axis_eject_prefix):
                #      # DEPRECATED: REPLACED BY TIP-EJECTION-POST GCODE MACRO!
                #     self.tip_eject(line, i)
                
                # This marks that a tool change will happen next
                elif line.startswith(toolchange_prefix):
                    self.tool_change(line, i)
                
                # Else, stream GRBL/GCODE actions to GRBL:
                else:
                    self.queue_grbl_command(line)

                # Go get another line from the protocol list object...
                i += 1
                # // end of the protocol loop //
        except KeyboardInterrupt as e:
            # https://stackoverflow.com/a/13181036
            print("\ndriver message:\n    run_protocol: cleaning up after keyboard interrupt:\n    ", e)
            tb = traceback.format_exc()
            print(tb)
            self.cleanup()
        except GrblError as e:
            print("\ndriver message:\n    run_protocol: GRBL error exception called during protocol streaming:\n    ", e)
            tb = traceback.format_exc()
            print(tb)
            exit_code = 1
            self.cleanup()
        except Exception as e:
            print("\ndriver message:\n    run_protocol: unexpected exception called during protocol streaming:\n    ", e)
            tb = traceback.format_exc()
            print(tb)
            exit_code = 2
            self.cleanup()

        else:
            print("\ndriver message:\n    run_protocol: Disabling steppers.")
            self.steppers_off()  # Turn off steppers
            
            print("\ndriver message:\n    run_protocol: Protocol run ended.")
            self.protocol_event.clear()
        
        return exit_code
    
    def tool_change(self, line, i):
        new_pipette = line.split(";")[1]
        print("\ndriver message:\n    tool_change: toolchange detected. From: " + self.pipette.pipette_model + " to " + new_pipette)
        
        self.pipette.configure_pipette(new_pipette)
        print("\ndriver message:\n    tool_change: New pipette configured: " + self.pipette.pipette_model)

    def send_to_serial(self, command):
        # Strip comments/spaces/new line and capitalize. Note: los comentarios son cosas entre paréntesis.
        command = re.sub(r'\s|\(.*?\)', '', command).upper()
        self.serial.write(str.encode(command + '\n'))  # Send command to serial directly
        self.serial.flush()                            # Wait until all data is written

    def wait(self, line, i=None):
        
        # Wait for input_queue join
        self.wait_for_grbl()

        seconds = float(line.split(";")[1])
        if self.verbose:
            print("\ndriver message:\n    wait: Waiting for " + str(seconds) + " seconds.\n")
        time.sleep(seconds)  # TODO: improve logic, sleep is unreliable...

        if self.verbose:
            print("\ndriver message:\n    wait: Done waiting for " + str(seconds) + " seconds.\n")

    def human(self, line, i):
        
        # Wait for input_queue join
        self.wait_for_grbl()

        socket_pause = True
        message = str(line.split(";")[1])
        if self.verbose:
            print("\ndriver message:\n    human: Waiting for HUMAN with message: " + message)
        if self.socket_use:
            self.sio_emit('human_intervention_required', {'text': message})
            while socket_pause and not self.alarm_event.is_set() and not self.killed:
                if self.verbose:
                    print("\ndriver message:\n    human: Waiting for websockets...")
                time.sleep(1)  # TODO: improve logic, sleep is unreliable...
                if self.socket_abort:
                    raise GrblError(
                        "human: Abort human action! Intervention cancelled on command " + str(i) + ": " + line)

    def pipette_home(self, line=None, i=None):
        """
        Homes the pipette tools
        @param line: A command, as generated by the protocol builder.
        For example: "Phome;all", "Phome;p200", "Phome;" or "Phome;p200,p20".
        @param i: The action index associated with the command.
        """
        if self.verbose:
            print("\ndriver message:\n    pipette_home: Homing pipette (after GRBL idle)...")
        
        # Wait for IDLE message from GRBL
        self.wait_for_grbl()
        
        if self.verbose:
            print("\ndriver message:\n    pipette_home: GRBL is IDLE, proceeding...")
        
        # Save current pipette model
        current_pipette = self.pipette.pipette_model
        
        # Parse tool name if any
        if line is None:
            # If none, then home whatever is active.
            # NOTE: When would this happen? It should happen at least when homing from the GUI.
            self.pipette.home_pipette()
        else:
            # If not None, get the tool names we wish to home
            # Remove the prefix, and keep the tool names.
            tool_name = line.split(";")[1]

            # Get tool names
            if tool_name == "all":
                # Use the list with all the tool names
                tool_names = self.pipette_models
            elif tool_name == "":
                # Home the current pipette if none was specified
                tool_names = current_pipette
            else:
                # Else just use whatever names the command provided.
                # Added support for comma-separated names, to home multiple tools.
                tool_names = tool_name.split(",")

            # Home tools one by one
            for tool in tool_names:
                self.pipette.configure_pipette(tool)
                if self.verbose:
                    print("\ndriver message:\n    pipette_home: homing tool with name:" + str(tool))
                self.pipette.home_pipette()
            # Reconfigure original tool
            self.pipette.configure_pipette(current_pipette)
        
        if self.verbose:
            print("\ndriver message:\n    pipette_home: Pipette homing done! for command: " + str(i))

        # Update pipette position
        # TODO: make this multi-tool capable
        self.sio_emit_p_pos(self.pipette.pipette["current_volume"])

    def pipette_displace(self, line="Pmove;0", i=None):
        if self.verbose:
            print("\ndriver message:\n    pipette_displace: Pipetting displacement (after GRBL idle)...")
        
        # Wait for all "ok"s
        self.wait_for_grbl()

        # Get millimeters
        millimeters = float(line.split(";")[1])

        # Displace pipette
        self.pipette.displace(displacement=millimeters)
        
        # Print messages
        if self.verbose:
            print("\ndriver message:\n    pipette_displace: Pipetting done!" + str(millimeters) + " mm, for command: " + str(i))
        
        # Update pipette position
        # TODO: use new "updating" system in serial_reader somehow...
        self.sio_emit_p_pos(self.pipette.pipette["current_volume"])

    def tip_eject(self, line=None, i=None):
        """
        DEPRECATED: we no longer eject pipette tips using a servo, the motion is planned by the GCODE builder. There is no replacement function.
        TODO: perhaps we _should_ have a tip ejection macro defined here. It is tool-specific, but so are the tip placement macros.
        @param line: Tip ejection command "Peject;".
        @param i: The action index associated with the command.
        """
        if self.verbose:
            print("\ndriver message:\n    tip_eject: Ejecting tip (after GRBL idle)...")

        # Wait for IDLE message from GRBL
        self.wait_for_grbl()

        # Eject tip with servo
        self.pipette.eject()

        if self.verbose:
            print("\ndriver message:\n    tip_eject: Tip ejected! with command: " + str(i))

    def tip_probe(self, line="PRB;$J=G91 Z-5 F500", i=None, timeout=5):
        """
        TODO: implement a tip probing function for the optostops in each pipette.
        Rely on the Pipette class.
        """
        # Parse the jog command
        command = line.split(";")[1].strip()
        if self.verbose:
            print("\ndriver message:\n    tip_probe: Starting GPIO tip probing with command: " + command)
        
        # Wait for IDLE GRBL
        self.wait_for_grbl()
        
        # Send Jog command directly, it will not occupy space in GRBL's serial buffer.
        # An OK will be received by the serial_reader
        self.serial.write(str.encode(command + '\n'))
        
        # Wait for the probe to fire, then cancel the Jog.
        # This will hang until the GPIO receives a LOW signal.
        result = self.pipette.probe_tip_wait(timeout=timeout)
        
        # Interrupt the Jog
        self.serial.write(str.encode("!"))  # feed-hold
        time.sleep(self.serial_response_delay)
        self.serial.write(str.encode("~"))  # resume
        
        # To avoid the character counting error in serial_writer,
        # get the 'ok' queue item corresponding to the jog command, and mark it done:
        self.ok_queue.get()
        self.ok_queue.task_done()
        
        if not result:
            raise GrblError("\ndriver message:\n    tip_probe: ERROR: timed out waiting for GPIO probe.")

    def status_probe(self, axis="Y", distance=5, mode="G91", jog_feed_rate='F200', timeout=10):
        """
        Use "Pn:P" status messages to probe, instead of using a sensor.
        This can be easily extended to use any limit switch signal: XYZDHRS (besides P).
        Just change the regex code in serial_reader, to recognize any of those events.
        """
        
        # Wait for IDLE message from GRBL
        if self.verbose:
            print("\ndriver message:\n    status_probe: Probing for tool parking (after GRBL idle)...")
        self.wait_for_grbl()
        
        """
        Unlike fast_jog, this function does not wait for GRBL to be idle by sending a G4 "dwell" command.
        Arguments are also different.
        """

        # Build command
        # Example: $J=G91 Y5 F500
        command = "$J=" + mode + " " + axis.upper() + str(distance) + ' ' + jog_feed_rate + '\n'
        if self.verbose:
            print("\ndriver message:\n    status_probe: Sending Jog command: " + command + '\n')
        
        # Setup fast status request mode, do this before sending the Jog command.
        # Get the one request_queue item, thereby blocking other status requests, and/or wait until other requests are done.
        self.request_queue.get()
        
        # Set probe event
        self.probe_event.set()

        # Send Jog command directly, it will not occupy space in GRBL's serial buffer.
        # An OK will be received by the serial_reader
        self.serial.write(str.encode(command))

        # "Wait until all data is written"
        # https://pyserial.readthedocs.io/en/latest/pyserial_api.html
        # if self.verbose:
        #     print("\ndriver message:\n    Waiting until all data is written. Flushing...")
        # self.serial.flush()
        
        # Wait for the probe event
        if self.verbose:
            print("\ndriver message:\n    status_probe: Waiting for the probe event...")
        # While it is not set, request status reports. And do it fast!
        t = 0
        while self.probe_event.is_set():
            if t > timeout:
                print("\ndriver message:\n    status_probe WARNING: Timed out waiting for the probe event...")
                break
            else:
                t += self.serial_response_delay/16
            # Same value as in serial_reader
            time.sleep(self.serial_response_delay/16)
        
        # Send a feed-hold realtime command immediately after the wait
        if self.probe_event.is_set():
            print("\ndriver message:\n    status_probe WARNING: Clearing probe_event because the serial_reader did not :(...")
            self.probe_event.clear()
        
        # Release the status reports
        self.request_queue.put("thank you!")
        
        # To avoid the character counting error in serial_writer,
        # get the 'ok' queue item corresponding to the jog command, and mark it done:
        self.ok_queue.get()
        self.ok_queue.task_done()

    def cleanup(self, thread_join_timeout=1.0):
        print("\ndriver message:\n    cleanup: Cleaning up!")
        
        if self.__clean:
            print("\ndriver message:\n    cleanup: Already cleaned up it seems... cleaning up anyways!")
        
        # Just in case
        # self.serial.flushInput()  # Commented out because I don't know what consequences it has.
        # self.serial.flushOutput() # Commented out because I don't know what consequences it has.

        # If the threads were setup:
        if self.running_event.is_set():

            # Disable steppers before quitting.
            print("\ndriver message:\n    cleanup: Disabling steppers...")
            self.steppers_off()

            # Stop the threads
            print("\ndriver message:\n    cleanup: Stop GRBL threading monitors")
            self.running_event.clear()
            self.writer_thread.join(timeout=thread_join_timeout)
            self.reader_thread.join(timeout=thread_join_timeout)
            # Check full stop
            if self.writer_thread.is_alive() or self.reader_thread.is_alive():
                print("\ndriver message:\n    cleanup: WARNING could not stop the threads, joining timed-out.")
                self.steppers_off(direct=True)
        else:
            # Try to turn off the steppers directly if the threads aren't running.
            self.steppers_off(direct=True)

        # Cleanup serial interface
        print("\ndriver message:\n    cleanup: Close serial connection")
        self.serial.close()

        # Close socket
        print("\ndriver message:\n    cleanup: Disconnecting websocket...\n")
        self.sio_disconnect()

        # Cleanup pipette and GPIOs
        print("\ndriver message:\n    cleanup: Cleanup pipette class (pigpio and GPIO)")
        self.pipette.close()  # Includes GPIO cleanup
        
        # Set cleanup flag: True means the Commander has been "cleaned up" and needs re-instantiating.
        self.__clean = True

        print("\ndriver message:\n    cleanup: Done cleaning up. Have a nice day! :)\n")
