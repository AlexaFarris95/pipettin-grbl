import time
import re
from grbl_error_codes import get_error_by_code
from grbl_alarm_codes import get_alarm_by_code
import collections.abc


def update(d, u):
    """
    Recursive dictionary update, as per: https://stackoverflow.com/a/3233356
    """
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            d[k] = update(d.get(k, {}), v)
        else:
            d[k] = v
    return d


class GrblError(Exception):
    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        if self.message:
            return 'GRBL error: the machine needs to be reset: {0} '.format(self.message)
        else:
            return 'GRBL error has been raised!'


class PollThreadError(Exception):
    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        if self.message:
            return 'Polling thread error: {0} '.format(self.message)
        else:
            return 'PollThreadError has been raised!'


def serial_reader(serial_interface,
                  output_queue, 
                  input_queue, ok_queue,
                  request_queue, error_queue,
                  protocol_event,
                  idle_event, running_event,
                  alarm_event, unlocked_event, 
                  reset_event, probe_event,
                  sio, socket_use,
                  serial_response_delay=0.1,
                  verbose=False):
    # Wait for polling request by "running_event.wait()"
    print('\nserial_reader:\n    waiting for running_event...')
    running_event.wait()
    
    # Setup exit code
    e_code = 0
    
    # Function to update the dictionary holding position/status info
    def tool_data_update(new=None, old=None, keep_prev=True):

        # PyCharm replaced "mutable" default arguments with this:
        if old is None:
            old = {}
        if new is None:
            new = {}

        default = {'position': {
            "x": None,
            "y": None,
            "z": None,
            "p": None,  # TODO: conseguir info de la posicion de la pipeta del SAxis driver y pasarla acá
            "status": None
            }
        }
        
        # Make sure all fileds are there.
        filled = update(default, old)
        # Update with new data, not discarding previous values.
        updated = update(filled, new)
        # Update with new data, discarding previous values.
        fresh = update(default, new)
        
        if keep_prev:
            return updated
        else:
            return fresh
    
    # Run once to set up the dictionary
    tool_data = tool_data_update()
    
    # Setup internal flag variables
    jogging = True

    print('\nserial_reader:\n    running_event is set! polling serial_interface continuously...')
    try:
        while running_event.is_set():
            
            # If we are probing, auto-update carefully.
            # Only send a status request if there are not pending messages to read.
            if probe_event.is_set() and not serial_interface.in_waiting:
                # Send "?" directly
                serial_interface.write('?'.encode())
            
            # Receive lines from serial_interface and put them in the output queue
            if serial_interface.in_waiting or probe_event.is_set():
                # Wait for grbl response with carriage return
                # Although a bit unnecessary: the while loop condition
                # ensures that there will always be a message for us at this point
                # if verbose: print('\nserial_reader:\n    new serial messages, reading from pyserial...')
                poll_line = serial_interface.readline().decode()  # Maybe use "read_until()", aunque quizas da igual.
                grbl_message = str(poll_line).strip()
                # if verbose: print('\nserial_reader:\n    processing received message: ' + grbl_message)

                # Check if the message is the output of GRBL configuration (i.e. the response to '$$')
                setting = re.search(r"^\$(\d+)=(.*)$", grbl_message, re.IGNORECASE)
                # Check if the message is a status report
                report = re.search(r"^<.+>$", grbl_message, re.IGNORECASE)

                # If a setting, print it and continue
                if setting is not None:
                    if verbose:
                        print('\nserial_reader:\n    setting line received: ' + grbl_message)
                    continue
                else:
                    if verbose:
                        print('\nserial_reader:\n    non-setting line received: ' + grbl_message)

                # Place the message in the output queue
                output_queue.put(grbl_message)  # Useless for now

                # If WPOS, send it to the websocket
                if grbl_message.upper().find('WPOS') > -1 and report:
                    # Ver: https://github.com/gnea/grbl/wiki/Grbl-v1.1-Interface#real-time-status-reports
                    wpos = re.search(r"WPos:(-?[\d\.]+),(-?[\d\.]+),(-?[\d\.]+)[\|>]",
                                     grbl_message, re.IGNORECASE)
                    if socket_use:
                        new_data = {'position': {
                            "x": float(wpos.group(1)),
                            "y": float(wpos.group(2)),
                            "z": float(wpos.group(3)),
                        }}
                        tool_data = tool_data_update(new_data, tool_data)
                        sio.emit('tool_data', tool_data)
                
                # Handle status reports with probe info
                if re.search(r'Pn:[XYZDHRS]*P[XYZDHRS]*[\|>]', grbl_message, re.IGNORECASE) and report:
                    if verbose:
                        print('\nserial_reader:\n    probe pin triggered in status report: ' + grbl_message)
                    if probe_event.is_set() and jogging:
                        # Terminate Jog
                        serial_interface.write(str.encode("!"))  # feed-hold
                        time.sleep(serial_response_delay)
                        serial_interface.write(str.encode("~"))  # resume
                        probe_event.clear()
                        
                        # We should not be jogging anymore
                        jogging = False  # CAUTION: this will likely have no effect, status reports are parsed below.

                # If Bf, register its values (requires $10=2)
                if grbl_message.upper().find('BF') > -1 and report:
                    buffer_status = re.search(r"Bf:(-?[\d\.]+),(-?[\d\.]+)[\|>]", grbl_message, re.IGNORECASE)
                    ok_planner_buffer_blocks = 15 == buffer_status.group(1)
                    ok_bytes_rx_buffer = 127 == buffer_status.group(2)
                
                # If ALARM
                if grbl_message.upper().find("ALARM") > -1:
                    # Extract alarm code and message
                    alarm_code = re.search(r"ALARM.(\d{1,2})", grbl_message, re.IGNORECASE)
                    if alarm_code is not None:
                        alarm_code = alarm_code.group(1)
                    else:
                        alarm_code = -1
                    print('\nserial_reader:\n    ALARM line received: ' + grbl_message +
                          " Description: " + get_alarm_by_code(alarm_code))

                    # Put the error in the queue
                    error_queue.put(get_error_by_code(alarm_code))
                    # Emit a socket alert
                    if socket_use:
                        sio.emit('alert', {'text': "ALARM message from GRBL: '" +
                                                   re.sub("[<>]", "", grbl_message) +
                                                   "'. and alarm code: " + str(alarm_code), 'type': 'alarm'})
                    # Sound the alarm
                    alarm_event.set()
                    
                    # Update status
                    if socket_use:
                        new_data = {'position': {"status": "ALARM: " + str(alarm_code)}}
                        tool_data = tool_data_update(new_data, tool_data)
                        sio.emit('tool_data', tool_data)

                # If ERROR
                elif grbl_message.upper().find("ERROR") > -1:
                    # Extract error code and message
                    error_code = re.search(r"ERROR.(\d{1,2})", grbl_message, re.IGNORECASE)
                    if error_code is not None:
                        error_code = error_code.group(1)
                    else:
                        error_code = -1
                    print('\nserial_reader:\n    ERROR line received: ' + grbl_message +
                          " Description: " + get_error_by_code(error_code))

                    # Put the error in the queue
                    error_queue.put(get_error_by_code(error_code))
                    
                    # Emit a socket alert
                    if socket_use:
                        sio.emit('alert', {'text': "ERROR message from GRBL: " + re.sub("[<>]", "", grbl_message),
                                           'type': 'error'})
                    # Sound the alarm
                    alarm_event.set()  # TODO: handle non-locking errors differently from alarms
                    
                    # Update status
                    if socket_use:
                        new_data = {'position': {"status": "ERROR: " + str(error_code)}}
                        tool_data = tool_data_update(new_data, tool_data)
                        sio.emit('tool_data', tool_data)

                # If OK
                elif grbl_message.lower().find("ok") > -1:
                    if input_queue.unfinished_tasks > 0:
                        ok_queue.put(grbl_message)  # Count "ok" responses
                        input_queue.task_done()     # Mark a GRBL command as "done"
                    else:
                        if verbose:
                            print('\nserial_reader:\n    WARNING - OK received, but no tasks were due... ignoring "OK". Was this a Jog?')
                        # In this program, Jog commands should be sent directly to the serial interface,
                        # but the serial_reader still receives an "ok" response for a JOG, so print a warning.
                        # a warning is produced if an "ok" is received without a corresponding task in the input_queue.
                        # If the Jog it were sent to the input_queue instead, the serial_reader will ignore it (it will never send it).
                    if verbose:
                        print('\nserial_reader:\n    OK received, remaining tasks: ' +
                              str(input_queue.unfinished_tasks))

                # If IDLE
                elif grbl_message.lower().find("idle") > -1 and report:
                    if verbose:
                        print('\nserial_reader:' +
                              '\n    Line received, status report "idle": ' + grbl_message +
                              "\n    Unfinished tasks: " + str(input_queue.unfinished_tasks)
                              )
                    alarm_event.clear()
                    
                    # Not jogging
                    jogging = False

                    # Solo setear "idle" cuando el buffer esta realmente en cero
                    # $10=2
                    #  Enabled Buf: field appears with "planner" and "serial RX" available buffer.
                    #  This shows the number of blocks or bytes **available** in the respective buffers.
                    #  Ejemplo: Bf:7,128
                    #  https://github.com/grbl/grbl/issues/932#issuecomment-198737235

                    # rx_planner = re.search(r"<.*Bf\:(\d+),(\d+).*>", grbl_message, re.IGNORECASE).group(2)
                    # buf_planner = re.search(r"<.*Bf\:(\d+),(\d+).*>", grbl_message, re.IGNORECASE).group(1)
                    # if verbose: print('\nserial_reader:\n' +
                    # '    Planner buffer: ' + buf_planner + " RX buffer: " + rx_planner)

                    # There are 15 planner blocks, and 128 buffer bytes,
                    # which must all be available in order to be _truly_ IDLE.
                    # if int(buf_planner) == 15 and int(rx_planner) == 128:
                    #    idle_event.set()
                    # else:
                    #    idle_event.clear()

                    idle_event.set()
                    
                    # Update status
                    if socket_use:
                        new_data = {'position': {"status": "IDLE"}}
                        tool_data = tool_data_update(new_data, tool_data)
                        sio.emit('tool_data', tool_data)

                # If HOMING
                elif grbl_message.lower().find("home") > -1 and report:
                    if verbose:
                        print('\nserial_reader:\n    Line received, status report "Home": ' + grbl_message)
                    idle_event.clear()  # Porque GRBL no está IDLE durante el homing.
                    alarm_event.clear()  # Porque home significa que no hay alarma.
                    
                    # Update status
                    if socket_use:
                        new_data = {'position': {"status": "HOME"}}
                        tool_data = tool_data_update(new_data, tool_data)
                        sio.emit('tool_data', tool_data)
                    
                    # Not jogging
                    jogging = False

                # If RUNNING
                elif grbl_message.lower().find("run") > -1 and report:
                    if verbose:
                        print('\nserial_reader:\n    Line received, status report "Run": ' + grbl_message)
                    idle_event.clear()  # Porque GRBL no está IDLE durante el Run.
                    alarm_event.clear()  # Porque home significa que no hay alarma.
                    
                    # Update status
                    if socket_use:
                        new_data = {'position': {"status": "RUN"}}
                        tool_data = tool_data_update(new_data, tool_data)
                        sio.emit('tool_data', tool_data)
                        
                    # Not jogging
                    jogging = False

                # If Jogging
                elif grbl_message.lower().find("jog") > -1 and report:
                    if verbose:
                        print('\nserial_reader:\n    Line received, status report "Jog": ' + grbl_message)
                    idle_event.clear()  # Porque GRBL no está IDLE durante el Jog.
                    alarm_event.clear()  # Porque home significa que no hay alarma.
                    
                    # We're jogging!
                    jogging = True
                    
                    # Update status
                    if socket_use:
                        new_data = {'position': {"status": "JOG"}}
                        tool_data = tool_data_update(new_data, tool_data)
                        sio.emit('tool_data', tool_data)
                
                elif grbl_message.upper().find('PRB') > -1:
                    # Ver: https://github.com/gnea/grbl/wiki/Grbl-v1.1-Interface#feedback-messages
                    if verbose:
                        print('\nserial_reader:\n    Probe line received: ' + grbl_message)
                    wpos = re.search(r"PRB:([\d\.]+),([\d\.]+),([\d\.]+):([\d\.]+)",
                                     grbl_message, re.IGNORECASE)
                    # TODO: send the PRB value somewhere useful.

                # Handle feedback MSGs
                # https://github.com/gnea/grbl/wiki/Grbl-v1.1-Interface#feedback-messages
                elif grbl_message.upper().find("MSG") > -1:
                    if verbose:
                        print('\nserial_reader:\n    Line received, MSG type: ' + grbl_message)
                    
                    # GRBL needs to be unlocked message
                    if grbl_message.upper().find("'$X' TO UNLOCK") > -1:
                        idle_event.clear(), alarm_event.set(), unlocked_event.clear(), reset_event.clear()
                        if socket_use:
                            sio.emit('alert', {'text': "serial_reader: GRBL needs to be unlocked: " + grbl_message,
                                               'type': 'alarm'})
                        # Put the error in the queue
                        error_queue.put(grbl_message)
                    
                    # GRBL needs to be reset message
                    if grbl_message.lower().find("reset to continue") > -1:
                        # Put the error in the queue
                        error_queue.put(grbl_message)
                        # This is the worst case scenario, GRBL will not even respond to '?' status requests.
                        idle_event.clear(), alarm_event.set(), unlocked_event.clear(), reset_event.set()
                        # Try to soft-reset GRBL with a "ctrl-x" (0x18) control charachter.
                        # serial_interface.write('0x18'.encode())
                        # Emit a socket alert
                        if socket_use:
                            sio.emit('alert', {'text': "serial_reader: GRBL must be reset to continue, ctrl-x has been sent. Message: " + grbl_message,
                                               'type': 'error'})
                    # GRBL has been unlocked message
                    if grbl_message.upper().find("CAUTION: UNLOCKED") > -1:
                        alarm_event.clear(), unlocked_event.set(), reset_event.clear()  # Should I add IDLE?
                        if socket_use:
                            sio.emit('alert', {'text': "serial_reader: GRBL was unlocked: " + grbl_message,
                                               'type': 'message'})
                    
                    # Update status
                    if socket_use:
                        new_data = {'position': {"status": "MSG: " + grbl_message}}
                        tool_data = tool_data_update(new_data, tool_data)
                        sio.emit('tool_data', tool_data)
                
                # If the message did not match any of the previous patterns, then just print it out.
                else:
                    if verbose:
                        print('\nserial_reader:\n    Line received, not classified: ' + grbl_message)

                # If a report was parsed, let the polling thread know that the info is up-to-date.
                if report is not None:
                    if verbose:
                        print('\nserial_reader:\n    Marking status tasks done: ' + grbl_message)
                    while request_queue.unfinished_tasks > 0:
                        # Clear all, in case timed-out requests accumulated:
                        request_queue.task_done()
            
            # Wait for a bit if no messages were waiting in the serial interface,
            # then check again!
            else:
                # Sleep a bit before checking for serial input again;
                # fastest looping is unnecessary.
                # This value mus be smaller than any other status request loop,
                # otherwise it will not be able to catch up, and the status reports
                # will contain outdated information.
                if not probe_event.is_set():
                    time.sleep(serial_response_delay/16)

    except Exception as e:
        print("'\nserial_reader:\n    exception in serial_reader!'" + str(e))
        error_queue.put(e)
        e_code = 1

    print('\nserial_reader:\n    Loop ended, returning.')
    return e_code


def serial_writer(serial_interface,
                  input_queue, ok_queue, 
                  request_queue, error_queue,
                  protocol_event,
                  idle_event, running_event, 
                  alarm_event, unlocked_event, 
                  reset_event, probe_event,
                  gcode_unlock,
                  serial_response_delay=0.1,
                  verbose=True,
                  RX_BUFFER_SIZE=128):
    # Wait for writing request by polling_event.wait()
    # Receive lines from serial_interface
    # Add the output to input_queue
    # Set OK and IDLE events
    # Catch alarm or error states

    exit_code = None

    def status(timeout=None, raise_error=True):
        """
        Report utility function.

        See: https://github.com/gnea/grbl/wiki/Grbl-v1.1-Interface#real-time-status-reports
        """
        # Get the one queue item, thereby blocking other requests, and/or wait until other requests are done.
        request_queue.get()
        serial_interface.write('?'.encode())  # Send "?"

        timed_out = False

        if timeout is None:
            # Wait for task_done in serial_reader to update.
            # Any report received by the serial_reader will unblock this.
            request_queue.join()
        else:
            endtime = time.time() + timeout
            # Wait for task_done in serial_reader to update.
            while request_queue.unfinished_tasks > 0:
                if endtime - time.time() <= 0.0:
                    if verbose:
                        print("\nserial_writer message:\n    ERROR: timed out waiting for status report.")
                    if raise_error:
                        raise GrblError("\nserial_writer message:\n    ERROR: timed out waiting for status report.")
                    else:
                        timed_out = True
                        break
                time.sleep(serial_response_delay/4)

        # Flag updated events
        if timed_out:
            idle, alarm = None, None
        else:
            idle = idle_event.is_set()
            alarm = alarm_event.is_set()

        # Put back the item, releasing status requests in other functions.
        request_queue.put("idle")

        return idle, alarm

    def wait_for_idle(timeout=None,
                      raise_error=False):
        """
        Report utility function
        """

        idle, alarm = status(timeout=serial_response_delay*2,
                             raise_error=False)

        t_counter = 0

        if timeout is None:
            if verbose:
                print("\nserial_writer message:\n    wait_for_idle: Waiting for idle (no time out).")
            while not idle:
                idle, alarm = status(timeout=serial_response_delay*2,
                                     raise_error=False)
                t_counter += serial_response_delay*2

                if verbose and t_counter > 5:
                    print("\nserial_writer message:\n    wait_for_idle: Waiting for idle (no time out).")
                    t_counter = 0

                if alarm:
                    print("\nserial_writer message:\n    wait_for_idle: Alarm fired while waiting for idle: " + str(timeout))
                    break
        else:
            if verbose:
                print("\nserial_writer message:\n    wait_for_idle: Waiting for idle with timeout: " + str(timeout))
            endtime = time.time() + timeout
            while not idle:
                idle, alarm = status(timeout=serial_response_delay,
                                     raise_error=False)
                t_counter += serial_response_delay*2

                if verbose and t_counter > 5:
                    print("\nserial_writer message:\n    wait_for_idle: Waiting for idle (no time out).")
                    t_counter = 0

                if endtime - time.time() <= 0.0:
                    print("\nserial_writer message:\n    wait_for_idle ERROR: timed out waiting for status report.")
                    if raise_error:
                        raise GrblError("\nserial_writer message:\n    wait_for_idle ERROR: timed out waiting for status report.")
                    else:
                        break

        return idle, alarm
    
    # Wait for the "run thread flag" before starting
    if verbose:
        print("\nserial_writer:\n    writer thread started waiting for running_event...")
    running_event.wait()

    if verbose:
        print("\nserial_writer:" +
              "\n    enabled running_event, waiting for commands from input_queue" +
              "\n    Unfinished tasks: " + str(input_queue.unfinished_tasks)
              )

    c_line = []  # Characters in lines sent to GRBL
    
    # Main loop #####
    elapsed = 0
    try:
        # Loop while running flag is set
        while running_event.is_set():
            
            # Don't hang if empty
            if input_queue.empty():
                # If more than 5 seconds have passed without an update, send a '?' status request.
                elapsed += serial_response_delay/4
                if elapsed > 5:
                    if verbose:
                        print("\nserial_writer:" + "\n    I'm bored... requesting status report.")
                    # Ask for a status report.
                    # Note: this bypasses the "request_queue" system.
                    serial_interface.write('?'.encode())
                    elapsed = 0
                
                # Sleep a bit before checking for input queue again
                time.sleep(serial_response_delay/4)
                continue
            else:
                # Get the next command (there is no wait since we just checked an item exists).
                command = input_queue.get()

            if verbose:
                print("\nserial_writer:" +
                      "\n    Got command from parser: " + command.strip() +
                      "\n    Unfinished tasks: " + str(input_queue.unfinished_tasks))

            # Strip comments/spaces/new line and capitalize. Note: los comentarios son cosas entre paréntesis.
            command = re.sub(r'\s|\(.*?\)', '', command).upper()
            # Track number of characters in grbl serial read buffer
            c_line.append(len(command) + 1)

            # While there are OK's in the queue, remove oldest "c_line" item
            # This makes the free byte count representative.
            if verbose:
                print("\nserial_writer:\n    Clearing ok queue")
            while not ok_queue.empty() and not (alarm_event.is_set() or reset_event.is_set()):
                # Get 'ok' queue items and mark them done
                ok_queue.get()
                ok_queue.task_done()
                # Delete the oldest line from the charcount list
                del c_line[0]

            # Hold here while the character buffer is getting full, and repeat the above when an OK arrives.
            # # The loop must end immediately if an ALARM is set, or if GRBL needs to be reset,
            # # because no further OK messages will be received.
            if verbose:
                print("\nserial_writer:\n    Checking buffer overflow...")
            while (sum(c_line) >= RX_BUFFER_SIZE - 1) and not (alarm_event.is_set() or reset_event.is_set()):
                if verbose:
                    print("\nserial_writer:\n    buffer overflow prevented. Waiting for 'ok' before sending...")
                # Wait for an 'ok' message from GRBL, but don't hang.
                if not ok_queue.empty():
                    ok_queue.get()
                    ok_queue.task_done()
                    # When an 'ok' is received, delete the oldest line from the charcount list
                    del c_line[0]
                    # Thus, the loop ends when the RX_BUFFER_SIZE will not be overflowed by sending another command
                    if verbose:
                        print("\nserial_writer:\n    Got 'ok', freeing some buffer and proceeding...")
                else:
                    time.sleep(serial_response_delay/4)

            # Ignore JOG commands.
            if command.startswith("$J"):
                # input_queue.task_done()  # Mark a Jog command as "done". CAUTION: Commented out because serial_reader already does this.
                continue  # Skip to the next command, doing nothing.
            # Jogs should be sent directly to the serial interface, 
            # because commands starting with "$" are handled specially below (as settings or homing),
            # and it didn't make sense to go through all the serial_writer stuff for a quick Jog.
            # Note: the serial_reader still receives an "ok" response for a JOG,
            # and will mark the input_queue task done anyway. But since JOGs
            # that are sent here are never sent, we must mark the task done now (CAUTION: why was this?).
            # Note: The serial_reader will produce a warning if an "ok" is received without
            # a corresponding task in the input_queue (i.e. when a Jog is sent through the serial interface).

            # Check if we have a "$" command (home, setting, unlock, jog, ...)
            if verbose:
                print("\nserial_writer:\n    Checking if it is setting command...")

            # GRBL "$" setting commands require an IDLE machine to work.
            # If it's not a setting command, skip this and just check if GRBL's status is bad (alarm, locked, needs reset).
            if command.startswith("$"):
                # If a "$" setting command was received we must wait for an all "ok" messages (task_done),
                # and also wait for GRBL idle state.
                # Note: This does not apply to "$X" when GRBL is in alarm state, in that case there is no wait.
                
                # The following ensures that no expected "ok" responses are missing at this point,
                # accounting for all GCODE sent previously.
                # Note: it would be equivalent to an "input_queue.join" which doesnt count the last ($...) command.
                if verbose:
                    print("\nserial_writer:" +
                          "\n    Setting line received. Waiting for " +
                          str(input_queue.unfinished_tasks) +
                          " unfinished tasks... " + command)
                
                # Wait for all remaining "ok" messages.
                #   If there is more than one task in the input_queue, wait for them to receive their OKs.
                #   There is usually (at least) one task, since this code is running because one was generated.
                #   So, we want only one pending task (the current one) before proceeding.
                #   Remember to exit if something went wrong.
                while input_queue.unfinished_tasks > 1 and not (alarm_event.is_set() or reset_event.is_set()):
                    if verbose:
                        print("\nserial_writer:\n    " +
                              "input_queue.unfinished_tasks (I) has " +
                              str(input_queue.unfinished_tasks) + " tasks.")

                    # Update status
                    idle, alarm = status()
                    time.sleep(serial_response_delay/4)

                # If the loop above ended because a "locked" or "alarm" event is on,
                # no pending "ok" messages will be received (I guess?),
                # So we must force "get" and "task_done" on the input_queue to clear it.
                if (not unlocked_event.is_set()) or alarm_event.is_set() or reset_event.is_set():
                    # On alarm, clear all tasks/commands.
                    while not input_queue.empty():
                        input_queue.get()
                    # And mark all tasks as done (even though they were never sent).
                    while input_queue.unfinished_tasks > 0:
                        input_queue.task_done()
                    # Then increment the unfinished tasks by 1,
                    # in order to expect an "ok" response in the serial_reader
                    # which will mark it "done".
                    input_queue.put(command)  # It is put...
                    input_queue.get()  # But never sent! and left "not done".
                # Check that only one task is left unmarked
                assert input_queue.unfinished_tasks == 1

                # We have waited for all "ok" messages we expected.
                # Now wait for IDLE, but _only_ if the "$" command is not "unlocking" (i.e. remove alarms or locking).
                # Else, wait for an "idle" state and warn with some messages.
                # TODO: rewrite to use G4 P0 dwell command.
                # Removed the $H command as "unlocking" because it got sent while the machine was not idle,
                # causing an error, and premature termination of the protocol.
                if command.startswith("$X"):  # or command.startswith("$H"):
                    print("\nserial_writer:\n    Unlocking command received, sending immediately: " + command)
                else:
                    print("\nserial_writer:\n    Non-unlocking setting GCODE received. Waiting for idle before sending: " + command)
                    wait_for_idle()  # Wait for idle or alarm.
                    if not unlocked_event.is_set() or alarm_event.is_set() or reset_event.is_set():
                        print("\nserial_writer:\n    GRBL is locked/alarmed/needs-reset. Your '$' command '" +
                              command + "' may be ignored by GRBL.")
                    # https://github.com/gnea/grbl/wiki/Grbl-v1.1-Interface#alarm-message

            # Alarm state check for non "$" setting commands.
            # In this case "wait_for_idle" would not make sense, since we'd like commands to stream ASAP.
            # So we just check if the alarm flags are ok, and produce a warning if not.
            # Exceptions should be raised elsewhere, if any.
            elif not unlocked_event.is_set() or alarm_event.is_set() or reset_event.is_set():
                print("\nserial_writer:\n    GRBL is locked/alarmed. Your command " +
                      command + " may be ignored by GRBL or produce error messages.")

            # Now send the command to GRBL's serial interface
            if verbose:
                print("\nserial_writer:\n    Sending command to serial interface: " + command)
            # OMG finally!
            serial_interface.write(str.encode(command + '\n'))

            # If it was a "$..." line, wait for the "ok" response.
            if command.startswith("$"):
                if verbose:
                    print("\nserial_writer:" +
                          "\n    Waiting for input queue join with unfinished_tasks: " +
                          str(input_queue.unfinished_tasks))

                # Wait for all "ok"s to be collected by the serial_reader.
                # Note: it should be just one "ok" for the current setting line, since it was ensured
                # previously that this is the only task in the input_queue.
                if command.startswith("$X"):
                    # Unlocking should have a shorter timeout, in case it doesn't work.
                    timeout = 1.0
                else:
                    # Other "$" less critical commands can have a longer timeout.
                    # TODO: what about $H commands?
                    timeout = 5.0
                # Timeout loop
                while (not input_queue.empty()) or (input_queue.unfinished_tasks > 0):
                    time.sleep(serial_response_delay/4)
                    timeout = timeout - serial_response_delay
                    if timeout < 0:
                        print("\nserial_writer:\n    WARNING timed out waiting for the input_queue to join!")
                        break
                
                # Note: however, another command _could_ be placed in the queue,
                # which would never get sent, and the "ok" would never be received,
                # thus it'd be stuck here forever (hence the timeouts).
                # That was also patched by using a "gcode_unlock" threading event, which
                # must be set to allow further gcode (see the queue_grbl_command function in this file).

                if verbose:
                    print("\nserial_writer:\n    Lifting gcode blockade...")

                # Finally release protocol progress locked by the "$" command.
                gcode_unlock.set()

            if verbose:
                print("\nserial_writer:\n    Done sending command: " + command +
                      ". Remaining buffer: " + str(RX_BUFFER_SIZE - 1 - sum(c_line)))

            # Wait for the next command
            command = None
            continue

    except GrblError as e:
        print("\nserial_writer:\n    GRBL error exception called during protocol streaming:\n    ", e)
        exit_code = 1

    except AssertionError as e:
        print(e)
        print("\nserial_writer:\n    assertion error in serial_writer!")
        error_queue.put(e)
        exit_code = 2

    except Exception as e:
        print("\nserial_writer:\n    Uncaught exception in serial_writer!: " + str(e))
        error_queue.put(e)
        exit_code = 3

    else:
        # Run if the try clause completes
        exit_code = 0

    finally:
        if verbose:
            print("\nserial_writer:\n    writer thread ending with exit code: " + str(exit_code))

    return exit_code
