# Examples: protocol2gcode 

Contents:

[[_TOC_]]

## Module usage examples

1. Open a CLI and `cd` to the `protocol2gcode` directory.
2. Open a `python3` console and run the examples in [EXAMPLES.md](./EXAMPLES.md).

> Note: if you edit the module's python code, remember to reload the systemd unit by running the `commander_restart` alias in the shell. Read more about that at [systemd.units/README.md](../systemd.units/README.md).

### Headless testing pre-requisites

To test the full module without the GUI, you must setup and populate the database with valid workspaces, protocols and platforms. 

Briefly:

1. Install and start `mongod`. Details at [gui/README.md](../gui/README.md).
2. Import the JSON files with' `mongoimport`, as shown in the  [defaults/README.md](../defaults/README.md).

Alternatively, the `Commander` class may be instantiated independently, and used to run instructions from a pre-made GCODE file or interactively.

### Example: List protocols

The `MongoObjects` class is your friend. It is defined in [`commander_utils.py`](commander_utils.py).

```python
from commander_utils import MongoObjects
from pprint import pprint

# Setup database
mo = MongoObjects(mongo_url='mongodb://localhost:27017/')

# List protocols (will print possible protocol names)
protocols = mo.listProtocols()

# Pretty-print the first one
pprint(protocols[0])
```

### Example: List platforms in a workspace

```python
# List workspaces (will print possible protocol names)
workspaces = mo.listWorkspaces()

# Pretty-print the first one
pprint(workspaces[0])

# Get the first one, and print its name:
workspace = workspaces[0]
print(workspace["name"])

# Show names of platforms (items) in the workspace
items = workspace["items"]
[p["name"] for p in items]
```

### Example: List platforms from protocol

```python
from commander_utils import MongoObjects

# Get the last protocol
protocol = protocols[-1]
protocol_name = protocol["name"]

# Get everything about this protocol by its name
protocol, workspace, platforms_in_workspace = database_tools.getProtocolObjects(protocol_name=protocol_name)

# Show names of platforms in the protocol's workspace
[p["name"] for p in platforms_in_workspace]
```

### Example: parse GUI protocol to GCODE

You'll need a protocol name, which can be acquired from the previous example.

```python
from gcodeBuilder import GcodeBuilder
from commander_utils import MongoObjects
import json
import pprint

database_tools = MongoObjects(mongo_url='mongodb://localhost:27017/',
                              parser_object=None,  # argparse no longer used
                              verbose=True,
                              pipette=None)

# Load protocol objects from mongodb:
protocol_name = "multitool_protocol_test_2 2022-09-10T08:12:44.726Z"  # You can get this name from the example above.
protocol, workspace, platforms_in_workspace = database_tools.getProtocolObjects(protocol_name=protocol_name)

# Initiate the task class, which will hold all relevant information for the current task
builder = GcodeBuilder(protocol, workspace, platforms_in_workspace, setup=False)

# Generate GCODE for the task, it is saved in the task class object
builder.parseProtocol()

# Get the command list, and print it
pprint.pprint(builder.commands)

```

### Example: Load the commander

This class is defined in the [`gcodeCommander.py`](gcodeCommander.py) file. The operation of the machine depends on it (unless GCODE is sent directly through the serial interface for some hopefully good reason).
 
The commands from the GUI are "routed" to this class by the `run_commander` (defined in [`commander_utils.py`](commander_utils.py)).
 
At this point there is not much to do with just an instantiated `Commander` class, other than checking out its stuff. For example browsing pipette configuration. If you want to use it for something, have a look at the "use the commander class directly" example below.
 
> Note that a commander is setup by the systemd unit continuously. Opening a serial port multiple times simultaneously might cause problems. You can prevent this temporarily by running `systemctl --user stop commander.service` in the Pi's shell.

```python
# Use python3
import commander as cm

# Customize startup arguments
# A description of all arguments is available in the functions help, with: "help(cm.default_args)"
args_dict = cm.default_args(port="/dev/ttyACM0") # Try /dev/ttyACM0, /dev/ttyUSB0, etc.

# Initialize the commander class using the arguments in the dictionary
commander = cm.prepare_commander(args_dict)

# Cleanup and close
commander.cleanup()
```

### Example: Dry protocol run

Here we use the `run_commander` function to do everything about running a protocol, except actually sending the commands. This is known as a "dry" run.

> The `run_commander` function decides how to use the `Commander` class, based on the arguments it received. Those arguments are usually provided by the GUI.

A dry run is useful for debugging!

* Setting the `dry` flag skips all protocol commands (GCODE and others).
* Setting the `really_dry` flag skips running the protocol entirely, only the protocol parsing steps are run.

```python
# Use python3
import commander as cm

# Obtain a protocol name as shown in the previous section
# A description of all arguments is available in the functions help, with: "help(cm.default_args)"
args_dict = cm.default_args(port="/dev/ttyUSB0", # or try /dev/ttyACM0, /dev/ttyUSB1, etc.
                            run_protocol="prueba_50ul 2022-09-03T16:53:38.460Z", 
                            really_dry=True,  # This flag skipps the run_protocol call.
                            dry = True)

# Convert the arguments dictionary to a tuple (sorry about this!)
args_tuple = cm.parse_args(args_dict)

# Dry-run the protocol
cm.run_commander(sio=None, args=args_tuple)
```

### Example: Send commands interactively

Also useful for debugging!

> Note the `interactive=True` argument passed to `default_args`.

**Custom command**s are prefixed by keywords:

* Sending `Peject;` will run the tip ejection routine.
* `HUMAN;` will wait for a human intervention.
* `Wait;10` will wait for `10` seconds.
* `Pmove;1` will move the pipette axis by 1 unit.
* `Phome;` will home the pipette axis.

All commands not prefixed by those keywords will be interpreted as GCODE and passed to GRBL:

* If you need to write GCODE manually for testing, look up some examples on the internet.
    * Beware: this machine should be configured in positive space. Start with small 1mm movements to check your directions to prevent crashing.
* GRBL follows LinuxCNC GCODE _closely_, see:
    * https://github.com/gnea/grbl/blob/master/doc/markdown/commands.md
    * http://www.linuxcnc.org/docs/2.4/html/gcode_overview.html#sec:Modal-Groups
* Jogging commands start with `$J=`

You can send commands interactively through this crude tool:

> Note that a valid protocol name is required to access the interactive tool.

```python
# Use python3

# Import the main "commander module"
import commander as cm

# Obtain a protocol name as shown in the previous section
# A description of all arguments is available in the functions help, with: "help(cm.default_args)"
args_dict = cm.default_args(port="/dev/ttyUSB0", 
                            run_protocol="prueba_50ul 2022-09-03T16:53:38.460Z", 
                            verbose=False,
                            interactive=True)

# Convert the arguments dictionary to a tuple (sorry about this!)
args_tuple = cm.parse_args(args_dict)

# Run the protocol interactvely
cm.run_commander(sio=None, args=args_tuple) # Hope for happy interactions! :)
```

### Example: Run protocol interactively

Useful for debugging.

> Note the `interactive=True` argument passed to `default_args`.

```python
# Use python3
import commander as cm

# Obtain a protocol name as shown in the previous section
# A description of all arguments is available in the functions help, with: "help(cm.default_args)"
# protocol_name = "multitool_protocol_test_2 2022-09-10T06:47:11.241Z"
protocol_name = "multitool_protocol_test_2 2022-09-10T08:12:44.726Z"
args_dict = cm.default_args(port="/dev/ttyUSB0", 
                            run_protocol=protocol_name, 
                            verbose=False,
                            interactive=True)

args_tuple = cm.parse_args(args_dict)

cm.run_commander(sio=None, args=args_tuple)
```

### Example: Run full protocol

Note: the workspace must be perfectly aligned and prepared before running a protocol. For now the only way to easily define a workspace and protocol is through the GUI.

```python
# Use python3
import commander as cm

# Obtain a protocol name as shown in the previous section
# A description of all arguments is available in the functions help, with: "help(cm.default_args)"
args_dict = cm.default_args(port="/dev/ttyACM0", 
                            run_protocol="Protocolo de prueba! 2022-05-08T21:15:03.598Z")

args_tuple = cm.parse_args(args_dict)

cm.run_commander(sio=None, args=args_tuple)
```

### Example: Control the pipettes

Use the multi-pipette Python class to test out correct wiring during assembly.

Be careful with maximum accelerations and maximum speed; its easy to start missing stepps.

* Maximum "speed" can be about `2`.
* Recommended acceleration interval: 20 or 50 milliseconds (i.e. `20/1000`).
* Also be _very_ careful not to exceed the maximum displacements.

```python
import pipetteDriver as pd

# Tune a few things, for this testing example.
pipette = pd.Pipette2(verbose=True)

# Select a p20 pipette from the builtin pipette models.
pipette.configure_pipette("p20")

# Displace the pipette's shaft by "-2".
pipette.displace(5, max_speed=5)

# Home the pipette tool
pipette.home_pipette()

# Select a p200 pipette from the builtin pipette models.
pipette.configure_pipette("p200")

# Displace the pipette's shaft by "-2".
pipette.displace(-1, max_speed=1)

# Home the axis
pipette.home_pipette(max_speed=2, retraction_displacement=1)

# Cleanup please
pipette.close()
```

> Warning: the limit switch pins are shared between pipettes (though with care, because some endstops [can cause short-circuits](https://softsolder.com/2017/11/30/mpcnc-makerbot-style-endstop-switch-modification/) in parallel configuration). See the electronics [README.md](../doc/electronica/README.md) for details.


Steppers can be enabled and disabled:

```python
# Disable (this method is called by the "close" method).
pipette.disable_stepper()

# Enable (this is the default state).
pipette.enable_stepper()
```

### Example: build a tool-change macro

Park a tool, considering a tool-head with a p200.

```python
import gcodePrimitives
from pprint import pprint
import pipetteDriver as pd

# Get the available pipettes
pipette = pd.Pipette2(dry=True)

# Gett tool post info for the p200
current_tool = pipette.pipettes["p200"]
tool_post_coords = current_tool["tool_post"]

# Placeholder limits
safe_z = 10
safe_y = 10

# Make macro
park_commands = gcodePrimitives.toolchange_probe(**tool_post_coords,
                                                 safe_z=safe_z, safe_y=safe_y,
                                                 extra_scan=1, closeup_dist=10,
                                                 mode_fwd="G38.2", mode_rev="G38.4", feedrate=200)
# Pretty print
pprint(tool_post_coords)
pprint(park_commands)
```

To stream this GCODE to GRBL, see "Streaming GCODE" at [grbl/README.md](../grbl/README.md).

Briefly:

```python
# Write gcode to a file
with open('/tmp/gcode.txt', 'w') as f:
    for line in park_commands:
        f.write(f"{line}\n")

```

```bash
# Go back to the bash terminal and run
cat /tmp/gcode.txt
python3 ../grbl/scripts/simple_stream.py /dev/ttyACM0 /tmp/gcode.txt ask
```

### Example: use the commander class directly

Run `systemctl --user stop commander.service` in the shell before doing this, or the service might interfere.

Probe for a tool

```python
from gcodeCommander import Commander

commander = Commander(serial_device_path='/dev/ttyUSB0')

# Start the reader and writer threads
# Notice that serial_writer gets bored, and periodically asks for a status report on its own.
commander.start_threads()

# Request a status report from GRBL yourself.
commander.status()

# Remember to cleanup
commander.cleanup()
```

```python
from gcodeCommander import Commander

commander = Commander(serial_device_path='/dev/ttyUSB0', interactive=True)

# Start the reader and writer threads
# Notice that serial_writer gets bored, and periodically asks for a status report on its own.
commander.start_threads()

# Request a status report from GRBL yourself.
commander.status()

import commander as cm

# Obtain a protocol name as shown in the previous section
# A description of all arguments is available in the functions help, with: "help(cm.default_args)"
args_dict = cm.default_args(port="/dev/ttyUSB0", 
                            run_protocol="multitool_protocol_test_2 2022-09-04T20:16:09.498Z")

args_tuple = cm.parse_args(args_dict)

cm.run_commander(sio=None, args=args_tuple, commander=commander)
```


### Example: test the tip probes

Run `systemctl --user stop commander.service` in the shell before doing this, or the service might interfere.

- [ ] TO-DO: the tip probe needs a capacitor as well.

```python
from gcodeCommander import Commander

commander = Commander(serial_device_path='/dev/ttyUSB0', verbose=True)

# Start the reader and writer threads
# Notice that serial_writer gets bored, and periodically asks for a status report on its own.
commander.start_threads()

# Request a status report from GRBL yourself.
commander.status()

# Configure the p200 micropipette
commander.pipette.configure_pipette("p20")
commander.pipette.configure_pipette("p200")

# Probe pin status
from RPi import GPIO
GPIO.input(commander.pipette.probe_pin) == GPIO.LOW

# Test the probing wait function: should release on 
result = commander.pipette.probe_tip_wait(timeout=15)
# Now, using your hands, raise the probe manually (of the selected pipette).
# The prompt should return to you!
print(result)

# Now try interrupting a Z jog command by manually raising the probe.
commander.tip_probe("PRB;$J=G91 Z-5 F100")
# Or try in the other direction
commander.tip_probe("PRB;$J=G91 Z5 F100")
```

## Exprimental
 
### Load tool info from workspace

Setup database:

```python
from commander_utils import MongoObjects

# Setup database
mo = MongoObjects(mongo_url='mongodb://localhost:27017/')
```

List platforms from a workspace:

```python
# List workspaces (will print possible protocol names)
workspaces = mo.listWorkspaces()

# List platforms (will print possible protocol names)
platforms = mo.listPlatforms()

# Get the last one, and print its name:
workspace = workspaces[-1]
print(workspace["name"])

# Show names of platforms (items) in the workspace
items = workspace["items"]
[p["name"] for p in items]
```

Get pipette models from `pipetteDriver.py`:

```python
from pprint import pprint
import pipetteDriver as pd

# Pipette models are p20 and p200 (2022/11).
# These names are in pipetteDriver.py, and 
# have been used to define tool-data in a
# platform.

# Get the available pipettes
pipette = pd.Pipette2(dry=True)
pipette_models = list(pipette.pipettes)
print(pipette_models)
```

Find which platforms have IDs starting with the pipette model name:

```python
# Get platform IDs (names of platform definition, not of the platform in a workspace).
# This is needed because although platforms are defined with ID "p20",
# they have names with an added index posfix: "p20 1".
# The "platform" key contains the actual platform ID, and will be here if they match the "pipette_models" exactly.
pipette_platform_ids = [ item["platform"] for item in items if item["platform"] in pipette_models ]
print(pipette_platform_ids)
```

Print tool data from the platform definition:

```python
# Print tool data. For now it is held in a key named exactly as the platform.
pipette_tools = {}
for pipette_platform_id in pipette_platform_ids:
    print(pipette_platform_id)
    for platform in platforms:
        if platform["name"] == pipette_platform_id:
            pipette_tools[pipette_platform_id] = platform[pipette_platform_id]

# Pretty print
for pipette_name in pipette_platform_ids:
    pprint(pipette_tools[pipette_name])

```

Test equality:

```python
# Print tool data. For now it is held in a key named exactly as the platform.
pipette_tools["p200"] == pipette.pipettes["p200"]

# Python sucks, it cant even compare objects in a generic, straightforward way...
def eq(a, b):
    if type(a) == list and type(b) == list:
        return a.sort() == b.sort()
    elif type(a) == list or type(b) == list:
        return False
    else:
        return a == b

# List differences
for p in pipette_platform_ids:
    for k in list(pipette_tools[p]):
        if not eq(pipette.pipettes[p][k], pipette_tools[p][k]):
            print("Difference at:", p, k, "->", pipette.pipettes[p][k], "VS", pipette_tools[p][k])

```

### Test a tool-change macro

Load a tool, considering an empty tool-head.

```python
from commander_utils import MongoObjects
from gcodeBuilder import GcodeBuilder
import pipetteDriver as pd

# Get the available pipettes
pipette = pd.Pipette2(dry=True)
pipettes = pipette.pipettes
# Print available models
print(list(pipettes))

# Setup database tools
mo = MongoObjects(mongo_url='mongodb://localhost:27017/')

# List protocols (will print possible protocol names)
protocols = mo.listProtocols()

# Get the last protocol
protocol = protocols[-1]
protocol_name = protocol["name"]

# Get everything about this protocol by its name
protocol, workspace, platforms_in_workspace = mo.getProtocolObjects(protocol_name=protocol_name)

# Instatiate the builder class, specifying tool parameters (pipettes).
builder = GcodeBuilder(protocol, workspace, platforms_in_workspace, pipettes = pipette.pipettes)

# Build the macro, it will be saved to the commands.
new_tool = "p200"  # You can get this name from "list(pipettes)[0]"
macro = builder.toolChangeMacro(new_tool=new_tool, current_tool=None, return_commands=True)

# Show the commands
_ = [print(m) for m in macro]
```
