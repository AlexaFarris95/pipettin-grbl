# Some actions might depend on previous actions or action history.
# So these objects are for tracking the state of the machine during the translation of actions to GCODE,
# or during gcode streaming to GRBL.

"""
Changes:

* Minimal backslash compensation: from 0.5 to 0.1
* Added a pour move after a new tip is filled.
"""

# import driverGcodeSender as Gcode
import pprint
from math import pi
import gcodePrimitives
from math import copysign
import sys
import traceback
from copy import copy, deepcopy
from pipetteDriver import Pipette2

# Sign function from https://stackoverflow.com/a/1986776
sign = lambda x: copysign(1, x)


class GcodeBuilder(object):
    """A class holding machine current status or configuration, a class for machine state tracking."""
    """Also holds all GCODE commands after parsing protocols."""

    def __init__(self, _protocol, _workspace, platformsInWorkspace,
                 pipettes=None, setup=True, s_retract=0, z_probe_height=None,
                 config_dict=None, verbose=True):
        """
        A class to contain all information for the pipetting task: protocol, workspace, platforms and tools.
        :param _protocol: A protocol JSON object from MONGODB.
        :param _workspace: A workspace JSON object from MONGODB.
        :param platformsInWorkspace: A platforms JSON object from MONGODB, containing platforms in the workspace.
        :param pipette: string identifying the micropipette choice.
        :param setup: if True, GCODE to home the machine is generated on class initialization by macroSetupMachine.
        :param s_retract: after homing the S axis, lower it by s_retract millimeters, pressing the pipette shaft up to
         it's first stop (or just over it, ideally). This position will be set as a zero volume reference point.
        :param z_probe_height: None unless a probe is at the XY homing origin. In that case, set this to its height.
        :param config_dict: TODO: use this option to load default options from a JSON configuration file.
        """
        print("Hi! initializing the pipettin' task construct.")

        self.verbose = verbose
        self.protocol = _protocol
        self.workspace = _workspace
        self.commands = []
        self.platformsInWorkspace = platformsInWorkspace

        # Clearance and probing parameters
        self.clearance = self.getClearance()  # requires self.platformsInWorkspace
        # TODO: base these values on a config file
        self.tip_probing_distance = 5         # Distance above item's height, from which probing will begin.
        self.probe_extra_dist = 1             # Just extra (downwards) probing distance. Harmless if the probe works.
        self.extra_clearance = 1 + self.tip_probing_distance

        # Maximum travel distances for each GRBL axis
        # TODO: base these values on the output of GRBL's "$$" command
        self.limits = {"z_max": 258,  # Z is homing at the top, with positive direction upwards.
                       "x_max": 350,  # X and Y are homing at 0 and move in positive space.
                       "y_max": 320,
                       "z_min": 0,
                       "x_min": 0,
                       "y_min": 0
                       }
        self.feedRateDefault = 1000  # For XYZ moves
        # TODO: base these values on a config file
        # self.g92_coords = dict(x=0, y=0, z=125)  # DEPRECATED: now using hadrcoded G54 coordinate system.
        self.probe_z = z_probe_height

        # Prefixes for pseudo-gocode (commands not for GRBL):
        self.human_prefix = "HUMAN;"
        self.wait_prefix = "Wait;"
        self.pipetting_command_prefix = "Pmove;"
        self.pipetting_homing_prefix = "Phome;"

        # Distance to travel downwards afeter homing the pipette,
        # This move is to push its shaft downwards and leave the pipette ready to suck volume
        self.pipetting_homing_retract = s_retract
        # Set reference position at home (just after homing).
        # "0" corresponds to first pipette stop, contracted position ready to load maximum volume.
        # self.s_value_at_home = s_retract  # A kind of "G92 Z0" thing. Not used for now.

        # State tracking variables
        self.homed = False
        self.tip_loaded = False
        self.state = {"s": 0, "vol": 0,  # pipette axis position in mm (s) and microliters (vol).
                      "tip_vol": 0,
                      "lastdir": None,
                      "x": None,
                      "y": None,
                      "z": None,
                      "platform": None,
                      "tube": None,
                      "paused": False,
                      "alarm": False
                      }

        # Available micropipette models and characteristics
        # TODO: base these values on a config file
        if pipettes is None:
            pipette_driver = Pipette2(dry=True)
            self.pipettes = pipette_driver.pipettes
        else:
            self.pipettes = pipettes

        # Print pipettes
        if self.verbose:
            print("\nGcodeBuilder: Available pipette models on startup:\n")
            pprint.pprint(self.pipettes)

        # Initialize empty state on each pipette
        for p in list(self.pipettes):
            self.pipettes[p]["state"] = deepcopy(self.state)

        # Setup tool tracking
        self.new_tool = None
        self.current_tool = None
        self.toolchange = False

        # Set chosen micropipette model
        # self.pipette = self.pipettes[pipette]
        # self.back_pour_correction = self.pipette["back_pour_correction"]

        self.lastdir = None  # To track the direction of the last pipette move (for compensation)

        # Run setup macro at startup if requested
        if setup:
            print("TaskEnvironment class init message: setting up machine...")
            ret_msg = self.macroSetupMachine()
            print("    ", ret_msg)

    # Example:
    # task = TaskEnvironment(protocol, workspace, platformsInWorkspace, clearance)
    # task._clearance = 20
    # print(g)

    # Sign function from https://stackoverflow.com/a/1986776
    # sign = lambda x: copysign(1, x)

    def getClearance(self):
        """Get the minimum clearance level for the Z axis from the platformsInWorkspace definition"""
        return max([float(platform["defaultTopPosition"]) for platform in self.platformsInWorkspace])

    def getGcode(self):
        return "\n".join(self.commands)

    @staticmethod
    def actionComment(_action, _i, *args):
        # print(_action)
        return "Processed actionComment with index " + str(_i) + " and text: '" + _action["args"]["text"] + "'"

    def actionWait(self, _action, _i, *args):
        self.commands.append(self.wait_prefix + str(_action["args"]["seconds"]))
        return self.human_prefix + " in action " + str(_i) + " with time " + _action["args"]["seconds"]

    def actionHuman(self, _action, _i, *args):
        # TO-DO: human intervention action ID
        # TO-DO: human intervention timeout behaviour
        self.commands.append(self.human_prefix + _action["args"]["text"])
        return self.human_prefix + " in action " + str(_i) + " with text " + _action["args"]["text"]

    def actionHOME(self, _action, _i, *args, do_final_g92=False):
        """
        TODO: Produce $H only if the machine has not been homed, and set the machine as homed if so.
        """
        # if self.homed:
        #     # TODO: make it so that if the machine is homed,
        #     #  another home command will move it to the origin.
        #     home_sweet_home = "; Already homed, skipping command."
        # else:
        #     home_sweet_home = gcodePrimitives.gcodeHomeXYZ()
        #     self.homed = True
        # print(_action)
        if "args" in _action.keys():
            if "which" in _action["args"].keys():
                home_sweet_home = gcodePrimitives.gcodeHomeXYZ(which_axis=_action["args"]["which"])
                self.commands.append(home_sweet_home)

                if _action["args"]["which"].upper().find("S") or _action["args"]["which"].upper() == "ALL":
                    self.macroPipetteHome()
        else:
            print("  HOME action supplied without action.args.which; defaulting to 'all'")
            home_sweet_home = gcodePrimitives.gcodeHomeXYZ()
            self.commands.append(home_sweet_home)
            self.macroPipetteHome()

        # if do_final_g92:  # DEPRECATED: using G54 coordinate system now.
        #     # self.commands.append("G92 X0 Y0 Z125")
        #     self.commands.append(
        #         gcodePrimitives.gcodeG92(**self.g92_coords)
        #     )

        return "Processed actionHOME with index " + str(_i)

    def actionDISCARD_TIP(self, _action, _i, *args):
        """Move over the trash box and discard the tip (if placed, else Except)."""
        # _action = self.protocol["actions"][4]
        # move to clearance level on Z
        # move over discarding bucket
        # eject tip
        # move to clearance level on Z

        if self.tip_loaded:
            self.macroMoveSafeHeight()  # move to clearance level on Z, should extend task.commmand list automatically

            # # move to next tip location on XY
            # # _i = 1
            # # _action = protocol["actions"][_i]  # for testing
            # platform_name = _action["args"]["item"]
            # platform_item = self.getWorkspaceItemByName(platform_name)
            # platform_ = self.getPlatformByName(platform_item)
            # 
            # _x = platform_item["position"]["x"]
            # _x += platform_['width']/2
            # 
            # _y = platform_item["position"]["y"]
            # _y += platform_['length']/2
            # 
            # # TODO: check and tune Z positioning extra clearance
            # _z = platform_["defaultTopPosition"] + self.extra_clearance  # BUG: this was too low, does not consider tip length
            # _z += self.tip_loaded["tipLength"]
            # 
            # self.commands.extend([
            #     gcodePrimitives.gcodeMove(x=_x, y=_y),  # Move over the trash, at Z = safe height set earlier
            #     gcodePrimitives.gcodeMove(z=_z)         # Move over the trash, at defaultTopPosition
            # ])

            # Generate tip ejection post macro
            safe_z = self.getSafeHeight()
            safe_y = self.getSafeY()
            eject_coords = self.pipette["eject_post"]
            eject_commands = gcodePrimitives.gcodeEjectTip(**eject_coords,
                                                           safe_z=safe_z, safe_y=safe_y)
            # Add them to the list
            self.commands.extend(eject_commands)

            # TODO: wtf will be the drop tip GCODE
            self.tip_loaded = False
            self.macroMoveSafeHeight()  # Raise to safety. Should do an automatic append

            # Ensure zeroed pipette axis
            self.macroPipetteZero()

            # Register no tip in the volume tracker
            # self.state["tip_vol"] = None
            self.pipettes[self.current_tool]["state"]["tip_vol"] = None

        else:
            # TODO: discutir si es mejor tirar este error o no hacer nada
            raise Exception("PROTOCOL ERROR: Cannot discard tip if one is not already placed! Action index: " + str(_i))

        return "Processed actionDISCARD_TIP with index " + str(_i)

    def getSafeHeight(self):

        # Calculate Z coordinate
        _z = self.clearance
        _z += self.extra_clearance

        # # Add tool offset if any
        # if self.current_tool is not None:
        #     _z += self.pipettes[self.current_tool]["tool_offset"]["z"]
        # elif self.verbose:
        #     print("\ngcodeBuilder message:    \n" + "no Y offset added, self.current_tool was None.")

        # Add tip offset if any
        if self.tip_loaded:
            _z += self.tip_loaded["tipLength"]
            # Add tool offset if any
            if self.current_tool is not None:
                _z += self.pipettes[self.current_tool]["tool_offset"]["z_tip"]
        # If no tips are loaded
        else:
            # But a tool is loaded
            if self.current_tool is not None:
                # Then add the "bare" tool offset
                _z += self.pipettes[self.current_tool]["tool_offset"]["z_bare"]

        return _z

    def getSafeY(self):
        """
        There is a limit on the Y axis movement. The tool-holder can crash into the parked tools.
        Here we use the parking post coordinates to establish a safe limit for the Y direction.
        TODO: we will eventually need safe "area" checks, for coordinates _and_ for paths.
        @return:
        """

        # Calculate Y coordinate
        safe_y = self.limits["y_max"]
        for p in list(self.pipettes):
            safe_y = min(safe_y, self.pipettes[p]["tool_post"]["y"])

        # Add tool length offset if the carriage is loaded with a tool.
        if self.current_tool is not None:
            y_clear = self.pipettes[self.current_tool]["tool_post"]["y_clearance"]
            safe_y -= y_clear
            if self.verbose:
                print("\ngcodeBuilder message:    \n" + f"{self.current_tool} y_clearance offset added: {y_clear}")
        elif self.verbose:
            print("\ngcodeBuilder message:    \n" + "no Y offset added, self.current_tool was None.")

        return safe_y

    def macroMoveSafeHeight(self, _mode="G90 G0"):
        """G0 to safe height in Z axis, considering loaded tipLength"""
        _z = self.getSafeHeight()
        commands = gcodePrimitives.gcodeMove(z=_z, _mode=_mode)
        self.commands.append(commands)
        return commands

    def macroPipette(self, _action, _i):
        """
        Pipetting works incrementally for now (i.e. relative volumes, not absolute volumes).
        Negative values mean 'dispense' or 'lower' the axis (see functions in driverSaxis.py).
        """
        volume = _action["args"]["volume"]

        # Backslash compensation
        # If previous and current directions are different
        # if sign(self.lastdir) * sign(volume) == -1:
        if sign(self.pipettes[self.current_tool]["state"]["lastdir"]) * sign(volume) == -1:
            # Then over-pour (negative volume, -0.5 uL for p200) or over-draw (positive volume, 0.5 uL for p200)
            backslash_correction = sign(volume) * self.pipette["backslash_compensation_volume"]
            volume += backslash_correction
            # Comment GCODE
            if self.verbose:
                self.commands.append(f"; Action {_i}: drawing extra {backslash_correction}" +
                                     " uL on backslash correction.")
        # TODO: be smarter about backslash

        # Compensation for under-drawing on first load:
        # Check if the tip is new/empty.
        # first_draw = (self.state["tip_vol"] <= 0) & (sign(volume) == 1)
        first_draw = (self.pipettes[self.current_tool]["state"]["tip_vol"] <= 0) & (sign(volume) == 1)
        # If so, we are drawing volume for the first time with the tip.
        if first_draw:
            # In which case we must over-draw by a specified amount (e.g. 5 uL for the p200)
            volume += self.pipette["extra_draw_volume"]
            # Comment GCODE
            if self.verbose:
                self.commands.append(f"; Action {_i}: drawing extra {self.pipette['extra_draw_volume']}" +
                                     " uL on first draw.")

        # Reciprocal compensation for capillary effects
        # Final volume calculation
        # final_volume = self.state["vol"]
        final_volume = self.pipettes[self.current_tool]["state"]["vol"]
        final_volume += volume
        if first_draw:
            # Back pour correction, negative, force dispensing
            final_volume += -abs(self.pipette["back_pour_correction"])

        # Define reciprocal function
        def capillary_corr(final_vol, start_vol=20, max_cor=2):
            """Linear function: -2 at final volume 0; 0 at final volume 20 """
            return -abs(-final_vol*max_cor/start_vol + max_cor)    # Must be negative to dispense volume
            # return -abs((-1/(21-x) + 0.04761905) * (2/0.952381))  # reciprocal attempt, KISS!
        # Apply correction if necesary: only for dispensing at low final volumes
        if sign(volume) == -1 and final_volume < 20:
            capillary_extra_vol = capillary_corr(final_volume)
            capillary_extra_vol = max([-2, capillary_extra_vol])  # Cap maximum correction on 2 uL
            if self.verbose:
                self.commands.append(f"; Action {_i}: final volume {final_volume} uL, " +
                                     f"dispensing extra {capillary_extra_vol} uL.")
            volume += capillary_extra_vol

        # Default pipetting mode should be incremental/relative volume pipetting
        if "mode" not in _action["args"].keys():
            _action["args"]["mode"] = "incremental"

        # Convert volume to shaft displacement in millimeters
        if _action["args"]["mode"] == "incremental":
            s_axis_movement = volume / self.pipette["scaler"]
        # TODO: be smart about current and next volume
        # TODO: implement "absolute" pipetting volumes
        # elif _action["args"]["mode"] == "absolute":
        #     s_axis_movement = volume / self.pipette["scaler"]
        else:
            raise Exception(f"PROTOCOL ERROR: invalid macroPipette mode at action with index {_i}")

        if self.verbose:
            print("\ngcodeBuilder message:    \n" +
                  f"Volume {volume} converted to {s_axis_movement} mm displacement" +
                  f" with scaler: {self.pipette['scaler']}")
            # Comment GCODE
            self.commands.append(f"; Action {_i}: {volume} uL conversion to {s_axis_movement} mm with scaler: {self.pipette['scaler']}")

        # Compose and append the pseudo-GCODE command (with volume units in microliters).
        # command = self.pipetting_command_prefix + str(s_axis_movement)  # TODO: add pipetting speed control or something
        command = self.pipetting_command_prefix + str(volume)  # TODO: add pipetting speed control or something
        self.commands.append(command)

        # Update pipette state
        # self.state["s"] += s_axis_movement
        # self.state["vol"] += volume
        # self.state["tip_vol"] += volume
        self.pipettes[self.current_tool]["state"]["s"] += s_axis_movement
        self.pipettes[self.current_tool]["state"]["vol"] += volume
        self.pipettes[self.current_tool]["state"]["tip_vol"] += volume
        # Update direction with the current move
        self.pipettes[self.current_tool]["state"]["lastdir"] = sign(volume)

        # Back-pour Compensation
        # If it is a first draw, try correcting potential backslash by pouring a bit back into the tube.
        # Do this by calling this very function I'M ZO SCHMART LOL
        if first_draw:
            vol_correction = -abs(self.pipette["back_pour_correction"])   # negative, force dispensing
            new_action = {"args": {"volume": vol_correction}}  # in microliters
            if self.verbose:
                print("\ngcodeBuilder message:\n    " + f"Using back_pour_correction with volume {vol_correction}")
                self.commands.append(f"; Action {_i}: applying {vol_correction} uL back-pour volume correction.")
            self.macroPipette(new_action, _i)  # in microliters

        return "Processed macroPipette action with index " + str(_i)

    def macroPipetteZero(self):
        """
        Move the pipette to the zero position (without re-homing).
        """
        # Reverse net displacement
        # volume = -self.state["vol"]
        volume = -self.pipettes[self.current_tool]["state"]["vol"]
        s_axis_movement = volume / self.pipette["scaler"]

        # Compose and append the pseudo-GCODE command (with volume units, in microliters)
        # command = self.pipetting_command_prefix + str(s_axis_movement)  # TODO: add pipetting speed control or something
        command = self.pipetting_command_prefix + str(volume)  # TODO: add pipetting speed control or something
        self.commands.append(command)

        # Update state trackers
        # self.state["s"] += s_axis_movement
        # self.state["vol"] += volume
        self.pipettes[self.current_tool]["state"]["s"] += s_axis_movement
        self.pipettes[self.current_tool]["state"]["vol"] += volume
        if self.tip_loaded:
            # self.state["tip_vol"] += volume
            self.pipettes[self.current_tool]["state"]["tip_vol"] += volume
        # Update direction with the current move
        self.pipettes[self.current_tool]["state"]["lastdir"] = sign(volume)

        # if (abs(self.state["vol"]) > 1e-10) or (abs(self.state["s"]) > 1e-10):
        if (abs(self.pipettes[self.current_tool]["state"]["vol"]) > 1e-10) or (abs(self.pipettes[self.current_tool]["state"]["s"]) > 1e-10):
            raise Exception("PROTOCOL ERROR: pipette was zeroed, but reference states did not match zero. " +
                            "Expected 0 vol and 0 s, but got: " +
                            str(self.pipettes[self.current_tool]["state"]["vol"]) + " vol" +
                            str(self.pipettes[self.current_tool]["state"]["s"]) + " s")

    def macroPipetteHome(self, retraction_mm=None, *args):
        """
        This command will be interpreted by the SAxis driver. TO-DO: this code really should be moved to driverSaxis.py
        It will use the limit switches to find the highest position, retract, and then lower it again by 'retraction_mm'
        :param retraction_mm: extra distance to the SAxis limit switch, defaults to self.pipetting_homing_retract
        :param args: extra arguments (ignored for now).
        """
        # Apparently I can't use "self" in default arguments at the beginning, noob (?)

        # Positive displacement for the retraction (i.e. lower pipette a bit after the limit switch is hit).
        # if retraction_mm is None:  # CAUTION: this code must be migrated to pipetteDriver.py
        #     retraction_mm = self.pipetting_homing_retract
        # s_axis_movement = abs(retraction_mm)

        # Which tool should be homed?
        if self.current_tool is None:
            which_tool = "all"
        else:
            which_tool = self.current_tool

        # Add the command
        # The following ends up calling "pipette.limitRetraction in gcodeSender.py
        self.commands.append(self.pipetting_homing_prefix + str(which_tool))

        # Corrección para el "pipeteo de menos" en la primera carga de volumen.
        # Lo que hace es mover la pipeta 20 uL hacia "arriba" (i.e. un movimiento de carga de volumen) despues del homing.
        # s_axis_movement = self.pipette["homing_backslash_compensation"] / self.pipette["scaler"]
        # s_axis_movement = abs(s_axis_movement)  # Debe ser positivo para que se eleve.
        # command = self.pipetting_command_prefix + str(s_axis_movement)
        # self.commands.append(command)

        # Zero the volumes
        # self.state["vol"] = 0
        # self.state["s"] = 0
        if which_tool == "all":
            for p in list(self.pipettes):
                self.pipettes[p]["state"]["vol"] = 0
                self.pipettes[p]["state"]["s"] = 0
                # Update direction with the current move
                self.pipettes[p]["state"]["lastdir"] = sign(-1)
        else:
            self.pipettes[self.current_tool]["state"]["vol"] = 0
            self.pipettes[self.current_tool]["state"]["s"] = 0
            # Update direction with the current move
            self.pipettes[self.current_tool]["state"]["lastdir"] = sign(-1)


        return "Pipette homed."

    def getWorkspaceItemByName(self, platform_name):
        """Iterate over items in the workspace looking for one who's name matches 'platform_name' """
        for item in self.workspace["items"]:
            if item["name"] == platform_name:
                return item
            else:
                continue
        return None

    def getPlatformByName(self, platform_item):
        """Iterate over platforms in workspace looking for one who's name matches the platform in 'platform_item' """
        for platform in self.platformsInWorkspace:
            if platform["name"] == platform_item["platform"]:
                return platform
            else:
                continue
        return None

    def macroSetupMachine(self):
        """
        Generate XYZ and pipette axis homing commands.
        Optionally probing the Z axis.
        """
        # TODO: rewrite the probing thing with new probe system

        # Home commands
        homing_commands = [
            "; Setup commands (from Class initialization)",
            gcodePrimitives.gcodeHomeXYZ(),
            # gcodePrimitives.gcodeG92(**self.g92_coords),  # DEPRECATED: now using hadrcoded G54 coordinate system.
            gcodePrimitives.gcodeSetFeedrate(self.feedRateDefault)
            # self.pipetting_homing_prefix + str(self.pipetting_homing_retract)  # TODO: fully evacuate the pipette
        ]
        self.commands.extend(homing_commands)

        self.macroPipetteHome()

        # DEPRECATED: Probe and eject probe
        if self.probe_z is not None:
            assert (not isinstance(self.probe_z, bool)) and isinstance(self.probe_z, (int, float))
            self.commands.extend([
                # gcodePrimitives.gcodePipetteProbe(z_scan=-abs(self.limits["z_max"]),  # Seek downwards only!
                #                                   feedrate=500,
                #                                   mode="G38.2"),  # This GCODE produces and error if probe doesn't fire during the move.
                gcodePrimitives.gcodePipetteProbe(z_scan=-2.5, mode="$J=G91", feedrate=200, axis="Z", prefix="PRB;"),
                gcodePrimitives.gcodeG92(z=self.probe_z)  # CAUTION: no me acuerdo por qué estaba esto.
            ])

        # Flag the machine as homed.
        self.homed = True

        return "Processed macroSetupMachine"  # DEPRECATED: + " with: " + gcodePrimitives.gcodeG92(**self.g92_coords)

    def macroPickNextTip(self, _action, _i):
        """
        Will throw an error if a tip is already loaded
        """

        if not self.tip_loaded:
            self.macroMoveSafeHeight()  # move to clearance level on Z, should extend task.command list automatically

            # move to next tip location on XY
            # _i = 1
            # _action = protocol["actions"][_i]  # for testing
            platform_name = _action["args"]["item"]
            platform_item = self.getWorkspaceItemByName(platform_name)
            platform_ = self.getPlatformByName(platform_item)

            # Get the next tip and delete it from the original platform_item object
            # So that it won't get picked again
            next_tip = next(content for content in platform_item["content"] if content["type"] == "tip")
            for i, content in enumerate(platform_item["content"]):
                if content["type"] == "tip" and content["index"] == next_tip["index"]:
                    del platform_item["content"][i]
                    # Referencing will remove it from the task class/workspace object class as well
                    # This is the desired (side)effect, but it does not propagate to the mongo database
                    break
                else:
                    continue

            # Calculate X coordinate
            _x = platform_item["position"]["x"]
            _x += platform_['firstWellCenterX']
            _x += next_tip["position"]["col"] * platform_['wellSeparationX']
            if self.current_tool is not None:
                _x += self.pipettes[self.current_tool]["tool_offset"]["x"]
            elif self.verbose:
                print("\ngcodeBuilder message:    \n" + "no Y offset added, self.current_tool was None.")

            # Calculate Y coordinate
            _y = platform_item["position"]["y"]
            _y += platform_['firstWellCenterY']
            _y += next_tip["position"]["row"] * platform_['wellSeparationY']
            if self.current_tool is not None:
                _y += self.pipettes[self.current_tool]["tool_offset"]["y"]
            elif self.verbose:
                print("\ngcodeBuilder message:    \n" + "no Y offset added, self.current_tool was 'None'.")

            # TODO: check and tune Z positioning according to tip seal pressure,
            #  this might need calibration. Ver issue #7
            # https://github.com/naikymen/pipettin-grbl/issues/7
            _z = platform_["defaultLoadBottom"][self.current_tool]
            # CAUTION: I must add this to the platform definition: "defaultLoadBottom": {"p200": 27, "p20": 20},
            # TODO: consider using the "z_bare" offset instead of the "defaultLoadBottom" property.

            # _z += platform_["tipLength"]

            # NO-PROBE CODE START
            # self.commands.extend([
            #     gcodePrimitives.gcodeMove(x=_x, y=_y),  # Move over the tip
            #     gcodePrimitives.gcodeMove(z=_z)  # Place tip
            # ])
            # NO-PROBE CODE END

            # Probe to place code START
            # TODO: make this a vibration if possible (i have not found this necessary).
            probe_to_place = self.pipette["tip_probing_distance"]  # Clearance distance before probing
            probe_extra_dist = self.pipette["probe_extra_dist"]
            # seal_retract = -(_probe_to_place + _sealing_move)
            # probe_final = _seal_retract
            z_scan = -(probe_to_place + probe_extra_dist)
            self.commands.extend([
                gcodePrimitives.gcodeMove(x=_x, y=_y),             # Move over the tip on XY
                gcodePrimitives.gcodeMove(z=_z+probe_to_place),    # Move over the tip on Z
                # Probe for the tip, with extra scan distance
                gcodePrimitives.gcodePipetteProbe(z_scan=z_scan, feedrate=500)
            ])
            # Probe to place code END

            # Save information about the loaded tip to the class
            next_tip["tipLength"] = platform_["tipLength"]
            next_tip["fromAction"] = _i
            next_tip["inTool"] = self.current_tool
            self.tip_loaded = next_tip

            # TODO: if multiple "extruders" are present,
            #  tip loading should do a G92 command with new XY and Z as well.
            # NOTE: I am handling this with tool offsets here, instead of messing with GRBL.

            self.macroMoveSafeHeight()  # Raise to safety. Should do an automatic append

            # Zero the tip volume tracking
            # self.state["tip_vol"] = 0
            self.pipettes[self.current_tool]["state"]["tip_vol"] = 0

            # move to Z just on top
            # TODO: seal the tip by pressing a little bit very slowly two times
            # move to clearance level on Z
        else:
            # TODO: discutir si es mejor descartar el tip
            #  si está automáticamente o tirar este error
            raise Exception("PROTOCOL ERROR: Cannot pick tip if one is already placed! Action index: " + str(_i))

        return "Processed macroPickNextTip action with index " + str(_i)

    # TODO: code the missing action interpreters

    def macroGoToAndPour(self, _action, _i, mode="incremental", *args):
        """
        Make volume "negative" and run macroGoToAndPipette: move to action XZY position and drop volume.
        :param _action: and "action" object from the protocol
        :param _i: action index in the protocol
        :param mode: pipetting mode
        :param args: ignore other arguments ignored for now.
        :return: "args" of the action: _action["args"]
        """
        # move to clearance level on Z
        # move to next tube location on XY
        # move to Z at dispensing height
        # dispense liquid
        # flush tip if all must be dropped, consider pipetting mode if reverse or repetitive
        # move to clearance level on Z

        _action["args"]["volume"] = -abs(float(_action["args"]["volume"]))  # Force negative volume

        ret_msg = self.macroGoToAndPipette(_action, _i)

        return ret_msg

    @staticmethod
    def filterTubesBy(content, _selector):
        """Helper function to use the tube "selector" dict in python's filter()"""
        if content["type"] == "tube" and content[_selector["by"]] == _selector["value"]:
            return True
        else:
            return False

    def macroGoToAndPipette(self, _action, _i):
        """
        LOAD_LIQUID action interpreter: moves to XYZ position and moves S axis to pipette.
        Will throw an error if a tip is not already loaded.
        """
        # move to clearance level on Z
        # move to next tube location on XY
        # move to Z at loading height
        # load liquid
        # move to clearance level on Z

        if self.tip_loaded:
            # move to clearance level on Z
            self.macroMoveSafeHeight()  # should extend task.commands list automatically

            # move to next tip location on XY
            # i = 3
            # _action = task.protocol["actions"][i]  # for testing
            platform_item = self.getWorkspaceItemByName(_action["args"]["item"])
            platform_ = self.getPlatformByName(platform_item)

            # TODO: implement current volume / max volume limitation checks
            selector = _action["args"]["selector"]
            _tube = next(filter(lambda content: self.filterTubesBy(content, selector),
                                platform_item["content"]))

            # Calculate XY of tube center
            _x = platform_item["position"]["x"]
            _x += platform_['firstWellCenterX']
            _x += _tube["position"]["col"] * platform_['wellSeparationX']

            _y = platform_item["position"]["y"]
            _y += platform_['firstWellCenterY']
            _y += _tube["position"]["row"] * platform_['wellSeparationY']

            # Add the current tool's offsets
            if self.current_tool is not None:
                _x += self.pipettes[self.current_tool]["tool_offset"]["x"]
                _y += self.pipettes[self.current_tool]["tool_offset"]["y"]

            # Calculate Z for pipetting
            # First reference is the "bottom" of the tube
            # _z = platform_["defaultBottomPosition"]      # si esto es 20 mm en el platform
            # Increase height because a tip is placed
            # _z += self.tip_loaded["tipLength"]           # se le suuma 50 mm de tip length, para obtener 70 mm
            # Decrease height because the pipette sinks
            #  into the tip to make the "seal"
            # _z += -self.pipette["tipSealDistance"]       # se le resta la distancia que entra la pipeta en el tip, y queda a 62 mm xej.

            # TODO: how should this be callibrated?
            # TODO: check and tune Z positioning according to tip seal pressure or seal distance,
            #  this might need calibration. Ver issue #7 y #25
            # https://github.com/naikymen/pipettin-grbl/issues/7
            # https://github.com/naikymen/pipettin-grbl/issues/25

            # Calculate Z for pipetting: DEFAULTS TO p200 VALUE
            # _z = platform_["p200LoadBottom"]

            # Use tool offsets defined in the platform:
            # # "defaultBottomPositionTools": {
            # #     "p200": 29,
            # #     "p20": 9
            # # }
            _z = platform_["defaultBottomPositionTools"][self.current_tool]

            # Add 5% to a volume loading pipette displacement:
            # Comentado porque no era necesario, el fix real es después del homing,
            # ver como agregar corrección de backslash de 0.5 uL,
            # y pipetear un poco de más al cargar liquido.
            # if _action["args"]["volume"] > 0:
            #    _action["args"]["volume"] = _action["args"]["volume"]*1.05

            self.commands.extend([
                gcodePrimitives.gcodeMove(x=_x, y=_y),  # Move over the tip
                gcodePrimitives.gcodeMove(z=_z)  # Go to the bottom of the tube
                # TODO: be smarter about pipetting height, using current tube volume to increase this.
            ])

            self.macroPipette(_action, _i)  # in microliters

            self.macroMoveSafeHeight()

            # move to Z just on top
            # TODO: seal the tip by pressing a little bit very slowly two times
            # move to clearance level on Z
        else:
            # TODO: discutir si es mejor descartar el tip
            #  si está automáticamente o tirar este error
            raise Exception("PROTOCOL ERROR: Cannot load or dispense without a tip. Action index: " + str(_i))

        return "Processed macroGoToAndPipette action with index " + str(_i)

    def toolChangeMacro(self, new_tool, current_tool=None, return_commands=False):
        # Mark a toolchange
        self.commands.append("toolchange;" + str(new_tool))

        # Park current tool, if any.
        park_commands = []
        if current_tool is not None:
            safe_z = self.getSafeHeight()
            safe_y = self.getSafeY()
            # Required arguments: "x, y, z, y_final, y_clearance, z_parking_offset, safe_z, safe_y".
            # "tool_post": {"x": 316, "y": 276, "z": 45, "y_final": 312, "y_clearance": 110, "z_parking_offset": 0},
            tool_post_coords = deepcopy(self.pipette["tool_post"])
            tool_post_coords["z_parking_offset"] = 0  # Because we are parking something.
            park_commands = gcodePrimitives.toolchange_probe(**tool_post_coords,
                                                             safe_z=safe_z, safe_y=safe_y,
                                                             extra_scan=1, closeup_dist=10,
                                                             mode_fwd="G38.2", mode_rev="G38.4", feedrate=200)
            # Add parking commands to the list.
            self.commands.extend(park_commands)

        else:
            # There is nothing to do if a tool is _not_ currently loaded.
            pass

        # Update the current tool.
        self.current_tool = new_tool
        # Update the current tool properties.
        if self.current_tool is None:
            # This should happen for a parking-only toolchange.
            self.pipette = None
        else:
            # And this for a normal toolchange
            self.pipette = self.pipettes[self.current_tool]  # save chosen micropipette model.

        # Pick-up the next tool
        pickup_commands = []
        if new_tool is not None:
            safe_z = self.getSafeHeight()
            safe_y = self.getSafeY()
            # Required arguments: "x, y, z, y_final, y_clearance, z_parking_offset, safe_z, safe_y".
            # "tool_post": {"x": 316, "y": 276, "z": 45, "y_final": 312, "y_clearance": 110, "z_parking_offset": 0},
            tool_post_coords = self.pipette["tool_post"]
            pickup_commands = gcodePrimitives.toolchange_probe(**tool_post_coords,
                                                               safe_z=safe_z, safe_y=safe_y,
                                                               extra_scan=1, closeup_dist=10,
                                                               mode_fwd="G38.2", mode_rev="G38.4", feedrate=200)
            # Add pickup commands to the list.
            self.commands.extend(pickup_commands)
        else:
            # There is nothing to do if the toolchange was just for parking.
            pass
        
        # Return
        if return_commands:
            return park_commands + pickup_commands
        else:
            return True

    def parseAction(self, i, action):
        """This function interprets actions in the JSON file and produces a GCODE version of the protocol"""
        # Python switch case implementations https://data-flair.training/blogs/python-switch-case/
        # All action interpreting functions will take two arguments
        # action: the action object
        # i: the action index for the current action
        # task: the object of class "TaskEnvironment"

        # Register new action start as comment in the GCODE
        self.commands.extend(["; Building action " + str(i) + ", with command: " + str(action["cmd"])])

        # List of available macros for the requested actions
        # TODO: implement repeat dispensing
        # TODO: implement reverse pipetting
        # TODO: implement available/maximum volume checks
        switch_case = {
            "HOME": self.actionHOME,
            "PICK_TIP": self.macroPickNextTip,
            "LOAD_LIQUID": self.macroGoToAndPipette,
            "DROP_LIQUID": self.macroGoToAndPour,
            "DISCARD_TIP": self.actionDISCARD_TIP,
            "PIPETTE": self.macroPipette,
            "COMMENT": self.actionComment,
            "HUMAN": self.actionHuman,
            "WAIT": self.actionWait
        }

        # Check for a toolchange
        self.toolchange = False
        if "args" in list(action):
            # Check for arguments in action
            if "tool" in list(action["args"]):
                # Check for a tool specification.
                # If the tool specification is different from the current one,
                # a tool-change is needed before the action takes place.
                if not action["args"]["tool"].lower() == self.current_tool:
                    # Flag the tool-change, so it is prepended to the list of actions
                    self.toolchange = True  # self.current_tool = None
                    # TODO: Save state
                    # self.pipettes[self.current_tool]["current_volume"] = copy(self.state["vol"])

        # Append the tool-change command to the action commands if a toolchange is needed
        if self.toolchange:
            new_tool = action["args"]["tool"].lower()
            self.toolChangeMacro(new_tool=new_tool, current_tool=self.current_tool)

        # Park the loaded tool if any, before final homing.
        if (not self.toolchange) and (i + 1 == len(self.protocol["actions"])) and action["cmd"] == "HOME":
            if self.current_tool is not None:
                self.toolChangeMacro(new_tool=None, current_tool=self.current_tool)

        # Produce the GCODE for the action
        try:
            if self.verbose:
                print("\nProcessing command", action["cmd"], "with index", str(i))
            action_function = switch_case[action["cmd"]]
            action_commands = action_function(action, i)
        except Exception as err:
            print(f"\nERROR: protocolInterpreter at action with index {i} and command: " + action["cmd"])
            print("Error message:", err)
            print("Traceback: ")
            print(traceback.format_exc())  # https://stackoverflow.com/a/3702847/11524079
            sys.exit("Protocol parser error!")

        if self.verbose:
            print("\nDone processing command", action["cmd"], "with index", str(i))
            print(action_commands)

        return action_commands

    def parseProtocol(self):
        """This function takes a task object and parses its actions into gcode"""

        # TODO: que los actions guarden su index en el dict, así no tengo que generarlos acá.
        protocol_parsed = [self.parseAction(i, action) for i, action in enumerate(self.protocol["actions"])]

    def save_gcode(self, path="examples/test.gcode"):
        """Save the GCODE to a file"""
        print(f"\ncommanderTest.py message: Saving GCODE to file: {path}\n")
        with open(path, "w") as gcode_file:
            gcode_file.writelines(self.getGcode())
