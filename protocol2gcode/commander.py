#!/usr/bin/python3
from collections import namedtuple
from commander_utils import run_commander
from gcodeCommander import Commander
import time
import systemd.daemon
import socketio
import json
import serial as serial
import traceback

# Commander systemd service
# Based on this guide: https://github.com/torfsen/python-systemd-tutorial
# Thanks!

"""
Systemd unit definition, located at "/home/pi/.config/systemd/user/commander.service".

Tracked by the git repo at "systemd.units/commander.service". See the README file at its directory.

The unit can be enabled with these commands:

$ systemctl --user daemon-reload  # reload user service files after editing
$ systemctl --user enable commander.service  # enable service on boot

Importantly, run "sudo loginctl enable-linger $USER" at least once as the pi user.

Also run "apt-get install python-systemd python3-systemd" to install the systemd python module.

To test this, a useful command is:

systemctl --user restart commander.service; journalctl --user-unit commander -f  # restart serviceand follow the logs
"""

# This flag is used by "command_message_handler" to check wether it has already been called, and "busy".
busy = False


def command_message_handler(new_args):
    """
    Handler for 'p2g_command' socket events.
    :param new_args: a named dictionary with arguments for the Commander.
    :return: process exit code.
    """

    # Pre-process arguments: update defaults and convert the dict to a named tuple.
    args = parse_args(new_args)

    # Read "global" busy status
    global busy

    if busy:
        print("Received GUI command while the commander is busy, ignoring call with content: " + str(new_args))
        e_code = 1
    else:
        # If we were not busy, we must be now! :)
        if args.move is None:
            # But busy should only be set if it is _not_ a Jog command.
            # Since jogging should not be a blocking command.
            busy = True

        print("Received GUI command while the commander is idle, parsing call with content: " + str(new_args))

        try:
            # Pass arguments to the robot "commander".
            # Note the "sio" SocketIO Client object is defined in the calling environment below (in the main loop).
            # The same holds for the "commander" object.
            e_code = run_commander(sio, args, commander=commander)

            # # The commander should be "tidy" before executing a protocol.
            # if args.run_protocol is not None:
            #     e_code = run_commander(sio, args, commander=commander)
            # # Other kinds of commands can go on with less overhead.
            # else:
            #     e_code = run_commander(sio, args, commander=commander)

        except Exception as e:
            print("Exception at run_commander() call: " + str(e))
            print(traceback.format_exc())
            e_code = 1

        # Mark not busy
        busy = False

    # Exit code return
    return e_code


def default_args(port="/dev/ttyACM0",
                 baudrate=115200,
                 run_protocol=None,
                 calibration=None,
                 mongo_url='mongodb://localhost:27017/',
                 workspace=None,
                 pipette_model="p200",
                 item=None,
                 content=None,
                 move=None,
                 distance=None,
                 command=None,
                 home=None,
                 s_retract=16.8,
                 interactive=False,
                 dry=False, really_dry=False,
                 verbose=True,
                 tool=""):
    """
    Generate default arguments dictionary for parse_args().
    : port          : Path to the arduino serial port interface, such as "/dev/ttyUSB0".
    : baudrate      : Baudrate of the Arduino's serial interface.
    : mongo_url     : URL to the MongoDB server.
    : home          : If set to a valid string, the specified machine's axis homed (i.e. "xyz").
    : calibration   : If set to a valid string, it performs a "callibration" type move (i.e. a "goto" callibration command).
    : run_protocol  : If set to a valid protocol name, it runs a protocol. It must match the name of a protocol in MongoDB.
    : move          : If set to a valid string, it indicates a "move" type command ("jogging" in GRBL/CNC jargon).
    : command       : If set to a string with a valid GRBL comand, it is passed directly to the Arduino's serial interface.
    : distance      : Distance relevant to the command, if any (i.e. for the "move" command).
    : workspace     : Workspace name for the protocol.
    : item          : Name of the relevant item in the workspace, if any (i.e. for the "move" or "callibration" commands).
    : content       : Name of the relevant content in the workspace, if any (i.e. for the "move" or "callibration" commands).
    : pipette_model : Name of the pipette model in use (only "p200" is supported in V1.0).
    : s_retract     : Retraction distance of the "s" axis (pipetting axis, the "s" used to be for "spindle" of the CNC/GRBL world).
    : interactive   : If True, the user is offered a command-line interface (CLI) to debug a protocol or just send commands.
    : dry           : If True, no instructions are actually sent, they are only "built".
    : really_dry    : If True, no instructions are actually sent, they are only "built", and "commander_run()" is never called.
    : verbose       : If True, a lot of messages will be printed to the console.
    """

    dflt = {"port": port,
            "baudrate": baudrate,
            "run_protocol": run_protocol,
            "calibration": calibration,
            "mongo_url": mongo_url,
            "workspace": workspace,
            "pipette_model": pipette_model,
            "item": item,
            "content": content,
            "move": move,
            "distance": distance,
            "command": command,
            "home": home,
            "s_retract": s_retract,
            "interactive": interactive,
            "dry": dry,
            "really_dry": really_dry,
            "verbose": verbose,
            "tool": tool}

    return dflt


def parse_args(new_args=None):
    """
    Generates arguments for run_commander(). First create defaults and update them with new_args, then convert the dict to a named tuple.
    :param new_args: dictionary with new arguments.
    :return: a named tuple with updated arguments.
    """

    # Default arguments
    dflt = default_args()

    # Update default with new values
    if new_args is not None:
        # Fix invalid name for namedtuple conversion
        if "run-protocol" in new_args.keys():
            new_args["run_protocol"] = new_args.pop("run-protocol")
        # Update values with new ones
        dflt.update(new_args)

    if dflt["verbose"]:
        print("commander parse_args: parsed arguments:")
        print(json.dumps(dflt, indent=2))

    # Convert to named tuple (analogue for "parser.parse_args()"
    args = namedtuple("Arguments", dflt.keys())(*dflt.values())

    return args


def prepare_commander(new_args=None, serial_object=None, sio=None):
    args = parse_args(new_args=new_args)  # None => default argument/options.

    new_commander = Commander(
        serial_device_path=args.port, baudrate=args.baudrate, serial_object=serial_object,
        pipette_model=args.pipette_model,
        sio_object=sio,  # Defined below as socketio.Client()
        dry=args.dry, interactive=args.interactive,
        verbose=args.verbose)

    return new_commander  # ready to do "serial.open()"


# Guard code: executes only when the "module" is the main program (i.e. it is called as "python commander.py" from the CLI).
# If the module is imported, the following does _not_ run.
# Effectively it means that the following will run when called by systemd, but will not if imported by the user in another script.
if __name__ == '__main__':

    """
    Arguments:
        port
        baudrate
        run-protocol
        calibration
        workspace
        item
        content
        move
        distance
        command
        home
        s_retract
        interactive
        dry
        verbose
    """

    # Global flag for a "busy" commander
    # global busy

    print('Starting up ...')

    # Instantiate socket object
    sio = socketio.Client()
    sio.on('p2g_command', command_message_handler)  # No arguments can be passed, except for the "data".

    # Instantiate and start the commander object:
    commander = prepare_commander(sio=sio)
    commander.start_threads()

    # Connect to the socket IO
    try:
        if not sio.connected:
            socket_port = 3333
            # Connect to socket
            sio.connect('http://localhost:' + str(socket_port))
            # Send a startup message to the GUI
            sio.emit('alert', {'text': "commander.py has start up.", 'type': 'message'})
        else:
            pass
    except socketio.exceptions.ConnectionError as err:
        print("Socket.io ConnectionError: " + str(err) + ". Retrying in 5 seconds.")
    except Exception as err:
        print("Other exception occurred: " + str(err) + ". Retrying soon.")

    # Keep socket connection alive
    while True:
        try:
            if not sio.connected:
                socket_port = 3333
                # Connect to socket
                sio.connect('http://localhost:' + str(socket_port))

            if commander.killed:
                # raise Exception("Commander killed! Must restart the systemd service.")
                print("The commander seems dead! Was it killed a single time?")

        except socketio.exceptions.ConnectionError as err:
            print("Socket.io ConnectionError: " + str(err) + ". Retrying in 5 seconds.")
            systemd.daemon.notify('RELOADING=1')  # https://www.freedesktop.org/software/systemd/man/sd_notify.html#

        except Exception as err:
            print("Commander.py unexpected error: " + str(
                err) + ". Cleaning up the commander and stopping commander.py main script.")
            sio.disconnect()
            systemd.daemon.notify("STOPPING=1")

            # Cleanup the commander
            commander.cleanup()
            break

        else:
            # When there is no error in the try block, check if the socket is OK and notify systemd.
            if sio.connected:
                print("Websocket connected!")
                systemd.daemon.notify('READY=1')  # https://www.freedesktop.org/software/systemd/man/sd_notify.html#

        time.sleep(2)
    # End main loop #

# Register websocket event listener function for "start human intervention"
# sio.on('human_intervention_continue', continue_message_handler)
# Register websocket event listener function for "cancelled human intervention"
# sio.on('human_intervention_cancelled', abort_message_handler)  # initialize event listener

