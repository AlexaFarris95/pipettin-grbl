# Systemd units for pipetting-grbl

"Systemd units" handle the startup of the Node server (for the [GUI](../gui/README.md)) and of the Python machine [drivers](../protocol2gcode/README.md).

## Setup

> Setup has already been done for you in the ISO image.

Configure systemd linger, at least once as the `pi` user:

```bash
sudo loginctl enable-linger pi
```

Install the systemd Python module, required by the "commander" unit:

```bash
sudo apt-get install python-systemd python3-systemd
```

Unit definitions reside at `~/.config/systemd/user/` in the RPi's filesystem.

Create the directory, link the units, and reload the systemd user daemon:

```bash
mkdir -p ~/.config/systemd/user/
# Link
ln -s /home/pi/pipettin-grbl/systemd.units/nodegui.service ~/.config/systemd/user/
ln -s /home/pi/pipettin-grbl/systemd.units/commander.service ~/.config/systemd/user/
# Reload
systemctl --user daemon-reload
```

Some status info is printed on login by bash (see `~/.bashrc`).

You may use standard systemctl commands to check on the services manually.

For example:

```bash
systemctl --user status nodegui.service
systemctl --user status commander.service
```

Handy bash aliases to restart the units are defined at `~/.bashrc`, such as:

```bash
# Restart units and follow logs
alias commander_restart='systemctl --user restart commander.service; journalctl --user-unit commander -f'
alias gui_restart='systemctl --user restart nodegui.service; journalctl --user-unit nodegui -f'
```

## Node GUI

Located at: `systemd.units/nodegui.service`

For development, this file uses `nodemon` instead of `node`.

A compatible (legacy) nodemon version was installed with:

```bash
sudo npm install -g nodemon@2.0.12 # Details at https://github.com/remy/nodemon/issues/1948#issuecomment-953665876
```

Start and enable the unit:

```bash
systemctl --user start nodegui.service
systemctl --user status nodegui.service
systemctl --user enable nodegui.service
```

> Note: edit the unit file to use `node` instead of `nodemon` for deployments.

## Python3 commander

Located at: `systemd.units/commander.service`

Learn more about it at: [`protocol2gcode/README.md`](../protocol2gcode/README.md)

Start and enable the unit:

```bash
systemctl --user start commander.service
systemctl --user status commander.service
systemctl --user enable commander.service
```

## Jupyter Lab

Unit file: [jupyter.service](./jupyter.service)

To be placed at `/etc/systemd/system/jupyter.service`.

```bash
sudo cp jupyter.service /etc/systemd/system/jupyter.service
sudo systemctl daemon-reload
sudo systemctl start jupyter.service
sudo systemctl status jupyter.service
sudo systemctl enable jupyter.service
```

## GitBuilding docs

Unit file: [gitbuilding.service ](./gitbuilding.service )

Requirements:

- docs repo at `/home/pi/pipettin-grbl-docs/`
- venv with gitbuilding installed at `/home/pi/pipettin-grbl-docs/bot_venv`
- Learn more at: https://gitlab.com/pipettin-bot/pipettin-grbl-docs

```bash
sudo cp gitbuilding.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl start gitbuilding.service
sudo systemctl status gitbuilding.service
sudo systemctl enable gitbuilding.service
```
