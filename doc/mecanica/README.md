Con poleas en los ejes XY, va más rápido.

Un motor de 200 (1.8º) pasos por vuelta, con una polea GT2 de 20 dientes tiene precisión más que suficiente (10 um).

Ver calculadora online: https://blog.prusa3d.com/calculator_3416/#steppermotors

