# Propuesta de interoperabilidad con OT2

Ver: https://github.com/Opentrons/opentrons/issues/11542

- [ ] Hablar con Fiendizymes.
- [ ] Hablar con Facu.
- [ ] Hablar con OpenTrons.

## Diagrama borrador

- [ ] Armar un diagrama para definir interfaces de compatibilidad.
- [ ] Definir qué trabajo tomaría cada interfaz.
- [ ] Elegir la más feliz para empezar.

![interop_diagram.svg](./interop_diagram.svg)

## Comment en su repo

Markdown copiado del issue: https://github.com/Opentrons/opentrons/issues/11542

### Overview

Hi!

First of all I wanted to say thanks for this great project.

Our request is about making software for pipetting robot compatible / interoperable. Specifically, we would like to add compatibility with the hardware/electronics of other CNC machines (for example, to reuse a 3D printer / CNC mill).

We'd like to invite you to chat for a bit, and decide which is the happiest place to add interoperability between different machines.

### Implementation details

There might be a sweet-spot for adding interoperability to open-hardware automation.

The high level JSON protocol produced by the protocol designer seems nice, because it seems rely little on the specific CNC hardware in the machine (other than knowing the available tools). It is also easier to understand by a human.

The python API also looks promising. But I would like to know how tied it is to the "drivers" for the CNC hardware in the OT2.

### Design


Here is my understanding of pipetting machines at birds eye view:

![interop_diagram](https://user-images.githubusercontent.com/3259326/194566604-ff55ef02-409f-44eb-8c05-bb93b6909a22.svg)

The top one is my impression of the OT, and the bottom one is the one we're developing.

> A note on motivation: there are a couple OT2's at our faculty, but we had a few incentives to start from scratch: (0) they are not easy to get to, and purchasing an OT2 or it's parts is out of our budget, (1) to have fun making stuff, and (2) because we had a few ideas to improve the machine (which we'd be happy to share).


### Acceptance criteria

- [ ] Have a chat.
- [ ] Define possible interfaces for interoperability.
- [ ] Decide which one is the happiest.
- [ ] Collaborate on its development.

Thanks again :)