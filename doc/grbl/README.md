# Información sobre GRBL y nuestras modifiaciones

Versión: 1.1h

# Espacio de coordenadas

Queremos:

* Dirección X positiva hacia la derecha, con origen a la izquierda.
* Dirección Y positiva hacia abajo, con origen arriba.
* Dirección Z positiva hacia arriba, con homing hacia arriba, y origen al fondo del eje (o sea abajo).

Como acompañamiento gráfico alas instrucciones más abajo, ofrecemos esto:

![sistema_de_coordenadas_final.svg](./sistema_de_coordenadas_final.svg)

## Ejes XY

Queremos:

* Dirección X positiva hacia la derecha, con origen a la izquierda.
* Dirección Y positiva hacia abajo, con origen arriba.

Es el estándar de las imágenes y de las cosas web, al contrario del [estándar de los CNCs](https://github.com/gnea/grbl/wiki/Frequently-Asked-Questions#why-is-grbl-in-all-negative-coordinates-after-homing-or-it-so-annoying-and-not-what-im-used-to).

Para hacer este setup, conviene usar un programa como bCNC. Para que bCNC registre bien las coordenadas actuales, hay que setear  `$10=1` en GRBL (`$10=0` no funcionaba).

Si usaron el firmware de GRBL que está en el repo del robot pipeteador, sigan estos pasos para poner los ejes XY en espacio positivo:

1. Hacer homing.
2. Usar el joystick para moverse un poco a la derecha (eje X) usando la flecha.
    - La máquina se debería mover alejándose del limit switch del eje X.
    - Si en cambio se acerca, hay que invertir la dirección del eje X con `$3` y la dirección del homing del eje X con `$23`.
3. Usar el joystick para moverse un poco a la derecha (eje X) usando la flecha.
    - La máquina se debería mover alejándose del limit switch del eje Y.
    - Si en cambio se acerca, hay que invertir la dirección del eje Y con `$3` y la dirección del homing del eje Y con `$23`.
    - También hay que invertir la dirección del eje "A". Es posible que el eje "A", que copia al eje Y, no invierta su dirección con `$3`. En ese caso hay que dar vuelta manualmente el conector del stepper (del eje A) que va conectado al CNC shield.

## Eje Z

Queremos:

* Dirección positiva hacia arriba.
* Homing hacia arriba.
* Origen al final del eje.

Como tenemos el end-stop arriba, lo complicado es eso último. Pasos:

1. Abrir bCNC.
2. Hacer homing en Z.
3. Chequear que la máquina esté en el `0,0,0` después de hacer homing.
4. Bajar el eje Z hasta que esté 1 mm por sobre el límite inferior, y tomar nota del la coordenada Z.
  - Por ejemplo, podría ser que nuestro Z mínimo `-259`.
5. Hacemos homing del eje Z de nuevo.
6. Chequear que la máquina esté en el `0,0,0` después de hacer homing.
7. Setear el sistema de coordenadas de trabajo (`G54`) con `G10 L2 P1`, para que el punto más alto se mida con el cero en el límite inferior del eje Z.
    - Por ejemplo, si nuestro Z mínimo antes fue  `-258`, entonces usaríamos `G10 L2 P1 Z-258`.
9. Bajar el eje Z al mínimo
    - Por ejemplo, si nuestro Z mínimo antes fue  `-258`, entonces usaríamos `G0 G90 Z0 F1000`.

> Nota: También se puede hacer con la GUI, pero es más complicado porque necesitamos `minicom` en algún momento. Ver el [README.md](../tool_callibration/README.md) en [doc/tool_callibration](../tool_callibration).

# TO-DO

Otras modificaciones (PWM, gatry, homing, settings, etc.).

Ver si vale la pena incluir esto
![sistema_de_coordenadas.svg](./sistema_de_coordenadas.svg)