# Development documentation

> Disclaimer: there are lots of "to-do"s around here.

The projects usage and assembly documentation has been moved to [this repo](https://gitlab.com/pipettin-bot/pipettin-grbl-docs) and published in [gitlab pages](https://pipettin-bot.gitlab.io/pipettin-grbl-docs).

Development documentation will remain in this repo, distributed in README files, one per directory, across the whole repository.

This directory holds "extra" development documentation, not belonging in other directories.

The most important sub-directories in this "doc/" folder are:

* [grbl](./grbl) conteins information on the coordinate system for GRBL: [README.md](./grbl/README.md).
* [tool_callibration](./tool_callibration) contains tool calibration procedures: [README.md](./tool_callibration/README.md).
* [electronica](./electronica) contains notes on the electronics of the machine: [README.md](./electronica/README.md).

Documentation is available under the terms of the [CC-BY-SA 4.0 licence](https://gitlab.com/pipettin-bot/pipettin-grbl/-/blob/master/LICENSE.md#documents).
