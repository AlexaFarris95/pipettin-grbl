# Electronics

Contents:

[[_TOC_]]

A first look:

![electronics_v3_overview.jpg](./images/electronics_v3_overview.jpg)

## Raspberry Pi

General connections:

* To GRBL, using a standard USB cable.
* To the Pololu and the limit switch, using the GPIO header.
* To the arduino reset pin, from the GPIO header and through a transistor.

### Schematics (v4)

In this version, two Arduinos with 3.0 CNC shields are used.

They are now, however driven by USB through Klipper, instead of GRBL.

![setup.png](../../klipper/images/setup.png)

Pin mappings are now defined here: [printer.cfg](../../klipper/printer.cfg)

A coarse diagram is available, mapping connections for the XYZA motors and sensors:

![klipper_pin_map.svg](./arduino_cnc_shield/klipper_pin_map.svg)

### Schematics (v3)

A second stepper driver must be added to the toolchanging robot. The drivers are mounted on a secondary  _bare_ CNC shield (v3.0) to run the pipette steppers (without the Arduino UNO).

The homing limit switch pins are shared (they cannot be homed simultaneously for now).

Each pipette has a "tip probe". These rely on optostops (similar to the [Gen7 OptoStop v2.1](https://reprap.org/wiki/OptoEndstop_2.1)) to detect a correcly placed tip, and send their signal to the Pi's GPIO _separately_, because they are "normal closed" endstops (and thus not easily wired in parallel).

Each tool also has "probe endstops" used by GRBL to park and grab tools from the parking posts. The signal pins are shared between tools.

![circuit_diagrams.svg](./circuit_diagrams/exports/circuit_diagrams.svg)

> KiCAD circuit diagram. The pin numbers have been assigned provisionally at [gcodeCommander.py](../../protocol2gcode/gcodeCommander.py) (reset pin) and [pipetteDriver.py](../../protocol2gcode/pipetteDriver.py) (pipette pins). The assignments will be moved to the GUI in a later version ([issue 42](https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/42)).

### Boards and components

* Arduino + CNC shield.
* Raspberry Pi
* Bare CNC shield, driven by the Pi.

![electronics_v3.png](./images/electronics_v3.jpg)

> Components: (1) An Arudino UNO + CNC shield operate the XYZ axis of the CNC machine; (2) A bare CNC shield with 2 drivers controls the pipettes, and controled directly from (3) the Pi's GPIO header; (4) a small circuit to trigger a hard-reset on the Arduino UNO, from the Pi's GPIO (needed because of the logic voltage difference: 5v vs 3.3V).

##  End-stop sensors

Main modifications:

* Small, 4.7 uF filtering capacitors were added in front ot all NO mechanical endstops (i.e. the two pipette homing switches, and the two parking endstops), to supress EMI noise issues.
* For mechanical NO endstops wired in parallel, the NC pins of the switches were severed. Otherwise a short circuit [can occur](https://softsolder.com/2017/11/30/mpcnc-makerbot-style-endstop-switch-modification/).

Endstop sensor net count: 6

* Two pipette homing endstops: 
    * Can be connected in parallel; the NC pins in the switch **must** be severed for this.
* Two normal closed (NC) optical probes: cannot be connected in series or parallel.
    * It means that the probing pin cannot be used for both, and thus cannot be used with GRBL easily.
* Two parking endstops: can be connected in parallel.
    * Probing is already used for testing toolchanges, wiring a couple NO endstops in parallel is simple, and the Arduino can handle normal open with $6=0. The NC pins on the switch **must** be severed, or the controller will burn.

Current approach:

* Home pipettes as usual, reading from two mechanical NO endstops with a shared GPIO pin.
    * No special soldering.
* Use two mechanical endstops and the one probe pin on the CNC shield for parking stuff. A single pin for two tools seems allright, they are never used simulateneuosly.
    * No special soldering.
* Probe for tips using a different GRBL command (maybe a slow Jog?), one that can be quickly cancelled.
    * No soldering.
    * "Jogs" and "Feed hold" seem to be our friends [here](https://github.com/gnea/grbl/wiki/Grbl-v1.1-Commands#grbl-v11-realtime-commands): "_If jogging, a feed hold [a real-time command] will cancel the jog motion and flush all remaining jog motions in the planner buffer. The state will return from JOG to IDLE or DOOR, if was detected as ajar during the active hold. [...] An input pin is available to connect a button or switch_".
    * Should use either the realtime command, or triggering the Arduino feed-hold pin from GPIO which seem fast. Software is easier though.

## CNC Shield 3.0 and Modifications

We used a slightly modified CNC Shield v3.0 (clone).

### Filtering capacitors

There is one 4.7 uF electrolytic capacitor between the signal and ground cables, of each of the endstops connectors. Add the capacitor as close as possible to the connectors (on the CNC shield, or the Pi's GPIO).

This is because they were too sensitive to [EMI](https://en.wikipedia.org/wiki/Electromagnetic_interference) noise.

Learn more here: https://marlinfw.org/docs/hardware/endstops.html

Would have loved to have NC (normal closed) endstops around. But nope!

### End-stops and modifications

Important for homing the Y/A axis, and for shared endstops in general, if and only if you _are_ feeding the red wire with voltage (which only feeds the nice red lights on the end-stop's board).

In parallel configuration, the NC pin of all switches needs to be severed, in order to prevent a short-circuit (power-loss and reset, etc.).

References:

* https://softsolder.com/2017/11/30/mpcnc-makerbot-style-endstop-switch-modification/
* https://wiki.frubox.org/proyectos/diy/cnc?s[]=limit#modificacion-con-end-stops-de-makerbot

### Pololu Vref adjustment

Set to 1.1 V, limiting total machine current to less than 2 A. Could be less I guess, haven't tested.

See https://wiki.frubox.org/proyectos/diy/cnc#ajuste-vref-pololu

More voltage means more reliable stepping, and means less current, which means less heat.

I have seen several Pololus die on 1.1V, so I reduced it to 0.7-0.8V for the time being. Their death was apparent when one of the machine's axis started stuttering, and the tiny heatsink was damn hot.

### Cooling fan

Stepper drivers can overheat at higher Vref without a cooling fan.

In all cases, the longer the run the more important cooling became.

Use a fan always, just in case.

### Clone the Y axis to A

We have a double Y axis; the A axis is configured to follow the Y axis in GRBL's firmware configuration, and home independently. CNC shield pins are setup [accordingly](https://blog.protoneer.co.nz/arduino-cnc-shield/).

This requires that the **Z** and A endstops are wired in parallel. There are two options:

* Plug the Z endstop to the +Z pins and the A endstop to the Z- pins, and add a filtering capacitor to one of them.
* Cut and rewire the endstops to share the same connector and filtering capacitor.

> Note: we use the square gantry feature. This is incompatible with the the spindle PWM in the CNCshield 3.0 clone.

### Reset pin capacitor

Add a capacitor between the reset pin and GND, to prevent resetting the Arduino on serial open/reconection (which locks it).

It also prevents uploading new firmware, do it after flashing GRBL :)

See: https://forum.arduino.cc/index.php?topic=21479.msg159720#msg159720

### Stepper wiring and directions

Important for correct workspace coordinate system. Please read the [doc/grbl/README.md](../grbl/README.md) file.

The idea is that the machine lives in "positive space" with an origin at the top-left (which is what web developers use as coordinate system :shrug:).

See also: https://wiki.frubox.org/proyectos/diy/cnc#stepper-wiring-y-sistema-de-coordenadas

## Reset Arduino from Pi's GPIO

Sometimes the Arduino needs to be reset by the RPi, and we need to discharge the reset capacitor.

The voltages are different (3.3V GPIO vs 5V Arduino), and grounding the Arduino with the Rapsberry seemed like a bad idea.

So, a 2N2222 transistor turned out useful for resetting/unlocking GRBL.

The concept is the same as in [this post](https://electronics.stackexchange.com/a/180709) but used a PNP transistor instead of an NPN.

# Deprecated

## Schematics (v1)

El diagrama de conexiones está en el archivo `diagrama_conexiones`.

El formato [`diagrama_conexiones.cddx`](diagrama_conexiones.cddx) puede editarse en: https://www.circuit-diagram.org/editor/

![](diagrama_conexiones.svg)

> Notes: The A and B pins in the stepper driver _are_ connected to a stepper, but not drawn in the diagram. The cricuit between the RPi and the Arduino is meant to reset GRBL running in the Arduino.

## Schematics (v2)

A second stepper driver must be added to the toolchanging robot. It shares all GPIO pins with the first, except for the STEP pin.

## Boards and components (v2)

![electronics_v2.png](./images/electronics_v2.png)

> Electronics [card]board: (1) power switch bewteen the PSU's 24V and the Pololu/CNC shield inputs; (2-3) 30x30 cooling fans; (4) CNC shield v3.0 clone; (5-6) A4988 drivers for the pipette steppers; (7) extra board where tip probes, pipette endstops, and parking endstops are connected; (8) Raspberry Pi 2; (9) behind these cables is the reset crictuit for the Arduino UNO.
> Note: capacitors for mechanichal and optical endstops have been added as close to the sensor pins as possible (i.e. on the Pi or Arduino side of the cables). Most of the times they were soldered on the cables directly, right after the connectors, others were added to the connector board (7).

### Pipette pololu capacitor

It is a "decoupling" capacitor.

Prevents voltage spikes :) it is connected across the main 24 Volt line.

## CNC Shield 3.0 and Modifications

### Invert pins 11 and 12 - DEPRECATED

> This modification is optional, it is no longer used.

To use the CNC shield with variable spindle GRBL 1.1h, we inverted pins 11 and 12.

This allows us to use the variable spindle PWM function to drive a servo (see `grbl/LEEME.md`).

See also: https://wiki.frubox.org/proyectos/diy/cnc#z-axis-limit-switch

### Servo-motor for tip ejection - DEPRECATED

> This modification is deprecated, it is no longer used.

Using the default pin for variable spindle in GRBL 1.1h (note that a rewire may be important if using an old CNC Shield v3.0 clone; this is described above).

The servo is powered by a 3A and 5V step-down power supply (a cheap buck converter).

Note that your servo might need adjustment of the spindle PWM frequency in GRBL to operate correctly (some help on that [here](https://github.com/grbl/grbl/issues/1398)).

## CNC shield hat - DEPRECATED

> Deprecated, not used.

I made a small hat to tidy up some of the modifications of the CNC Shield described previously.

KiCAD designs for the hat are in the `cnc_shield_hat` directory.

Also, KiCAD designs for the a CNC Shield are in the `sb-cnc-shield` (cloned repository from GitHub, not used here).

Settings for pcb2gcode[GUI] and a sample command are in the `pcb2gcode` directory.

My notes follow, feel free to skip them.

### KiCAD 101

KiCAD is an [EDA](https://en.wikipedia.org/wiki/Electronic_design_automation) software (for circuit design).

I used it to make some circuit diagrams, but later opted for the a much more simple [web app](https://www.circuit-diagram.org/editor/).

A simple video showing the basic workflow: https://www.youtube.com/watch?v=-tN14xlWWmA

The idea was to mill my own PCB using a Dremel-CNC router I was working on. Some details on that will follow.

### Installing and using pcb2gcode

This translates gerber files to millable gcode.

pcb2gcode might be moderately hard to install. I had to modeify a header file on `gerbv` 's source code for it to compile on modern GCC.

Then, to compile `pcb2gcode` with the headers in the `gerbv` I built in the previous step, it is important to set an environmen variable before running `./configure`:

    export PKG_CONFIG_PATH=~/Software/pcb2gcode/gerbv-2.7.0/src

The GUI was cloned, built, and installed without issues on my system.

There are some GRBL-specific configurations for `pcb2gcode`. Once taken into account, only minor modifications of the gcode are necessary.

Read for details: https://wiki.frubox.org/proyectos/diy/cnc/kicad_pcb

### Preview of the GCODE

This can be done on bCNC directly or on http://chilipeppr.com/grbl (or http://chilipeppr.com/jpadie).

### Editing and sending gcode with bCNC

The only important edits are:

  * Optional: add a homing cycle and your G92 command.
  * Remove unnecessary header commands.

See: https://wiki.frubox.org/proyectos/diy/cnc#bcnc

# To-do 

- [ ] Update the circuit diagram of the "v2 schematics".
* [ ] Completar este readme.
* [ ] Como se conectaron los limit-switches (en particular para el doble eje Y).
* [x] Como se concta el reset del Arduino al GPIO del Pi.
* [ ] Alimentación.
* [ ] Pololus, refrigeración y ajuste de Vref.
* [ ] Conexion GPIO a Pololu y limit-switch del eje S.

