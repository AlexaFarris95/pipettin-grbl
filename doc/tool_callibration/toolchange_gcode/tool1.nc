$1=255; Enable steppers
$H; Home the machine

; Alignment
G90 G0 Y280; Fast approach, keep full clearance.
G90 G0 Z20; Align Z to post.
G90 G0 X13; Align X to post.

; Load tool
G90 G1 Y312 F500; Slow docking move, magnets will bind.
G90 G1 Y314 F500; Slow parking probing move, tool-lock released.
G90 G0 Y256; Fast pull-off to full clearance.

; Park tool
G90 G0 Y312; Fast docking move, tool-lock engaged.
G90 G1 Y314 F500; Slow parking probing move.
G90 G1 Y305 F100; Slow magnet undocking move.
G90 G0 Y280; Fast pull-off to full clearance.

$1=10; Disable steppers
$H; Final homing move
