# Calibration procedures for CNC tools

## Toolchange calibration

### Rationale

<!-- Copied over to 02_calibration.md in the docs -->

There are 3 important measurements for each tool:

* The XYZ coordinates of an empty carriage when it is just "in front" of a parked tool.
* The XYZ of a carriage when it has docked completely with a parked tool.
* The length of a tool along the Y axis, measured from the front of the carriage.

If you measure and configure these parameters correctly, collisions should not happen.

Notes:
* The carriage's coordinates are those shown in the GUI after a succesful homing.
* For now these parameters are hardcoded here: [gcodeBuilder.py](../../protocol2gcode/gcodeBuilder.py). In the future these parameters will be set through the GUI (hopefully with the help of a wizard).
* If during calibration the tool "loading" Z coordinate is different from the tool "parking" coordinate, set this offset value in the `    z_parking_offset` slot of the configuration. If your machine is perfect, the coordinates will not differ, and the offset should be set to zero.

![drawing.svg](./drawing.svg)

## Using the GUI

### Procedure

<!-- Copied over to 02_calibration.md in the docs -->

1. Manually remove any tools, and place them in their corresponding parking posts.
2. Open the GUI.
3. Home the machine.
   - If the steppers are not enabled, open a serial port (using `minicom` for example) and:
        1. Send `$1=255` to keep the steppers on during the procedure.
        2. Send `$H` to home the machine again.
        3. Close the serial port with `Ctrl a x`, to prevent conflicts with the Python scripts.
4. To get the tool-change coordinates, use the GUI's joystick to move the machine to the following locations, and take note of the coordinates at each step (these are shown in the "tool data" element of the GUI).
    1. Align the carriage just in front of the a tool's alignment rods, but clear of them.
    2. Slide the carriage in, until the latch "clicks".
    3. Adjust the parking switch set screw such that it barely triggers at this position.
    4. Slide out the carriage, now with the tool loaded, until the alingment rods slide out of the parking post completely.
    5. Slide out the carriage, untill the Y position is such that the tool cannot crash into other tools.
    6. Slide the tool back into the parking post, until the latch "clicks" again.
    7. Slowly slide out the carriage until the magnets detach.
    8. Slide out the carriage until the alingment rods slide out of the bearings in the carriage.
6. Repeat step 4 with the other tool.
7. Update pipette definitions with the new information, provisionally hard-coded at [protocol2gcode/pipetteDriver.py](../../protocol2gcode/pipetteDriver.py).

Each set of 8 coordinates is required to define the tool-change movements for a tool.

Below you will find examples, illustrating the expected result of the calibration procedure.

### Tool 1: p200

Initial conditions:

* Coordinate space correctly configured (not the current coordinates after homing look like 0,0,258).
* Enabled steppers (send `$1=255` through `minicom`, bCNC, Arduino IDE serial monitor, etc.).

![01-manual_control.png](./images/01-manual_control.png)

> Note: the GUI's homing function will disable the steppers by default, which we want to avoid, so don't use it now.

In the following sections you will:

1. Record coordinates to dock a pipette tool.
2. Record coordinates to eject a tip.
3. Record coordinates to park a pipette tool.

#### Docking coordinates

By following the steps above, you will obtain coordinates such as these:

| Location | Tool data                                         | X  | Y   | Z  |
|----------|---------------------------------------------------|----|-----|----|
| Front    | {position:{x:42,y:278,z:39,p:null,status:"IDLE"}} | 42 | 278 | 39 |
| Click    | {position:{x:42,y:310,z:39,p:null,status:"IDLE"}} | 42 | 310 | 39 |
| Retract  | {position:{x:42,y:274,z:39,p:null,status:"IDLE"}} | 42 | 274 | 39 |

From this data, a rough version of the GCODE to load the tool to an empty carriage would be:

```
G0 G90 X42 Y275 Z39; Align with the post
G1 G90 X42 Y310 Z39 F1000; Dock the tool
G0 G90 X42 Y274 Z39; Move back to a clearance position
```


#### Tip ejection coordinates

<!-- Copied over to 02_calibration.md in the docs -->

As for the tool-change, use the GUI to move the tool and record the coordinates at each step:

1. Place a tip on the tool manually.
2. Move to a safe Z coordinate.
3. Move to a safe Y coordinate.
4. Align the X coordinate of the tip-ejection button with the tip-ejection post.
5. Align the Z coordinate of the tip-ejection button with the tip-ejection post, such that it is just beneath the post (e.g. -1 mm).
6. Align the Y coordinate of the tip-ejection button with the tip-ejection post. The buttonw should now be directly below the post.
7. Slowly move upwards to eject the tip (NOTE: should use probing here).
8. Move downwards to release the button.
9. Move away from the ejection post up to safe Y (clear of all parked tools, should be the same as in step 3).
10. Move to a safe Z coordinate (clear of all workspace objects tools, should be lower thatn in step 2, because the tip has been ejected).

By following the steps above, you will obtain coordinates such as these:

```
G0 G90 Z131; 2) go to safe Z (above all workspace objects)
G0 G90 Y170; 3) go to safe Y (clear of any parked tools)
G0 G90 X180; 4) pre-approach X position of the 
G0 G90 Z127; 5) pre-approach Z position
G0 G90 Y312; 6) approach the toolpost
G1 G90 Z135 F200; 7) slowly eject the tip
G0 G90 Z127; relax
G0 G90 Y170; move away from the ejection post up to safe Y (parked tools)
G0 G90 Z131; go to safe Z (workspace tip box)
```

These values were written down by hand, by moving the tools with the GUI and registering the machine's position at each of the key positions.

> Note: if the pipette is not correctly fastened to the adapter, you will notice at this step. As the tip ejector starts making force, the whole tool will twis if the pipette is loose. To correct this, make sure the bottom piece holding the pipette is very tight.

#### Parking coordinates

By following the steps above, you will obtain coordinates such as these:

| Location | Tool data                                         | X  | Y   | Z  |
|----------|---------------------------------------------------|----|-----|----|
| Front    | {position:{x:42,y:275,z:41,p:null,status:"IDLE"}} | 42 | 275 | 41 |
| Click    | {position:{x:42,y:310,z:41,p:null,status:"IDLE"}} | 42 | 310 | 41 |
| Retract  | {position:{x:42,y:274,z:41,p:null,status:"IDLE"}} | 42 | 274 | 41 |
From this data, a rough version of the GCODE to dock the tool:

```
G0 G90 X42 Y275 Z41; Align with the post
G1 G90 X42 Y310 Z41 F1000; Park the tool
G0 G90 X42 Y274 Z41; Move back to a clearance position
```

### Tool 2: p20

Repeat steps for tool 1.

#### Docking coordinates

By following the steps above, you will obtain coordinates such as these:

| Location | Tool data                                          | X   | Y   | Z  |
|----------|----------------------------------------------------|-----|-----|----|
| Front    | {position:{x:316,y:279,z:45,p:null,status:"IDLE"}} | 316 | 279 | 45 |
| Click    | {position:{x:316,y:312,z:45,p:null,status:"IDLE"}} | 316 | 312 | 45 |
| Retract  | {position:{x:316,y:276,z:45,p:null,status:"IDLE"}} | 316 | 276 | 45 |

From this data, a rough version of the GCODE to load the tool to an empty carriage would be:

```
G0 G90 X316 Y279 Z45
G1 G90 X316 Y312 Z45 F1000
G0 G90 X316 Y276 Z45
```


#### Tip ejection coordinates

By following the steps above, you will obtain coordinates such as these:

```
G0 G90 Y170; go to safe Y (parked tools)
G0 G90 Z115; go to safe Z (workspace tip box)
G0 G90 X180 Y170; pre-approach XY position
G0 G90 Z127; pre-approach Z position
G0 G90 Y312; approach the toolpost
G1 G90 Z147 F200; eject the tip
G0 G90 Z130; relax
G0 G90 Y170; move away from the ejection post up to safe Y (parked tools)
G0 G90 Z115; go to safe Z (workspace tip box)
```

These values were written down by hand, by moving the tools with the GUI and registering the machine's position at each of the key positions.

> Note: if the pipette is not correctly fastened to the adapter, you will notice at this step. As the tip ejector starts making force, the whole tool will twis if the pipette is loose. To correct this, make sure the bottom piece holding the pipette is very tight.

#### Parking coordinates

By following the steps above, you will obtain coordinates such as these:

| Location | Tool data                                          | X   | Y   | Z  |
|----------|----------------------------------------------------|-----|-----|----|
| Front    | {position:{x:316,y:276,z:45,p:null,status:"IDLE"}} | 316 | 276 | 45 |
| Click    | {position:{x:316,y:311,z:45,p:null,status:"IDLE"}} | 316 | 311 | 45 |
| Retract  | {position:{x:316,y:277,z:45,p:null,status:"IDLE"}} | 316 | 277 | 45 |
From this data, a rough version of the GCODE to dock the tool:

```
G0 G90 X316 Y276 Z45
G1 G90 X316 Y311 Z45 F1000
G0 G90 X316 Y277 Z45
```

### Full test macro

Prerequisites:

- Empty tool carriage.
- Tips placed on both pipettes.

Toolchange GCODE with tip ejection for each pipette:

```
$1=255
$H

; load tool 1
G0 G90 X42 Y275 Z39
G1 G90 X42 Y310 Z39 F1000
G0 G90 X42 Y274 Z39

; eject tip 1
G0 G90 Y170; go to safe Y (parked tools)
G0 G90 Z131; go to safe Z (workspace tip box)
G0 G90 X180 Y170; pre-approach XY position
G0 G90 Z127; pre-approach Z position
G0 G90 Y312; approach the toolpost
G1 G90 Z135 F200; eject the tip
G0 G90 Z127; relax
G0 G90 Y170; move away from the ejection post up to safe Y (parked tools)
G0 G90 Z131; go to safe Z (workspace tip box)

; park tool 1
G0 G90 X42 Y275 Z41
G1 G90 X42 Y310 Z41 F1000
G0 G90 X42 Y278 Z41

c; load tool 2
G0 G90 X316 Y279 Z45
G1 G90 X316 Y312 Z45 F1000
G0 G90 X316 Y276 Z45

; eject tip 2
G0 G90 Y170; go to safe Y (parked tools)
G0 G90 Z115; go to safe Z (workspace tip box)
G0 G90 X180 Y170; pre-approach XY position
G0 G90 Z130; pre-approach Z position
G0 G90 Y312; approach the toolpost
G1 G90 Z147 F200; eject the tip
G0 G90 Z130; relax
G0 G90 Y170; move away from the ejection post up to safe Y (parked tools)
G0 G90 Z115; go to safe Z (workspace tip box)

; park tool 2
G0 G90 X316 Y276 Z45
G1 G90 X316 Y311 Z45 F1000
G0 G90 X316 Y277 Z45

; back off
G0 G91 Y-10
G4 P0
$1=20
$H
```

#### Streaming

Besides streaming settings, [simple_stream.py](../../grbl/scripts/simple_stream.py) can send normal gcode programs, such as the example above, saved here: [toolchange.gcode](../../grbl/scripts/sample_gcode/toolchange.gcode).

```bash
# Note: this script requires pyserial
# Install it with "pip install pyserial", or use your system's package manager
cd grbl
python3 scripts/simple_stream.py /dev/ttyUSB0 scripts/sample_gcode/toolchange.gcode  # Or /dev/ttyACM0 
```

A few example files are saved here: [sample_gcode](../../../../../../../Mount/pipettin-grbl/grbl/scripts/sample_gcode)

* [toolchange_and_eject.gcode](../../../../../../../Mount/pipettin-grbl/grbl/scripts/sample_gcode/toolchange_and_eject.gcode)
* [toolchange.gcode](../../../../../../../Mount/pipettin-grbl/grbl/scripts/sample_gcode/toolchange.gcode)

### Python implementation (WIP)

See the [protocol2gcode/README.md](../../protocol2gcode/README.md) file.

## Using the CLI (WIP)

The [EXAMPLES.md](../../protocol2gcode/EXAMPLES.md) file shows how to interact with the machine using the python module, through SSH.

We well now use the CLI to directly send GCODE to the CNC controller, and find parking positions for each tool.

This information is derived from key parameters in the workspace (e.g. safe heights for movements) and the pipettes (tool post coordinates). For example, to get the information from a p200 pipette run:

```python
from pprint import pprint
import pipetteDriver as pd

# Get the available pipettes
pipette = pd.Pipette2(dry=True)

# Gett tool post info for the p200
current_tool = pipette.pipettes["p200"]
tool_post_coords = current_tool["tool_post"]

pprint(tool_post_coords)
```

Which outputs the coordinates shown in the previous drawing:

```json
{'x': 42,
 'y': 274,
 'y_clearance': 110,
 'y_final': 310,
 'z': 39,
 'z_parking_offset': 2}
```

A tool-change macro template can be built with commands in the "Test a tool-change macro" section, at [EXAMPLES.md](../../protocol2gcode/EXAMPLES.md). This is a possible result:

```
; START tool-change macro.  
G90 G0 Z86.0; move to the safe Z height.  
G90 G0 Y164; move to the Y coordinate clear of other parked tools.  
G90 G0 X42; move to the X position in front of the parking post.  
G90 G0 Y274 Z39; move to the Y position in front of the parking post, and align to the post's Z.  
G90 G0 Y300; insert the alignment rods.  
G38.2 Y311 F200; probe the remaining Y distance  
G38.4 Y274 F200; probe back  
G90 G0 Y274; back away from the parking area, clearing other tools  
G90 G0 Z86.0; move to the safe Z height  
G90 G0 Y164; back out of the way of other tools  
; END tool-change macro.
```

To callibrate positions manually:

1. Manually remove and park all tools.
2. Send the following commands, one by one using [simple_send.py](../../grbl/scripts/simple_send.py), `minicom` or other tools. For example: `python3 ~grbl/scripts/simple_send.py /dev/ttyACM0`.
    1. Do not send the commands as they appear below, instead modify them to approach the toolpost slowly.
    2. You may use `G91 G0` incremental moves as a pseudo-joystick.

```gcode
$1=255; Enable steppers
$H; Home the machine

; Alignment
G90 G0 Y280; Fast approach, keep full clearance.
G90 G0 Z20; Align Z to post.
G90 G0 X13; Align X to post.

; Load tool
G90 G1 Y312 F500; Slow docking move, magnets will bind.
G90 G1 Y314 F500; Slow parking probing move, tool-lock released.
G90 G0 Y256; Fast pull-off to full clearance.

; Park tool
G90 G0 Y312; Fast docking move, tool-lock not engaged.
G90 G1 Y314 F500; Slow parking probing move, tool-lock engaged.
G90 G1 Y305 F100; Slow magnet undocking move.
G90 G0 Y280; Fast pull-off to full clearance.

$1=10; Disable steppers
$H; Final homing move

```

1. Tool 1, front-park, free, park-clear: `WPos:13.000,282.000,20.000` (tools parked, empty head).
2. Tool 1, front-park, docked: `WPos:13.000,312.000,20.000` (tools parked, empty head).
3. Tool 1, front-park, docked, release click: `WPos:13.000,314.000,20.000` (tools parked, empty head).
4. Tool 1, front-park, docked, park-clear: `WPos:13.000,256.000,20.000` (tools parked, empty head).
5. Tool 1, front-park, docked, lock click: `WPos:13.000,314.000,20.000` (tools parked, empty head).
6. Tool 1, front-park, free, park-clear: `WPos:13.000,282.000,20.000` (tools parked, empty head).
    - Bug: acá perdió pasos, probablemente fue demasiado rápido.

## Troubleshooting

### Lost steps while undocking

1. Adjust GT2 belt tension. The skipping might be actually happening at the stepper pulleys, not _in_ the stepper.
2. Adjust Vref to increase current, and thus torque.
3. Remove a magnet from the toolchanger head. This is simpler and will affect all tools.

# Pipette homing calibration

<!-- Copied over to 02_calibration.md in the docs -->

## Rationale

The initial state of a pipette is assumed to be at the first stop (when it's shaft is fully depressed, but not further than the start of the second stop).

From this position the pipette is ready to load volume.

What we need to find is the distance that the carriage must travel from the homing sensor, to reach the second stop (but not further).

## Procedure

You may use the examples in [protocol2gcode/EXAMPLES.md](../../protocol2gcode/EXAMPLES.md) for controlling the pipettes directly.

First, `ssh` into the Raspberry Pi, `cd` to the `protocol2gcode` directory, and open a `python3` console:

```bash
ssh pi@YOUR_PIs_ADDRESS # Pipettin Pi 2
cd pipettin-grbl/protocol2gcode/  
python3
```

Then run the example in the "Multi-pipette machine" section, which is copied here for convenience.

> Note: before doing this, make sure that your limit switches and stepper motors are correctly wired and configured. Otherwise things might crash into each other and some parts may be damaged.

First a simple homing test, for both pipettes:

```python
import pipetteDriver as pd

# Tune a few things, for this testing example.
pipette = pd.Pipette2(verbose=True)

# Select a p20 pipette from the builtin pipette models.
pipette.configure_pipette("p20")

# Home the pipette tool
pipette.home_pipette()

# Displace the pipette's shaft by "-2".
pipette.displace(-2, max_speed=1)

# Select a p200 pipette from the builtin pipette models.
pipette.configure_pipette("p200")

# Home the axis
pipette.home_pipette(max_speed=2, retraction_displacement=1)

# Displace the pipette's shaft by "-2".
pipette.displace(-2, max_speed=1)

# Cleanup please
pipette.close()
```

If everyng worked out, now it's time to tune the homing position, using the `retraction_displacement` argument to `home_pipette`. This parameter defines how far the motor will travel downwards after hitting the limit switch on top.

Start out with one pipette, and run:

```python
import pipetteDriver as pd

# Tune a few things, for this testing example.
pipette = pd.Pipette2(verbose=True)

# Select a p20 pipette from the builtin pipette models.
pipette.configure_pipette("p200")

# Reduce the maximum speed if you are scared
# pipette.max_speed=8

# Home the pipette tool
pipette.home_pipette(retraction_displacement=0)
```

Now:

1. Increase the value on `retraction_displacement` by some amount.
    - For example `pipette.home_pipette(retraction_displacement=2)`
    - Note: you can try with larger steps if you've already seen what "2" looks like and you are confident.
3. Re-run the `home_pipette` again with each new value.
4. Check if the pipette's shaft has reached the first stop.
    - The carriage will squeeze your fingers if you place them on top of the pipette's button. Try grabbing it from the sides, either to hold it down, or to check if there is still some distance before the first stop (after homing of course).
5. Repeat from step 1 until the pipette's shaft has reached the first stop after homing.
    - My final value was `22.9` mm for the p20 pipette tool.
    - My final value was `22.9` mm for the p20 pipette tool.
6. Change the pipette, and repeat all steps (for each tool you have).
    - In my case, I switched to the next tool with `pipette.configure_pipette("p20")`.

# Workspace and platform offsets

## Tool XY offset calibration

First, you must calibrate the position of items with respect to the a "first" tool, to which you will assign zero offset.

Other tools will have offsets with respect to that one.

1. Manually load the reference tool (e.g. p200) and home the machine.
2. Follow the instructions at [doc/platforms/README.md](../platforms/README.md) to callibrate XYZ coordinates for platforms, relative to the first tool.
3. Follow the "Offset calculation procedure" section to obtain XY offsets for the second tool.
4. Follow the instructions at [doc/platforms/README.md](../platforms/README.md) again, to callibrate Z coordinates for platforms, relative to the **second** tool.
5. Update platform and pipette definitions with the new information.

### Offset calculation procedure

All you need is to:

1. Place reference object on the workspace, which you can align precisely to the pipette's point (or tip if placed).
2. Align the first tool to the object using the GUI's joystick.
3. Record the XY coordinates.
4. Remove the first tool and place the second tool (manually).
5. Align the second tool to the object using the GUI's joystick.
6. Record the XY coordinates.
7. Calculate the difference between the first and the second set of coordinates, to obtain the second tool's offset.

- [ ] TODO: the Z coordinate offset will be discussed later. Each platform will have a specific "default load bottom" parameter, associated with each pipette. Also the tip lengths will have to be measured. See the next section.

Use the recorded coordinates to calculate the offsets of the second tool (p20) using the first tool (p200) as a reference:

```python
cal = {"p200": {"x": 168.875,"y": 73.875,"z": 25.91}, 
       "p20": {"x": 169.875,"y": 80.875,"z": 50.98}}

offset_x = cal["p20"]["x"] - cal["p200"]["x"]
offset_y = cal["p20"]["y"] - cal["p200"]["y"]

print("x: ", offset_x)
print("y: ", offset_y)
```

> Note that we subtracted from the second set of coordinates. This is because the GCODE is built for the coordinates of the first tool, and the offsets are added just before saving each command.

## Platform Z-offsets

Each platform has a top and bottom attribute, which we must calibrate for each pipette.

### Editing platforms for multi-tool support

- [ ] TO-DO: put this somewhere else, more approrpiate.

Keep in mind that the only platforms that are currently correcly configured for the new multi-pipette software are:

* `5x16_1.5_rack`
* `200ul_tip_rack_MULTITOOL`

The rest will probably fail, because they lack the necessary attributes.

It's a matter of updating the rest to change some parameters so that they have a value for each pipette.

In a tip rack we need something like:

```json
    "defaultLoadBottom": {
        "p200": 25,
        "p20": 4
    },
```

Ina tube rack we need something like:

```json
    "defaultBottomPositionTools": {
        "p200": 29,
        "p20": 9
    }
```

# Edit pipette definitions

For now this is hard-coded at [pipetteDriver.py](../../protocol2gcode/pipetteDriver.py).

We are slowly moving to a GUI-accessible definition, see:

- https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/87
- https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/88


