# Pipetting GRBL firmware

GRBL is a firmware for the Arduino UNO (and others) that reads GCODE from the serial interface and controls the drivers for stepper motors on a CNC machine.

Summary:

* Vanilla GRBL 1.1h should work fine unless you need to control a servo (which used to be needed to actuate the tip ejector on the micropipette).
* Positive workspace coordinates are used.
* The square gantry feature is enabled on the Y axis.
* Hard-limits may be disabled (due to noisy wiring).
* Soft-limits are enabled. 

Page contents:

[[_TOC_]]

## Usage

Import this directory as a library in the Arduino IDE. Use the "grbl upload" example to load GRBL onto your Arduino UNO (with a CNC shield v3 on top).

Note: If you have already [installed the 10uF capacitor](../doc/electronica/README.md) across the reset/abort pins (to prevent a GRBL reset on serial reconnection), you may need to remove it temporarily before uploading the frimware (or you may get some errors while doing so).

## Testing

You may interact with GRBL from a linux terminal using `minicom`. For example:

```bash
minicom -D /dev/ttyACM0 -b 115200
```

Try typing `?` to check GRBL's status, or `$$` and pressing enter to see its settings.

The python scripts offer a rudimentary interactive text-based interface, using the `--interactive` option. That one wraps the serial interface, and controls other parts of the machine (i.e. the tool/pipette axis).

## Firmware modifications

All major modifications are on the hardware side, specially on the Arduino CNC Shield 3.0 clone.

Summary:

* Enable dual axis homing on the Y axis.
* Enable `HOMING_FORCE_SET_ORIGIN`: to put the machine in positive space, see the [README.md](../doc/grbl/README.md) under [doc/grbl/](../doc/grbl).

### Disable variable spindle

This feature is incompatible with the CNC shield clone, based on Protoneer's CNC shield v3.0.

It is also incompatible with the Dual-axis homing feature (enabled below) on the CNC shield clone.

To disable variable spindle, comment out line 339 at [config.h](./grbl/config.h):

```c
// #define VARIABLE_SPINDLE // Default enabled. Comment to disable.
```

> If you do not disable this feature, the Z-axis limit switch pin and VS pin will be swapped, and things will go badly.

### Dual Y axis Gantry squaring

If you have dual X or Y axis (ie, two independent steppers and threaded rods for one direction of movement) enable gantry squaring with `ENABLE_DUAL_AXIS`. Follow the official GBRL documentation for correct configuration and limit switch setup.

```c
#define DUAL_AXIS_SELECT  Y_AXIS  // Must be either X_AXIS or Y_AXIS
```
> Line 644 of `grbl/config.h`.

```c
#define DUAL_AXIS_CONFIG_CNC_SHIELD_CLONE  // Uncomment to select. Comment other configs.
```
> Line 644 of `grbl/config.h`.

This is enabled in our build. Our A axis clones the Y axis.


### Servo control via Variable Spnidle PWM (DEPRECATED)

If using a servo motor on the spindle enable pin, enable variable spindle with:

```c
#define VARIABLE_SPINDLE
```
> Line 339 of `grbl/config.h`.

**IMPORTANT**: `ENABLE_DUAL_AXIS` and `VARIABLE_SPINDLE` are incompatible with the V3 CNC shield (though in theory its not impossible to enable them simultaneously).

The spindle PWM frequency is altered in `cpu_map.h` to be compatible with a hobby servo. Using a 1024 prescaler in fast-mode PWM we get `~60 Hz` which worsk great for ejecting tips.

Note: we tried to use this for the pipette axis. But the servo was not precise enough, and had a "bounce" effect which cannot happen during pipetting.

Ver wiki:

  * https://wiki.frubox.org/proyectos/diy/cnc#controlar-un-servo-con-el-spindle

## Configuration

### Settings

Official info on GRBL settings can be found [here](https://github.com/gnea/grbl/blob/master/doc/markdown/settings.md), and our configuration is saved in [grbl_settings.txt](./grbl_settings.txt).

Summary:

* The full configuration for our setup makes GRBL work in positive space coordinates with origin at X0 Y0 by setting the masks correctly, **and** by issuing a corresponding G92 command after homing (such as `G92 X-115 Y-10 Z30.5`).
* We also needed to reverse the Y axis and the XT homing direction. This is because the web-based GUI uses that coordinate system (right and down are positive the directions) and because reversing the homing direction will leave the machine at this origin (it otherwise homes to max X/Y instead of min X/Y).
* The settings for "setup" are in the [grbl_setup_settings.txt](./grbl_setup_settings.txt) file (i.e. to test motor and endstop connections). Remember to write back the settings in [grbl_settings.txt](./grbl_settings.txt) when done.

### Write configuration

How to send settings:

* One at a time, through a serial interphase: Arduino IDE, minicom, bCNC, etc.
* In bulk: bCNC, Universal GCODE Sender (UGS), the simple Python script: [simple_stream.py](./scripts/simple_stream.py), or others.
* A mode detailed guide: https://all3dp.com/2/gbrl-settings-gbrl-configuration/

> An example is provided for the script alternative. bCNC can be used to configure GRBL using a GUI, one setting at a time. We recommend the latter for adjustments and tests, and the script for initial configuration.

The configuration script will:

* Stream `grbl_settings.txt` to a serial device at `/dev/ttyUSB0`.
* Wait for a response to each setting, and print it to console.

Open a commandline on the repo's root directory and run:

```bash
# Note: this script requires pyserial
# Install it with "pip install pyserial", or use your system's package manager
cd grbl
python3 scripts/simple_stream.py /dev/ttyUSB0 grbl_setup_settings.txt  # Or /dev/ttyACM0 
```

> Check for error messages just in case.

Notes:

* I advise not using `$1=255` as it does not seem to play nice with our testing hardware (A4988 drivers). The current python code sets this value during a protocol, and resets it to a small value when idle.

### Coordinate system

Given the following machine limits of:

```
$130=270.000  
$131=390.000  
$132=152.000
```

And a homing pull-off of `$27=2.000` mm.

We set the coordinate system with G10 L2 like so:

```
G10 L2 P0 X-268 Y-388 Z-150
```

This makes the origin sit at the lowermost Z, the leftmost X, and the back Y, as shown here: [sistema_de_coordenadas_final.svg](../doc/grbl/sistema_de_coordenadas_final.svg). Learn more at [doc/grbl/README.md](../doc/grbl/README.md).

The pull-off distance must be subtracted from the limits. Otherwise the machine might move to the 0,0 coordinate, and trigger the endstops.


## Wiring

The project's electronics are described in the [doc/electronica/README.md](../doc/electronica/README.md) file.

### Jumpers

Leer: https://www.zyltech.com/arduino-cnc-shield-instructions/

1. Setup microstepping: place two jumpers over each pair of M1 and M0 pins (on all four motor sockets).
2. Setup independent A axis: plate two jumpers over D13/Astep and D12/Adir.

### Vref adjustment

Leer:

* https://all3dp.com/2/vref-calculator-tmc2209-tmc2208-a4988/
* https://www.zyltech.com/arduino-cnc-shield-instructions/

El voltaje deseado se calcula con la formula: `VREF = Imot x 8 x Rsense`.

Los datos necesarios son:

* Corriente que queremos pasar por nuestros motores (xej: `0.5 A`).
* Resistencia de sensado, que varía entre proveedores y debe medirse (`R100` = `0.1 Ohm` en mi caso).

De esa manera `VREF = 0.5 x 8 x 0.1 = 0.4 V`.

Para medir el Vref a mi me resultó necesario conectar el Arduino por USB además de conectar el CNC shield a 24V. A [otras personas](https://foro.metalaficion.com/index.php?topic=32855.0) también le sucedió que medían 0V si no lo conectaban por USB también.

Para que el eje Y/A no perdiera pasos al cambiar de herramienta, fui subiendo el Vref hasta `0.9V` aproximadamente.

### Motor wiring and testing

For testing:

1. Place all molex/dupont/2.54mm connectors of the stepper cables on the CNC shield.
2. Connect one motor to the Z axis cable.
3. Connect the 24V power, and plug the Arduino to a PC.
4. Send a `$J=G91 X10 Y10 Z10 F500` jogging commend through the Arduino IDE (or other serial interface). The motor should rotate briefly and stop.
5. Disconnect the 24V power.
6. For each of the other 3 motors, repeat steps 2 through 5.

Notes:

* Remember to use the settings in [grbl_setup_settings.txt](./grbl_setup_settings.txt) to avoid issues with soft or hard limits (i.e. disable soft and hard limits).
* The motors can be connected in two ways to the CNC shield, such that a motor will spin in the other direction when the connector is flipped.
* The "A" axis motor may not respond to a software inversion of the direction pin. If ever [invert the Y axis direction from the GRBL](https://github.com/gnea/grbl/wiki/Grbl-v1.1-Configuration#3--direction-port-invert-mask) settings, you _must_ flip the A-axis motor connector on the CNC shield manually.

### Limit-switch wiring

The Z and Y limit switches must be wired in parallel to make the double Y axis homing work properly.

The NC pin on the switch **must** be severed, or you will fry your Arduino or Raspberry Pi.

Learn more here: [doc/electronica/README.md](../doc/electronica/README.md)

### Limit-switch and Probe tests

Open a commandline on the repo's root directory and run:

```bash
# Note: this script requires pyserial
# Install it with "pip install pyserial", or use your system's package manager
cd grbl
python3 scripts/simple_status.py /dev/ttyUSB0
```

> *Note*: You may press `Ctrl+C` to exit the script cleanly.

This script will continuously request status reports to GRBL. It's output will be useful to test limit-switches, probes, etc.

> *Note*: the script first sends a `$X` unlocing command, so any `Alarm` state may be cleared without notice.

Connect your limit switches and test each one by pressing the button manually. If succesful, the status report will change to a message similar to the following:

```
<Idle|WPos:0.000,0.000,0.000|F:0|Pn:PYZ>
```

> Notice the `Pn:PYZ` charachters at the end, which indicate that the limit switch of the `YZ` axis was triggered (otherwise only `Pn:P` may be shown).

## Workspace coordinates

We require the machine to operate in positive space.

Go over the [README.md](../doc/grbl/README.md) at [doc/grbl](../doc/grbl) to learn how to setup the coordinates correctly.

We recommend doing this one you've built the machine.

# Streaming GCODE

Besides streaming settings, [`simple_stream.py`](./scripts/simple_stream.py) can send normal gcode programs, such as the examples here [sample_gcode](./scripts/sample_gcode).

```bash
# Note: this script requires pyserial
# Install it with "pip install pyserial", or use your system's package manager
cd grbl
python3 scripts/simple_stream.py /dev/ttyUSB0 scripts/sample_gcode/toolchange.gcode  # Or /dev/ttyACM0 
```

The examples there were made by following the toolchange calibration procedure descibed here: [README.md](../doc/tool_callibration/README.md) (at [doc/tool_callibration](../doc/tool_callibration)).

# Jogging in GRBL

We use "`$J=`" commands in several places.

To understand how Jogging commands work please have a look at:

  * https://github.com/gnea/grbl/wiki/Grbl-v1.1-Jogging
  * https://github.com/vlachoudis/bCNC/blob/master/bCNC/controllers/GRBL1.py

# [GRBL](https://github.com/gnea/grbl)

> Contents of the original README file from the GRBL project follow.

![GitHub Logo](https://github.com/gnea/gnea-Media/blob/master/Grbl%20Logo/Grbl%20Logo%20250px.png?raw=true)

***
_Click the `Release` tab to download pre-compiled `.hex` files or just [click here](https://github.com/gnea/grbl/releases)_
***
Grbl is a no-compromise, high performance, low cost alternative to parallel-port-based motion control for CNC milling. This version of Grbl runs on an Arduino with a 328p processor (Uno, Duemilanove, Nano, Micro, etc).

The controller is written in highly optimized C utilizing every clever feature of the AVR-chips to achieve precise timing and asynchronous operation. It is able to maintain up to 30kHz of stable, jitter free control pulses.

It accepts standards-compliant g-code and has been tested with the output of several CAM tools with no problems. Arcs, circles and helical motion are fully supported, as well as, all other primary g-code commands. Macro functions, variables, and most canned cycles are not supported, but we think GUIs can do a much better job at translating them into straight g-code anyhow.

Grbl includes full acceleration management with look ahead. That means the controller will look up to 16 motions into the future and plan its velocities ahead to deliver smooth acceleration and jerk-free cornering.

* [Licensing](https://github.com/gnea/grbl/wiki/Licensing): Grbl is free software, released under the GPLv3 license.

* For more information and help, check out our **[Wiki pages!](https://github.com/gnea/grbl/wiki)** If you find that the information is out-dated, please to help us keep it updated by editing it or notifying our community! Thanks!

* Lead Developer: Sungeun "Sonny" Jeon, Ph.D. (USA) aka @chamnit

* Built on the wonderful Grbl v0.6 (2011) firmware written by Simen Svale Skogsrud (Norway).

***

## Official Supporters of the Grbl CNC Project
![Official Supporters](https://github.com/gnea/gnea-Media/blob/master/Contributors.png?raw=true)


***

## Update Summary for v1.1
- **IMPORTANT:** Your EEPROM will be wiped and restored with new settings. This is due to the addition of two new spindle speed '$' settings.

- **Real-time Overrides** : Alters the machine running state immediately with feed, rapid, spindle speed, spindle stop, and coolant toggle controls. This awesome new feature is common only on industrial machines, often used to optimize speeds and feeds while a job is running. Most hobby CNC's try to mimic this behavior, but usually have large amounts of lag. Grbl executes overrides in realtime and within tens of milliseconds.

- **Jogging Mode** : The new jogging commands are independent of the g-code parser, so that the parser state doesn't get altered and cause a potential crash if not restored properly. Documentation is included on how this works and how it can be used to control your machine via a joystick or rotary dial with a low-latency, satisfying response.

- **Laser Mode** : The new "laser" mode will cause Grbl to move continuously through consecutive G1, G2, and G3 commands with spindle speed changes. When "laser" mode is disabled, Grbl will instead come to a stop to ensure a spindle comes up to speed properly. Spindle speed overrides also work with laser mode so you can tweak the laser power, if you need to during the job. Switch between "laser" mode and "normal" mode via a `$` setting.

	- **Dynamic Laser Power Scaling with Speed** : If your machine has low accelerations, Grbl will automagically scale the laser power based on how fast Grbl is traveling, so you won't have burnt corners when your CNC has to make a turn! Enabled by the `M4` spindle CCW command when laser mode is enabled!

- **Sleep Mode** : Grbl may now be put to "sleep" via a `$SLP` command. This will disable everything, including the stepper drivers. Nice to have when you are leaving your machine unattended and want to power down everything automatically. Only a reset exits the sleep state.

- **Significant Interface Improvements**: Tweaked to increase overall performance, include lots more real-time data, and to simplify maintaining and writing GUIs. Based on direct feedback from multiple GUI developers and bench performance testing. _NOTE: GUIs need to specifically update their code to be compatible with v1.1 and later._

	- **New Status Reports**: To account for the additional override data, status reports have been tweaked to cram more data into it, while still being smaller than before. Documentation is included, outlining how it has been changed. 
	- **Improved Error/Alarm Feedback** : All Grbl error and alarm messages have been changed to providing a code. Each code is associated with a specific problem, so users will know exactly what is wrong without having to guess. Documentation and an easy to parse CSV is included in the repo.
	- **Extended-ASCII realtime commands** : All overrides and future real-time commands are defined in the extended-ASCII character space. Unfortunately not easily type-able on a keyboard, but helps prevent accidental commands from a g-code file having these characters and gives lots of space for future expansion.
	- **Message Prefixes** : Every message type from Grbl has a unique prefix to help GUIs immediately determine what the message is and parse it accordingly without having to know context. The prior interface had several instances of GUIs having to figure out the meaning of a message, which made everything more complicated than it needed to be.

- New OEM specific features, such as safety door parking, single configuration file build option, EEPROM restrictions and restoring controls, and storing product data information.
 
- New safety door parking motion as a compile-option. Grbl will retract, disable the spindle/coolant, and park near Z max. When resumed, it will perform these task in reverse order and continue the program. Highly configurable, even to add more than one parking motion. See config.h for details.

- New '$' Grbl settings for max and min spindle rpm. Allows for tweaking the PWM output to more closely match true spindle rpm. When max rpm is set to zero or less than min rpm, the PWM pin D11 will act like a simple enable on/off output.

- Updated G28 and G30 behavior from NIST to LinuxCNC g-code description. In short, if a intermediate motion is specified, only the axes specified will move to the stored coordinates, not all axes as before.

- Lots of minor bug fixes and refactoring to make the code more efficient and flexible.

- **NOTE:** Arduino Mega2560 support has been moved to an active, official Grbl-Mega [project](http://www.github.com/gnea/grbl-Mega/). All new developments here and there will be synced when it makes sense to.


```
List of Supported G-Codes in Grbl v1.1:
  - Non-Modal Commands: G4, G10L2, G10L20, G28, G30, G28.1, G30.1, G53, G92, G92.1
  - Motion Modes: G0, G1, G2, G3, G38.2, G38.3, G38.4, G38.5, G80
  - Feed Rate Modes: G93, G94
  - Unit Modes: G20, G21
  - Distance Modes: G90, G91
  - Arc IJK Distance Modes: G91.1
  - Plane Select Modes: G17, G18, G19
  - Tool Length Offset Modes: G43.1, G49
  - Cutter Compensation Modes: G40
  - Coordinate System Modes: G54, G55, G56, G57, G58, G59
  - Control Modes: G61
  - Program Flow: M0, M1, M2, M30*
  - Coolant Control: M7*, M8, M9
  - Spindle Control: M3, M4, M5
  - Valid Non-Command Words: F, I, J, K, L, N, P, R, S, T, X, Y, Z
```

-------------
Grbl is an open-source project and fueled by the free-time of our intrepid administrators and altruistic users. If you'd like to donate, all proceeds will be used to help fund supporting hardware and testing equipment. Thank you!

[![Donate](https://www.paypalobjects.com/en_US/i/btn/btn_donate_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=CUGXJHXA36BYW)
