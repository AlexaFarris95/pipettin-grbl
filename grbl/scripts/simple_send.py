#!/usr/bin/env python
"""\
Simple g-code streaming script for grbl

Provided as an illustration of the basic communication interface
for grbl. When grbl has finished parsing the g-code block, it will
return an 'ok' or 'error' response. When the planner buffer is full,
grbl will not send a response until the planner buffer clears space.

G02/03 arcs are special exceptions, where they inject short line 
segments directly into the planner. So there may not be a response 
from grbl for the duration of the arc.

---------------------
The MIT License (MIT)

Copyright (c) 2012 Sungeun K. Jeon

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
---------------------
"""

import serial
import time
import sys

# Open grbl serial port
s = serial.Serial(sys.argv[1],115200)

# Wake up grbl
s.write("\r\n\r\n".encode())
time.sleep(2)   # Wait for grbl to initialize 
s.flushInput()  # Flush startup text in serial input

print("\n\nType a command and press <Enter> to send, <Ctrl+C> to cancel, and <q+Enter> to Quit. A 1mm pseudo-joystick is available with the <[wasdrf]+Enter> keys.")

# Stream g-code to grbl
goon = True
while goon:
    try:
        command = input("\n\nType a command and press <Enter> to send, <Ctrl+C> to cancel, and <q+Enter> to Quit: ")
        # Joystick
        if command is "w":
            command = "G91 G0 Y1"
        elif command is "s":
            command = "G91 G0 Y-1"
        elif command is "a":
            command = "G91 G0 X-1"
        elif command is "d":
            command = "G91 G0 X1"
        elif command is "r":
            command = "G91 G0 Z1"
        elif command is "f":
            command = "G91 G0 Z-1"
        # Quit or send
        if command is "q":
            goon = False
        else:
            l = command .strip() + '\n' # Strip all EOL characters for consistency
            print('Sending: ' + command .strip())
            s.write(l.encode()) # Send g-code block to grbl
            grbl_out = s.readline().decode() # Wait for grbl response with carriage return
            print('Response received: ' + grbl_out.strip())
        # Request status
        print('Sending: ?\n')
        s.write('?'.encode()) # Send g-code block to grbl
        grbl_out = s.readline().decode() # Wait for grbl response with carriage return
        print('Response received: ' + grbl_out.strip())
    except KeyboardInterrupt:
        # https://stackoverflow.com/a/13181036
        pass

# Close file and serial port
s.close()

