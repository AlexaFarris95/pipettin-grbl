$1=255
$H

; load tool 1
G0 G90 X42 Y275 Z39
G1 G90 X42 Y310 Z39 F1000
G0 G90 X42 Y274 Z39

; eject tip 1
G0 G90 Y170; go to safe Y (parked tools)
G0 G90 Z131; go to safe Z (workspace tip box)
G0 G90 X180 Y170; pre-approach XY position
G0 G90 Z127; pre-approach Z position
G0 G90 Y312; approach the toolpost
G1 G90 Z135 F200; eject the tip
G0 G90 Z127; relax
G0 G90 Y170; move away from the ejection post up to safe Y (parked tools)
G0 G90 Z131; go to safe Z (workspace tip box)

; park tool 1
G0 G90 X42 Y275 Z41
G1 G90 X42 Y310 Z41 F1000
G0 G90 X42 Y278 Z41

c; load tool 2
G0 G90 X316 Y279 Z45
G1 G90 X316 Y312 Z45 F1000
G0 G90 X316 Y276 Z45

; eject tip 2
G0 G90 Y170; go to safe Y (parked tools)
G0 G90 Z115; go to safe Z (workspace tip box)
G0 G90 X180 Y170; pre-approach XY position
G0 G90 Z127; pre-approach Z position
G0 G90 Y312; approach the toolpost
G1 G90 Z147 F200; eject the tip
G0 G90 Z130; relax
G0 G90 Y170; move away from the ejection post up to safe Y (parked tools)
G0 G90 Z115; go to safe Z (workspace tip box)

; park tool 2
G0 G90 X316 Y276 Z45
G1 G90 X316 Y311 Z45 F1000
G0 G90 X316 Y277 Z45

; back off
G0 G91 Y-10
G4 P0
$1=20
$H
