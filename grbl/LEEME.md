# Pipetting GRBL firmware

GRBL is a firmware for the Arduino UNO (and others) that reads GCODE from the serial interface and controls the drivers for stepper motors on a CNC machine.

Summary:

* Vanilla GRBL 1.1h should work fine unless you need to control a servo (which used to be needed to actuate the tip ejector on the micropipette).
* Positive workspace coordinates are used.
* The square gantry feature is enabled on the Y axis.
* Hard-limits may be disabled (due to noisy wiring).
* Soft-limits are enabled. 

## Usage

Import this directory as a library in the Arduino IDE. Use the "grbl upload" example to load GRBL onto your Arduino UNO (with a CNC shield v3 on top).

Note: If you have already installed the 10uF capacitor across the reset/abort pins (to prevent reset on serial reconnect), you may need to remove it temporarily before uploading the frimware (or you may get some errors while doing so).

## Testing

You may interact with GRBL from a linux terminal using `minicom`. For example:

```bash
minicom -D /dev/ttyACM0 -b 115200
```

Try typing `?` to check GRBL's status, or `$$` and pressing enter to see its settings.

The python scripts offer a rudimentary interactive text-based interface, using the `--interactive` option. That one wraps the serial interface, and controls other parts of the machine (i.e. the tool/pipette axis).

## Firmware modifications

All major modifications are on the hardware side, specially on the Arduino CNC Shield 3.0 clone.

Summary:

* Enable dual axis homing on the Y axis.

### Servo control via Variable Spnidle PWM (DEPRECATED)

If using a servo motor on the spindle enable pin, enable variable spindle with:

```c
#define VARIABLE_SPINDLE
```
> Line 339 of `grbl/config.h`.

**IMPORTANT**: `ENABLE_DUAL_AXIS` and `VARIABLE_SPINDLE` are incompatible with the V3 CNC shield (though in theory its not impossible to enable them simultaneously).

The spindle PWM frequency is altered in `cpu_map.h` to be compatible with a hobby servo. Using a 1024 prescaler in fast-mode PWM we get `~60 Hz` which worsk great for ejecting tips.

Note: we tried to use this for the pipette axis. But the servo was not precise enough, and had a "bounce" effect which cannot happen during pipetting.

Ver wiki:

  * https://wiki.frubox.org/proyectos/diy/cnc#controlar-un-servo-con-el-spindle

### Dual Y axis Gantry squaring

If you have dual X or Y axis (ie, two independent steppers and threaded rods for one direction of movement) enable gantry squaring with `ENABLE_DUAL_AXIS`. Follow the official GBRL documentation for correct configuration and limit switch setup.

```c
#define DUAL_AXIS_SELECT  Y_AXIS  // Must be either X_AXIS or Y_AXIS
```
> Line 644 of `grbl/config.h`.

```c
#define DUAL_AXIS_CONFIG_CNC_SHIELD_CLONE  // Uncomment to select. Comment other configs.
```
> Line 644 of `grbl/config.h`.

This is enabled in our build. Our A axis clones the Y axis.

## Configuration

### How to send configuration commands

How to send settings:

* One at a time: Arduino IDE, minicom, bCNC, etc.
* In bulk: Universal GCODE Sender (UGS), the simple Python script: [simple_stream.py](./scripts/simple_stream.py), or others.
* A mode detailed guide: https://all3dp.com/2/gbrl-settings-gbrl-configuration/

An example is provided for the script alternative. The script will:

* Stream `grbl_settings.txt` to a serial device at `/dev/ttyUSB0`.
* Wait for a response to each setting, and print it to console.

Open a commandline on the repo's root directory and run:

```bash
cd grbl
python3 scripts/simple_stream.py
```

Check for error messages just in case.

### GRBL Settings

GRBL settings are described [here](https://github.com/gnea/grbl/blob/master/doc/markdown/settings.md), and our configuration is saved in [grbl_settings.txt](./grbl_settings.txt).

Summary:

* The full configuration for our setup makes GRBL work in positive space coordinates with origin at X0 Y0 by setting the masks correctly and issuing the corresponding G92 command after homing.
* We also needed to reverse the Y axis and the XT homing direction. This is because the web-based GUI uses that coordinate system (right and down are positive the directions) and because reversing the homing direction will leave the machine at this origin (it otherwise homes to max X/Y instead of min X/Y).

### Notes

I advise not using `$1=255` as it does not seem to play nice with our testing hardware (A4988 drivers). The current python code sets this value during a protocol, and resets it to a small value when idle.


