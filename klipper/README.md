# Klipper setup for the Pipetting Bot

Work in progress!

Printer config file: [printer.cfg](./printer.cfg)

GitLab Issues:

* [# 47 - Replace GRBL with Klipper](https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/47)
* [# 17 - Firmware projects list](https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/17)

The objective is to use this setup:

![setup.png](./images/setup.png)

# [GCODE](https://www.klipper3d.org/G-Codes.html)

## Status

* `STATUS`

```
Send: STATUS
Recv: // Klipper state: Ready
```

* `QUERY_ENDSTOPS`

```
Send: QUERY_ENDSTOPS
Recv: manual_stepper p20:TRIGGERED manual_stepper p200:TRIGGERED x:TRIGGERED y:TRIGGERED y1:TRIGGERED z:TRIGGERED
```

* `GET_POSITION`

```
Send: GET_POSITION
Recv: // mcu: stepper_x:0 stepper_y:0 stepper_y1:0 stepper_z:0
Recv: // stepper: stepper_x:0.000000 stepper_y:0.000000 stepper_y1:0.000000 stepper_z:0.000000
Recv: // kinematic: X:0.000000 Y:0.000000 Z:0.000000
Recv: // toolhead: X:0.000000 Y:0.000000 Z:0.000000 E:0.000000
Recv: // gcode: X:0.000000 Y:0.000000 Z:0.000000 E:0.000000
Recv: // gcode base: X:0.000000 Y:0.000000 Z:0.000000 E:0.000000
Recv: // gcode homing: X:0.000000 Y:0.000000 Z:0.000000
Recv: ok
```

Enable/disable stepper x:

* `SET_STEPPER_ENABLE STEPPER=stepper_x ENABLE=1`
* `SET_STEPPER_ENABLE STEPPER=stepper_x ENABLE=0`

Home X axis:

* `G28 X`

Manual stepper commands:

* `MANUAL_STEPPER STEPPER=p20 SET_POSITION=0`
* `MANUAL_STEPPER STEPPER=p20 ENABLE=1`
* `MANUAL_STEPPER STEPPER=p20 MOVE=20`

# Klipper mods

## Extruder homing

See: [homingextruder.py](./code/homingextruder.py)

