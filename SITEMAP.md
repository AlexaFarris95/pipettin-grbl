# Site map

Get your bearigs: learn how to navigate the repo here.

## Repository structure

Each subdirectory groups different kinds of source material. In each one, you'll find a `README.md` explaining its contents, and linking to other parts of the repo.

If you are looking for assembly or usage documentation, find it in the documentation repository [here](https://gitlab.com/pipettin-bot/pipettin-grbl-docs).

## [Docs and Guides](./GUIDES.md)

Links to guides and documentation are available at: [GUIDES.md](./GUIDES.md)

## Top-level contents

[[_TOC_]]

---

### Protocol designer: [`gui`](./gui)

Source code for the Node.js GUI, written in pure JavaScript.

![Screenshot_20220721_121830.png](./doc/gui/Screenshot_20220721_121830.png)

### CAD files: [`models`](./models)

Source model files for the machine's parts, modeled mostly with [FreeCAD](https://www.freecadweb.org/).

![assembly3_tooclhanger.png](./models/modelos_XYZS/images/assembly3_tooclhanger.png)

### Python module: [`protocol2gcode`](./protocol2gcode)

Source files for the main python module, responsible for:

- Parsing "high-level" protocols from the GUI into GCODE  (with a few "extensions").
- Operates the hardware, by sending GCODE to GRBL and directly controling the GPIO through [RPi.GPIO](https://sourceforge.net/projects/raspberry-gpio-python/) and [pigpio](https://abyz.me.uk/rpi/pigpio/).

Overall information flow diagram:

![drawing.svg](./protocol2gcode/images/drawing.svg)

### Mix planner: [`Rscripts`](./Rscripts)

R scripts to compute pipetting steps for the automatic "PCR workspace builder" feature in the GUI.

They work based on hierarchichal clustering of the target sample composition or contidions, to find a short "path" of pipetting steps.

![dendro_planner.png](./Rscripts/images/dendro_planner.png)

### Binary files: [`bin`](./bin)

Any binary files we used would be here, but it's empty for now.

MongoDB files for the RPi on Raspbian _would_ be here, but they are huge. They are included in the OS image available for download below.

### Pipette [`calibration`](./callibration)

Pipette callibration data, analysis and notes.

![pruebas_serie_bxplt.png](./calibration/results/21-11-08-p200-calib/pruebas_serie_bxplt.png)

> Example calibration for a 30 uL volume dispensing protocol. Note that the last volume is less than expected because of surface tension (i.e. the last microliter is hard to expell from the tip).

### Default workspaces and objects: [`defaults`](./defaults)

JSON definition for the "default" GUI workspaces, platforms, protocols, etc.

### Development documentation: [`doc`](./doc)

Extra documentation directory (currently in migration to the doc repo and/or other project subdirectories).

See its [README.md](./doc/README.md).

Content in this directory is referenced in the repo's READMEs when relevant.

### Arduino CNC firmware: [`grbl`](./grbl)

Source code for our configuration of the GRBL v1.1h Arduino Uno firmware.

All due credit to it's authors, read the `LICENSE` file for licencing information.

### Startup scripts: [`systemd.units`](./systemd.units)

`systemd.units` files to start/restart the Node.js server ([GUI](gui/README.md)) and the robot driver ([protocol2gcode](protocol2gcode/README.md) module).

> Some peopple hate systemd, but I don't know enough Linux to realize why.
